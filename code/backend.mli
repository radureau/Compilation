open Ass_prog

(*
@ensures
  - compiler a (Some(f))
      compile l'ass_prog 'a' vers l'out_channel 'f'
  - compiler a None
      affiche sur la sortie standard un dump du code compilé
*)
val compiler : ass_prog -> out_channel option -> unit

(*
@ensures
  - table_symboles a false
      retourne la table des symboles de l'ass_prog 'a'
  - table_symboles a true
      affiche la table des symboles en plus
*)
val table_symboles : ass_prog -> bool -> (string * int) list

(* 
@ensures
  - Vérifie la sémantique du programme, c'est-à-dire si les .org sont bien correctes et les labels bien initialisés
*)
val verif_semantique : ass_prog -> (int * (string * int) list)