{
  open Parser
  exception Impossible
}
rule decoupe = parse
| [' ''\t''\n'] { decoupe lexbuf }
| '#'[^'\n']*'\n' { COMMENTAIRE(Lexing.lexeme lexbuf) }

| '+' { CPLUS }
| '-' { CMOINS }
| '*' { CFOIS }
    
|   (
      "0"
	|(['1'-'9']['0'-'9']*)
	|('0''x'['a'-'f''A'-'F''0'-'9']+)
	|'\'' _ '\''
    ) as s
	{
	  let n =
	    if s.[0]='\''
	    then int_of_char s.[1]
	    else int_of_string s
	  in CNUM(n)
	}
    
| ':' { DEUXPOINTS }

| '(' { PARENTHESE_GAUCHE }
| ')' { PARENTHESE_DROITE }

| ".org " { ORG }
| ".long " { LONG }
| ".word " { WORD }
| ".byte " { BYTE }
| ".string " { STRING }
| "\"" ([^'"']* as s) "\"" { STR(s) }

| (
  "add"|"sub"|"mul"|"mov"|"if"|"in"|"out"
    as op
  )
  (
  'b'|'w'|'l'
    as sz
  )
  {
    let sz' = match sz with
      | 'b' -> Ass_prog.B
      | 'w' -> Ass_prog.W
      | _ -> Ass_prog.L
    in
    if op="if"
    then IF(sz')
    else
      let op' =
	match op with
	| "add" -> Ass_prog.Add
	| "sub" -> Ass_prog.Sub
	| "mul" -> Ass_prog.Mul
	| "mov" -> Ass_prog.Mov
	| "in" -> Ass_prog.In
	| "out" -> Ass_prog.Out
	| _ -> raise Impossible
      in
      INSTRUCTION(op',sz')
  }

| "then" { THEN }
| "else" { ELSE }
  
| "jmp" { JMP }
| "call" { CALL }
| "ret" { RET }
| "stop" { STOP }
    
| ['a'-'z''A'-'Z''_']['a'-'z''A'-'Z''_''0'-'9']* { IDENTIFIANT(Lexing.lexeme lexbuf) }

| '@' { AROBASE }
| ',' { VIRGULE }
| '$' { DOLLAR }
| "++" { INCREMENT }
| '[' { CROCHET_GAUCHE }
| ']' { CROCHET_DROIT }
| '%'(['0'-'9']|('1'['0'-'5']) as i) { REGISTRE(int_of_string i) }

| "==" { EGAL }
| "!=" { N_EGAL }
| ">" { SUP }
| "<" { INF }
| "<="|"=<" { INF_EGAL }
| ">="|"=>" { SUP_EGAL }
    
| eof { EOF } (* fin de fichier *)
