open Backend

let source_filename = ref ""
let dump = ref false
let show_symbols = ref false
let output = ref "picode"
  
(* * * * * * * * * * * * * * * * *)
(* Traitement arguments commande *)
(* * * * * * * * * * * * * * * * *)

let nb_anon_arg = ref 0
let fun_anon anon_arg = if !nb_anon_arg=0 then (source_filename := anon_arg ; nb_anon_arg := !nb_anon_arg+1) else failwith("nombre d'arguments incorrect. Documentation disponible avec l'option --help")

let speclist = [("-d", Arg.Set dump, "Réalise un Dump sur le flux standard de sortie du code PICODE généré.");
		("-o", Arg.Set_string output, "Le nom du fichier PICODE généré, par defaut picode.");
		("-t", Arg.Set show_symbols, "Affiche la table des symboles puis termine. ");
	       ]
;;
let usage_msg = "aspico est un compilateur d'assembleur pico (*.asm) vers du PICODE.\n\taspico filename [-d][-o <output filename>][-t]\nLes options disponibles sont :";;

Arg.parse speclist fun_anon usage_msg ;;

(* Fin traitement arguments *)

try
  (

    let f_in = open_in !source_filename in
    let ast = Parser.main (Lexer.decoupe) (Lexing.from_channel f_in) in

    let f_out = open_out_bin !output in
    
    if !show_symbols
    then let _ = table_symboles ast true in ()
    else
      (
	if !dump then compiler ast None;
	compiler ast (Some(f_out))
      )
  )
with
| Not_found -> if Sys.file_exists !output then Sys.remove !output ; failwith("utilisation d'identifiant indéfini")
| Ass_prog.Depassement -> if Sys.file_exists !output then Sys.remove !output ;  failwith("dépassement mémoire")
