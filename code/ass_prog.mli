type cst =
  | Plus of cst * cst
  | Moins of cst * cst
  | Fois of cst * cst
  | Cte of int
  | Id of string

type sz = L|W|B|Z

type op =
  | Add
  | Sub
  | Mul
  | Mov
  | Jmp
  | If_Egal | If_N_Egal
  | If_Inf | If_SupEgal
  | If_InfEgal | If_Sup
  | Call
  | Ret
  | In
  | Out
  | Stop

type opsz = op*sz

type primitive =
  | Org of cst
  | Long of cst
  | Word of cst
  | Byte of cst
  | String of string
  
type operande =
  | Im of cst (* immediat *)
  | DirMem of cst (* direct memoire *)
  | IndMem of cst (* indirect memoire *)
  | DirReg of int (* direct registre *)
  | IndReg of int (* indirect registre *)
  | IncIndReg of int (* indirect registre preincremente *)
  | IndRegInc of int (* indirect registre postincremente *)

type ass_prog =
  | Fin
  | Sequence of ass_prog * ass_prog  (* le premier ass_prog != de Sequence *)
  | Commentaire of string
  | Label of string
  | Primitive of primitive
  | Instruction of opsz * operande list * cst option * cst option
(* Instruction of opsz [dest ; S1 ; ...] N N2 *)

exception BadSequence
      
(*
@requires
  'ast' tq si 'ast' match Sequence(a,b)
  alors a ne match pas Sequence(a',b')
@ensures fold f ast acc
  effectue un fold sur 'ast'
@raises BadSequence
Remarque: f ne sera jamais appliqué à un ass_prog.Sequence
*)
val fold: (ass_prog -> 'a -> 'a) -> ass_prog -> 'a -> 'a
      
(* * * */*\* * * *)
(* * * >°v°< * * *)
(* * * *w*w* * * *)
(* * ENC()DAGE * *)

exception Depassement

(*
@ensures sz_to_nbytes sz
  retourne le nombre d'octets correspondants à 'sz'
*)
val sz_to_nbytes: sz -> int
  
(*
@ensures opsz_to_byte opsz
  retourne un entier <256 correspondant
  à l'encodage de l'instruction 'opsz' sur 1 octet
*)
val opsz_to_byte: opsz -> int

(*
@ensures op_has_N o
  retourne true si l'operation 'o' a un octet N
*)
val op_has_N: op -> bool
(*
@ensures op_has_D o
  retourne true si l'operation 'o' a un octet D
*)
val op_has_D: op -> bool
(*
@ensures op_has_S0 o
  retourne true si l'operation 'o' a un octet S0
*)
val op_has_S0: op -> bool
(*
@ensures op_has_Si o
  retourne true si l'operation 'o' a des octets Si
*)
val op_has_Si: op -> bool
(*
@ensures op_has_N2 o
  retourne true si l'operation 'o' a un octet N2
*)
val op_has_N2: op -> bool

(*
@ensures operande_to_nbytes o sz
  retourne le nombre d'octets que prends l'encodage
  de l'operande 'o'
*)
val operande_to_nbytes: operande -> sz -> int

(*
@requires 'instruction' match Instruction(_,_,_,_)
@ensures instruction_to_nbytes instruction
  retourne le nombre d'octets que prends l'encodage
  de l'instruction 'instruction'
*)
val instruction_to_nbytes: ass_prog -> int
  
(*
@ensures addr_to_byte n
  retourne un entier <2^16 correspondant
  à l'encodage de l'addresse 'n' d'une instruction sur 2 octets
@raises Depassement
*)
val addr_to_byte: int -> int

(*
@requires 0<=d<=1
@ensures effective_addr_to_first_byte d o
  retourne un entier correspondant
  à l'encodage du premier octet de l'operande 'o'
  avec 'd' = 1 si il s'agit du dernier operande, 0 sinon
*)
val operande_to_first_byte: int -> operande -> int

(*
@ensures number_to_a2_bytes nbytes n
  retourne un entier correspondant
  à l'encodage a2 sur 'nbytes' octets
  de la valeur 'n'
@raises Depassement
*)
val number_to_a2_bytes: int -> int -> int

(*
@require 'table_symboles' est une liste de couple (symbole,adresse)
@ensures eval c table_symboles
  retourne la valeur de 'c'
@raises Not_found
  si 'c' match Id(s) et s n'est pas dans 'table_symboles'
*)
val eval: cst -> (string*int)list -> int

(*
@requires
  'sz' provient de l'instruction utilisant l'operande 'op'
  0<='d'<=1
  'o' match Im|DirMem|IndMem
  'table_symboles' est une liste de couple (symbole,adresse)
@ensures operande_to_bytes nbytes o table_symboles
  retourne  le triplet (b1,nbytes,n) correspondant
  à l'encodage de l'operande 'o'
  avec 'd'= 1 si il s'agit du dernier operande, 0 sinon
*)
val operande_to_bytes: sz -> int -> operande -> (string*int)list -> int*int*int

(*
@requires
  'p' ne match pas Org|String
  'table_symboles' est une liste de couple (symbole,adresse)
@ensures primitive_to_bytes p table_symboles
  retourne le couple (nbytes,n) correspondant
  à l'encodage 'n' de la primitive 'p'
  sur 'nbytes' octets
*)
val primitive_to_bytes: primitive -> (string*int)list -> int*int
  
(*
@ensures print_bytes out nbytes n
  ecrit 'n' sur 'nbytes' octets
  dans le channel 'out'
*)
val print_bytes: out_channel -> int -> int -> unit

(*
@ensures print_string s out
  ecrit String.length 's' +1 octets
  dans le channel 'out'
*)
val print_string: string -> out_channel -> unit
