%{
  open Ass_prog
%}

%token EOF DEUXPOINTS

%token <string> COMMENTAIRE

%token <int> CNUM
%token <string> IDENTIFIANT
%token CPLUS CMOINS CFOIS
%token PARENTHESE_GAUCHE PARENTHESE_DROITE
%left CPLUS CMOINS
%left CFOIS
%nonassoc UPLUS UMOINS /* UPLUS et UMOINS permettent d'ecrire +1 et -1 */

%token ORG LONG WORD BYTE STRING
%token <string> STR

%token <Ass_prog.opsz> INSTRUCTION
%token <Ass_prog.sz> IF
%token AROBASE VIRGULE DOLLAR INCREMENT CROCHET_GAUCHE CROCHET_DROIT
%token <int> REGISTRE

%token EGAL N_EGAL SUP INF INF_EGAL SUP_EGAL

%token THEN ELSE

%token JMP CALL RET STOP

%start main
%type <Ass_prog.ass_prog> main
%%
main:
   | expr main { Sequence($1,$2) }
   | EOF { Fin }
;
  expr:
   | COMMENTAIRE { Commentaire($1) }
   | IDENTIFIANT DEUXPOINTS { Label($1) }
   | primitive { Primitive($1) }
   | instruction_operandes { $1 }
   | instruction_sans_operandes { $1 }
   | instruction_if { $1 }
;
cst:
   | cst CPLUS cst { Plus($1,$3) }
   | cst CMOINS cst { Moins($1,$3) }
   | cst CFOIS cst { Fois($1,$3) }
   | PARENTHESE_GAUCHE cst PARENTHESE_DROITE { $2 }
   | IDENTIFIANT { Id($1) }
   | CNUM { Cte($1) }
   | CPLUS cst %prec UPLUS { $2 }
   | CMOINS cst %prec UMOINS { Moins(Cte(0),$2) }
;
primitive:
   | ORG cst { Org($2) }
   | LONG cst { Long($2) }
   | WORD cst { Word($2) }
   | BYTE cst { Byte($2) }
   | STRING STR { String($2) }
;
instruction_operandes:
   | INSTRUCTION operandes { Instruction($1,$2,None,None) }
   | INSTRUCTION operandes ais { Instruction($1,$2,Some($3),None) }
;
instruction_if:
   | IF operande oc operande THEN fin_if
       {
	 let (bra1,bra2) = $6 in
	 Instruction(($3,$1),[$2;$4],bra1,bra2)
       }
;
fin_if:
   | cst { (Some($1),None) }
   | cst ELSE cst { (Some($1),Some($3)) }
;
operandes:
   | operande { [$1] }
   | operandes VIRGULE operande { $1@[$3] }
;
operande:
   | DOLLAR cst { Im($2) } /* immediat */
   | cst { DirMem($1) } /* direct memoire */
   | CROCHET_GAUCHE cst CROCHET_DROIT { IndMem($2) } /* indirect memoire */
   | REGISTRE { DirReg($1) } /* direct registre */
   | CROCHET_GAUCHE REGISTRE CROCHET_DROIT { IndReg($2) } /* indirect registre */
   | INCREMENT CROCHET_GAUCHE REGISTRE CROCHET_DROIT { IncIndReg($3) } /* indirect registre préincrementé */
   | CROCHET_GAUCHE REGISTRE CROCHET_DROIT INCREMENT { IndRegInc($2) } /* indirect registre postincrémenté */
;
instruction_sans_operandes:
   | JMP cst { Instruction((Jmp,Z),[],Some($2),None) }
   | CALL cst { Instruction((Call,Z),[],None,Some($2)) }
   | CALL cst ais { Instruction((Call,Z),[],Some($3),Some($2)) }
   | RET { Instruction((Ret,Z),[],None,None) }
   | STOP { Instruction((Stop,Z),[],None,None) }
;
ais:
   | AROBASE cst { $2 }
;
oc:
   | EGAL { If_Egal }
   | N_EGAL { If_N_Egal }
   | SUP { If_Sup }
   | INF { If_Inf }
   | INF_EGAL { If_InfEgal }
   | SUP_EGAL { If_SupEgal }
;
