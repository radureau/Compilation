open Ass_prog

(* * * * * * * *)
(* * DUMPING * *)
(* * * * * * * *)

let debug = Printf.printf
  
(*
Affiche en hexa sur la sortie standard le codage de n sur nbytes
*)
let rec dump nbytes n =
  let rec aux nbytes n =
    if nbytes=1
    then debug "%02x" n
    else
      let ndecalages = 8*(nbytes-1) in
      let first_byte = n land (255 lsl ndecalages) lsr ndecalages in
      let remain = n lsr ndecalages in
      debug "%02x" first_byte ; dump (nbytes-1) remain
  in if nbytes>0 then (aux nbytes n ; debug " ") else ()

let dump_string s =
  let len = String.length s in
  let rec aux i =
    if i=len then ( dump 1 0 ; debug "\t.string %s" s )
    else ( dump 1 (int_of_char s.[i]) ; aux (i+1) )
  in aux 0

(* * * * * * * * *)
(* Verifications *)
(* Semantiques * *)
(* * * * * * * * *)

let validation_instruction ast =
  match ast with
  | Instruction((op,_),operandes,_,_) ->
     (
       if op_has_D op then
	 (
	   match operandes with
	   | [] -> failwith("il manque l'operande 'D' de l'instruction")
	   | d::operandes' -> (match op with In|Out->()|_->(match d with Im(_)->failwith("mauvais operande 'D'estination")|_->()))
	 )
       ;
       let len = List.length operandes in
       match op with
       |Add|Sub|Mul-> if len<3 then failwith("pas assez d'operandes pour l'operation")
       |Mov -> if len!=2 then failwith("mov<sz> a besoin d'exactement 2 operandes")
       |In|Out -> if len!=1 then failwith("in/out<sz> ont besoin d'exactement 1 operande")
       | _ -> ()
	 
     )
  | _ -> ()

  
(* Vérifie la sémantique du programme, c'est-à-dire si les .org sont bien correctes et les labels bien initialisés *)
let verif_semantique ast =
	let symbole =
		fold 
		(
		fun ast symbole ->
			match ast with
			| Label(s) -> 
				(
				let symbole' =
					try 
						(s,0)::symbole
					with Not_found -> failwith("Variable non définie")
				in symbole'
				)
			| _ -> symbole
		)
		ast
		[]
	in

	fold
	(
	fun ast (acc,symbole) ->
		match ast with
		| Instruction((op,sz),operandes,c1,c2) ->
			(
			let _ = 
				match c1 with
				| Some(x)-> 
				(
					let _ = 
						try eval x symbole
						with Not_found -> failwith("Identifiant de label non définie c1")
					in ()
				)
				| None-> ()
			in
			let _ = 
				match c2 with
				| Some(x)-> 
				(
					let _ = 
						try eval x symbole
						with Not_found -> failwith("Identifiant de label non définie c2")
					in ()
				)
				| None-> ()
			in
			let _ =
				List.fold_left
				(
				fun accu operandes_val ->
					match operandes_val with
					| Im(x) | DirMem(x) | IndMem(x) ->
						(
						let _ = 
							try eval x symbole
							with Not_found -> failwith("Identifiant de label non définie")
						in ()
						)
					| _ -> ()
				)
				()
				operandes
			in (acc,symbole)
			)
		| Primitive(p) ->
			(match p with
			| Org(x) ->
				let x' = 
					try eval x [] 
					with Not_found -> failwith("Il ne faut pas d'identifiant dans un .org")
				in 
				if x'>=acc
				then (x',symbole)
				else failwith("Le nouveau .org est inférieur à l'ancien")
			| _ -> (acc,symbole)
			)
		| _ -> (acc,symbole)
	)
	ast
	(0,symbole)


(* * * * * * * * *)
(* * Table des * *)
(* * Symboles* * *)
(* * * * * * * * *)

let table_symboles ast dumping =
	let _ = verif_semantique ast in
    let _,symboles =
    fold
      ( fun ast (position,symboles) ->
	match ast with
	| Label(s) ->
	   let () = if dumping then debug "\t%s:\t@%04x\n" s position else ()
	   in position,(s,position)::symboles

	| Primitive(p) ->
	     (match p with
	     | Long(_) -> 4
	     | Word(_) -> 2
	     | Byte(_) -> 1
	     | String(s) -> String.length s + 1
	     | Org(c) -> try eval c [] with Not_found -> failwith(".org <cst>")
	     )
	   + position,symboles

	| Instruction(_,_,_,_) -> instruction_to_nbytes ast + position, symboles
	     
	| _ -> position,symboles
      )
      ast
      (0,[])
  in symboles


(* * * * * * * *)
(* COMPILATION *)
(* * * * * * * *)

let compiler ast out' =

  let dumping = match out' with None->true|_->false in
  let out = match out' with None->stdout|Some(file)->file in
  let write = print_bytes out in
  let write nbytes n = if dumping then dump nbytes n else write nbytes n in
  let print_string s out = if dumping then dump_string s else print_string s out in
  let addr_to_byte n = if dumping then (debug "@" ; addr_to_byte n) else addr_to_byte n in
  
  let (_,_) =
  fold
    (
      fun ast (position, symboles) -> let () = if dumping then debug "\n%04x:\t" position else () in
	match ast with

	| Primitive(p) ->
	   let ((nbytes,n),position') =
	     (match p with
	     | Long(_) | Word(_) | Byte(_) ->
		let new_position = position +
		  (match p with Long(_)->4|Word(_)->2|Byte(_)->1|_->failwith("Impossible"))
		in primitive_to_bytes p symboles,new_position
	     | String(s) -> print_string s out ; (0,0), String.length s + 1 + position
	     | Org(c) ->
		let new_position = try eval c [] with Not_found -> failwith(".org <cst>") in
		let () =
		  if new_position<position
		  then failwith(".org <cst>")
		  else if new_position>65535 then raise Depassement
		  else seek_out out new_position
		in let () = if dumping then debug ".org @%04x" new_position else ()
		in
		(0,0),new_position
	     )
	   in
	   let () = write nbytes n
	   in (position',symboles) (* modifie la position courante *)

	| Instruction( (op,sz) , d_operandes , c1, c2) ->
	   let () = validation_instruction ast in
	   let position' = instruction_to_nbytes ast + position in (* calcul adresse prochaine instruction *)
	   let ais_bra1 = match c1 with None->position' | Some(c) -> eval c symboles in (* branchement 1 ou AIS *)
	   let bra2 = match c2 with None->position' | Some(c) -> eval c symboles in (* branchement 2 *)

	   let () = write 1 (opsz_to_byte (op,sz)) in (* * *OP SZ * * *)
	   
	   let () = (* * * N * * *)
	     if op_has_N op
	     then write 2 (addr_to_byte (match op with If_Egal|If_Inf|If_InfEgal->bra2|_->ais_bra1))
	     else ()
	   in

	   let () = if op_has_D op || op_has_S0 op || op_has_Si op (* * * D S0 Si... * * *)
	     then
	       let inv = List.rev d_operandes in
	       let dernier_operande = List.hd inv in
	       let operandes = List.rev (List.tl inv) in
	       let () =
		 List.fold_left
		   (fun acc op ->
		     let b1,nbytes,bytes = operande_to_bytes sz 0 op symboles in
		     write 1 b1 ; write nbytes bytes
		   )
		   ()
		   operandes
	       in
	       let b1,nbytes,bytes = operande_to_bytes sz 1 dernier_operande symboles in
	       write 1 b1 ; write nbytes bytes
	     else ()
	   in

	   let () = (* * * N2 * * *)
	     if op_has_N2 op
	     then write 2 (addr_to_byte (match op with If_Egal|If_Inf|If_InfEgal->ais_bra1|_->bra2))
	     else () in
	   
	   (position',symboles)

	| _ -> (position, symboles) (* n'altère ni le code ni la table de symboles *)
 
    )
    ast
    (0,table_symboles ast false)
    
  in if dumping then Printf.printf "\n" else ()
