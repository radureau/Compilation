type cst =
  | Plus of cst * cst
  | Moins of cst * cst
  | Fois of cst * cst
  | Cte of int
  | Id of string

type sz = L|W|B|Z

type op =
  | Add
  | Sub
  | Mul
  | Mov
  | Jmp
  | If_Egal | If_N_Egal
  | If_Inf | If_SupEgal
  | If_InfEgal | If_Sup
  | Call
  | Ret
  | In
  | Out
  | Stop

type opsz = op*sz

type primitive =
  | Org of cst
  | Long of cst
  | Word of cst
  | Byte of cst
  | String of string
  
type operande =
  | Im of cst (* immediat *)
  | DirMem of cst (* direct memoire *)
  | IndMem of cst (* indirect memoire *)
  | DirReg of int (* direct registre *)
  | IndReg of int (* indirect registre *)
  | IncIndReg of int (* indirect registre preincremente *)
  | IndRegInc of int (* indirect registre postincremente *)

type ass_prog =
  | Fin
  | Sequence of ass_prog * ass_prog (* le premier ass_prog != de Sequence *)
  | Commentaire of string
  | Label of string
  | Primitive of primitive
  | Instruction of opsz * operande list * cst option * cst option
(* Instruction of opsz [dest ; S1 ; ...] N N2 *)

exception BadSequence
      
(*
@requires
  'ast' tq si 'ast' match Sequence(a,b)
  alors a ne match pas Sequence(a',b')
@ensures fold f ast acc
  effectue un fold sur 'ast'
@raises BadSequence
Remarque: f ne sera jamais appliqué à un ass_prog.Sequence
*)
let rec fold f ast acc =
  match ast with
  | Sequence(Sequence(_,_),_) -> raise BadSequence
  | Sequence(ass_prog,ast') ->
     let acc' = f ass_prog acc in
     fold f ast' acc'
  | _ -> f ast acc
      
(* * * */*\* * * *)
(* * * >°v°< * * *)
(* * * *w*w* * * *)
(* * ENC()DAGE * *)

exception Depassement

(*
@ensures sz_to_nbytes sz
  retourne le nombre d'octets correspondants à 'sz'
*)
let sz_to_nbytes sz =
  match sz with
  | L -> 4
  | W -> 2
  | B -> 1
  | Z -> 0
  
(*
@ensures opsz_to_byte opsz
  retourne un entier <256 correspondant
  à l'encodage de l'instruction 'opsz' sur 1 octet
*)
let opsz_to_byte (op,sz) =
  let b7_2 =
  match op with
  | Add -> 1
  | Sub -> 2
  | Mul -> 3
  | Mov -> 4
  | Jmp -> 5     
  | If_Egal | If_N_Egal -> 6
  | If_Inf | If_SupEgal -> 7
  | If_InfEgal | If_Sup -> 8
  | Call -> 9
  | Ret -> 10
  | In -> 11
  | Out -> 12
  | Stop -> 13
  in
  let b0_1 =
    match sz with
    | L -> 3
    | W -> 2
    | B -> 1
    | Z -> 0
  in b7_2*4 + b0_1

(*
@ensures op_has_N o
  retourne true si l'operation 'o' a un octet N
*)
let op_has_N o = match o with Ret|Stop->false|_->true
(*
@ensures op_has_D o
  retourne true si l'operation 'o' a un octet D
*)
let op_has_D o = match o with Ret|Stop|Jmp|Call->false|_->true
(*
@ensures op_has_S0 o
  retourne true si l'operation 'o' a un octet S0
*)
let op_has_S0 o = match o with Ret|Stop|Jmp|Call|In|Out->false|_->true
(*
@ensures op_has_Si o
  retourne true si l'operation 'o' a des octets Si
*)
let op_has_Si o = match o with Add|Sub|Mul->true|_->false
(*
@ensures op_has_N2 o
  retourne true si l'operation 'o' a un octet N2
*)
let op_has_N2 o = match o with Call|If_Egal|If_N_Egal|If_Inf|If_SupEgal|If_InfEgal|If_Sup->true|_->false

(*
@ensures operande_to_nbytes o sz
  retourne le nombre d'octets que prends l'encodage
  de l'operande 'o'
*)
let operande_to_nbytes o sz =
  1
  +
    match o with
    | Im(_) -> sz_to_nbytes sz
    | DirMem(_)|IndMem(_) -> 2
    | _ -> 0
  
(*
@requires 'instruction' match Instruction(_,_,_,_)
@ensures instruction_to_nbytes instruction
  retourne le nombre d'octets que prends l'encodage
  de l'instruction 'instruction'
*)
let instruction_to_nbytes instruction =
  match instruction with Instruction( (op,sz) , d_operandes , _N, _N2) ->
    1
    +
      (if op_has_N op then 2 else 0)
    +
      (
	if op_has_D op || op_has_S0 op || op_has_Si op
	then
	  List.fold_left
	    (fun acc op -> operande_to_nbytes op sz + acc)
	    0
	    d_operandes
	else 0
      )
    +
      (if op_has_N2 op then 2 else 0)
  | _ -> failwith("instruction_to_nbytes")

(*
@ensures addr_to_byte n
  retourne un entier <2^16 correspondant
  à l'encodage de l'addresse 'n' d'une instruction sur 2 octets
@raises Depassement
*)
let addr_to_byte n =
  if n>65535 (* 2^16-1 *)
  then raise Depassement
  else n

(*
@ensures number_to_a2_bytes nbytes n
  retourne un entier correspondant
  à l'encodage a2 sur 'nbytes' octets
  de la valeur 'n'
@raises Depassement
*)
let number_to_a2_bytes nbytes n =
  let bound = 1 lsl (nbytes*8) / 2 in
  let () = if n > bound-1 || n < -bound then raise Depassement in
  if n<0
  then (-n) lxor (bound-1) + 1
  else n

(*
@ensures eval c table_symboles
  retourne la valeur de 'c'
@raises Not_found
  si 'c' match Id(s) et s n'est pas dans 'table_symboles'
*)
let eval c table_symboles =
  let rec aux c =
    match c with
    | Cte(i) -> i
    | Plus(c1,c2) -> (aux c1) + (aux c2)
    | Moins(c1,c2) -> (aux c1) - (aux c2)
    | Fois(c1,c2) -> (aux c1) * (aux c2)
    | Id(s) -> List.assoc s table_symboles
  in aux c

(*
@requires 0<=d<=1
@ensures effective_addr_to_first_byte d o
  retourne un entier correspondant
  à l'encodage du premier octet de l'operande 'o'
  avec 'd' = 1 si il s'agit du dernier operande, 0 sinon
*)
let operande_to_first_byte d o =
  let d_bit = d lsl 4 in
  let codage =
    match o with
    | Im(_) -> 0
    | DirMem(_) -> 32
    | IndMem(_) -> 64
    | DirReg(i) -> 128 + i
    | IndReg(i) -> 128+32 + i
    | IndRegInc(i) -> 128+64 + i
    | IncIndReg(i) -> 128+64+32 + i
  in codage + d_bit
  
(*
@requires 
  'sz' provient de l'instruction utilisant l'operande 'op'
  0<='d'<=1
  'o' match Im|DirMem|IndMem
  'table_symboles' est une liste de couple (symbole,adresse)
@ensures operande_to_bytes nbytes o table_symboles
  retourne  le triplet (b1,nbytes,n) correspondant
  à l'encodage de l'operande 'o'
  avec 'd'= 1 si il s'agit du dernier operande, 0 sinon
*)
let operande_to_bytes sz d o table_symboles =
  let nbytes = operande_to_nbytes o sz - 1 in
  let b1 = operande_to_first_byte d o in
  let bytes =
  match o with
  | Im(c) -> number_to_a2_bytes nbytes (eval c table_symboles)
  | DirMem(c) | IndMem(c) -> addr_to_byte (eval c table_symboles)
  | _ -> 0
  in (b1,nbytes,bytes)
     
(*
@requires 'p' ne match pas Org|String
@ensures primitive_to_bytes p table_symboles
  retourne le couple (nbytes,n) correspondant
  à l'encodage 'n' de la primitive 'p'
  sur 'nbytes' octets
*)
let primitive_to_bytes p table_symboles =
  let nbytes =
    match p with
    | Long(_) -> 4 | Word(_) -> 2 | Byte(_) -> 1
    | String(s) -> failwith(".string a un encodage potentiellement trop long")
    | Org(_) -> failwith(".org est une primitive sans encodage")
  in
  let n =
    match p with Long(c) | Word(c) | Byte(c)
      -> number_to_a2_bytes nbytes (eval c table_symboles)
    | _ -> failwith("Impossible")
  in (nbytes,n)
    
(*
@ensures print_bytes out nbytes n
  ecrit 'n' sur 'nbytes' octets
  dans le channel 'out'
  en little endian
*)
let print_bytes out nbytes n =
  let rec aux nbytes n =
    if nbytes=1
    then output_byte out n
    else
      let ndecalages = 8*(nbytes-1) in
      let first_byte = n land (255 lsl ndecalages) lsr ndecalages in
      let remain = n lsr ndecalages in
      output_byte out first_byte ; aux (nbytes-1) remain
  in if nbytes>0 then aux nbytes n else ()

(*
@ensures print_string s out
  ecrit String.length 's' +1 octets
  dans le channel 'out'
*)
let print_string s out =
  let len = String.length s in
  let rec aux i =
    if i=len then print_bytes out 1 0
    else ( print_bytes out 1 (int_of_char s.[i]) ; aux (i+1) )
  in aux 0
