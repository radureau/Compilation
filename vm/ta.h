/*########################################################################
# $Software: ta tools
# $$RCSfile: ta.h.in,v $ 
# $Revision: 2.6.2.6 $
# $Author  : Ivan Auge (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ######################################################################*/

#ifndef FILE_TA_H
#define FILE_TA_H

#if defined(__cplusplus) && !defined(FILE_CPP)
#   define FILE_CPP
#endif

/*!
 * \defgroup talibs Library description an usage 
 * The Tools 'A' provides
 * a general purpose C/C++ static library named \b libta.a,
 * a static library dedicated to the debug of memory allocations and
 * frees of pure C application named \b libtamd.a,
 * a shared library also dedicated to memory debugging named \b tamd.so,
 * it must not been linked but ld-preloaded.
 * 
 * The general purpose library libta.a exists also under a development
 * form containing watchdog, its name is \b libtawd.a. 
 *
 * The memory debugging libraries are configured to debug applications
 * having less than 500,000 simultaneous allocated blocs. The memory
 * debugger uses for itself arround 36 mb of memory.
 * The tamd5.so library is similar to the tamd.so library but it can
 * support the debug of applications having around 5,000,000 simultaneous
 * allocated memory blocks. In this case the memory debugger uses
 * for itself arround 360 mb of memory.
 *
 * The libtamd.a library must only be used within purely C application.
 * When using both the general purpose library and the memory debug
 * library, you must use the "libtawd.a" library.
 *
 * \attention you must not ld-preload tamd.so with apllication linked with
 * libtamd.a.
*/

/************************************************************************/
/**** release                                                        ****/
#define TA_VERSION_MAJOR  2
#define TA_VERSION_MEDIUM 6
#define TA_VERSION_MINOR  2

#define TA_VERSION_UPDATE 'a'

/************************************************************************/
/**** standart include.                                              ****/

#include <stdio.h>
#include <stdarg.h>
#include <string.h>

/************************************************************************/
/**** main defines                                                   ****/

#ifndef TRUE
/* (1==1) not supported by gcc 2.7.2.2 for default paramerter values */
#   define TRUE 1
#endif

#ifndef FALSE
#   define FALSE 0 /* (1!=1) */
#endif

#ifndef NULL
#   define NULL (void*)0
#endif

#define TA_FL32_HVALUE  ((ta_uintxx)0xffffffff)
#define TA_FL32_LVALUE  ((ta_uintxx)0x0)

#define TA_FLXX_HVALUE  ((ta_uintxx)-1)
#define TA_FLXX_LVALUE  ((ta_uintxx)0x0)

/*** shortcut for defining C++ section ***/
#if defined(FILE_CPP) || defined(__cplusplus)
#   define TA_START_CXX_SECTION extern "C" {
#   define TA_END_CXX_SECTION   }
#   define TA_CG_InlineTemplate
#else
#   define TA_START_CXX_SECTION
#   define TA_END_CXX_SECTION
#endif

/*** defines avoid compilation warnings  ***/
#ifdef TA_NoCcWarning
TA_START_CXX_SECTION
extern void taAccW_Eat1Operand( const void* x);
extern void taAccW_Eat2Operand( const void* x, const void* y);
extern void taAccW_Eat3Operand( const void* x,const void* y, const void* z);
TA_END_CXX_SECTION
#else
#define taAccW_Eat1Operand(x)   
#define taAccW_Eat2Operand(x,y) 
#define taAccW_Eat3Operand(x,y,z)   
#endif

/************************************************************************/
/**** types.                                                         ****/

/*** basic types. ***/
typedef char           ta_int8;       /* signed   of   8 bits */
typedef short          ta_int16;      /* signed   of  16 bits */
typedef int            ta_int32;      /* signed   of  32 bits */
typedef unsigned char  ta_uint8;      /* flag     of   8 bits */
typedef unsigned short ta_uint16;     /* flag     of  16 bits */
typedef unsigned int   ta_uint32;     /* flag     of  32 bits */
#define TA_FLAGXXBITSZ  64
typedef unsigned long  ta_uintxx;     /* flag     of 32 or 64 bits */
#if    TA_FLAGXXBITSZ == 64
typedef ta_uintxx      ta_uint128[2]; /* flag     of 128 bits */
#elif  TA_FLAGXXBITSZ == 32
typedef ta_uintxx      ta_uint128[4]; /* flag     of 128 bits */
#else
#error TA_FLAGXXBITSZ macro must be defined to 32 or 64 
#endif /* TA_FLAGXXBITSZ */

typedef const void* ta_cvoid;
typedef const char* ta_cstr;
typedef char  ta_path[1<<12];

/*** predefinition of ta types. ***/

/* dynamicl array */
typedef struct _ta_memt    ta_memt;
typedef struct _ta_strbcWS ta_strbcWS;  /* working set of strings */

/* no-clone set */
typedef struct _ta_noclone ta_noclone;

/* dynamic string */
typedef void* ta_strbc;  /* dynamical string */
   
/* for save/load features */
typedef const char*            ta_SL_Oid;
typedef struct _ta_SL_Table    ta_SL_Table;
typedef struct _ta_SL_Main     ta_SL_Main;

/*** predefinition of standard types. ***/
struct stat;

/************************************************************************/
/**** DO NOT USE THEM, THEY WILL BE SUPPRESED                        ****/
/**** as soon as that ta code which uses it, will be rewritten.      ****/
#define ta_boolean ta_uint8
#define cstr       ta_cstr
#define t_path     ta_path
#define int8       ta_int8
#define int16      ta_int16
#define int32      ta_int32
#define uint8      ta_uint8
#define uint16     ta_uint16
#define uint32     ta_uint32
#define uintxx     ta_uintxx
#define uint128    ta_uint128
#define flag8      ta_uint8
#define flag16     ta_uint16
#define flag32     ta_uint32

/************************************************************************/

/*! \defgroup taany  convert string to miscellaneous arithmetic type
 * @{
**/

#define TAANY_TypStr        char *
#define TAANY_TypSInt       signed int
#define TAANY_TypUInt       unsigned int
#define TAANY_TypReal       double

#define TAANY_UNDEF         0  /* unknow sub type */
#define TAANY_ArgStr        1  /* arg.u.xxx_str */
#define TAANY_ArgSInt       2  /* arg.u.xxx_sint */
#define TAANY_ArgUInt       3  /* arg.u.xxx_uint */
#define TAANY_ArgReal       4  /* arg.u.xxx_real */
#define TAANY_ArgStrArray  11  /* arg.u.xxx_str_array */
#define TAANY_ArgSIntArray 12  /* arg.u.xxx_sint_array */
#define TAANY_ArgUIntArray 13  /* arg.u.xxx_uint_array */
#define TAANY_ArgRealArray 14  /* arg.u.xxx_real_array */


typedef union _taany_data {
        TAANY_TypStr      str;
        TAANY_TypSInt     sint;
        TAANY_TypUInt     uint;
        TAANY_TypReal     real;
        struct {
          int            nbele; /* number of elements */
          union {
            TAANY_TypStr   *strarray;
            TAANY_TypSInt  *sintarray;
            TAANY_TypUInt  *uintarray;
            TAANY_TypReal  *realarray;
          } u;
        } array;
        TAANY_TypStr   **strarray2;
        TAANY_TypSInt  **sintarray2;
        TAANY_TypUInt  **uintarray2;
        TAANY_TypReal  **realarray2;
#define any_str        str
#define any_sint       sint
#define any_uint       uint
#define any_real       real
#define any_nbele      array.nbele
#define any_strarray   array.u.strarray
#define any_sintarray  array.u.sintarray
#define any_uintarray  array.u.uintarray
#define any_realarray  array.u.realarray
#define any_strarray2  strarray2
#define any_sintarray2 sintarray2
#define any_uintarray2 uintarray2
#define any_realarray2 realarray2
} taany_data;

#define taany_init(any,fl) do { \
    memset(any,0,sizeof(*any)); any->type=fl; } while (0)

/*!
 * \brief Convert string to string.
 *
 * The \b taany_str2str function set \p any->any_str to \p arg.
 * \n\n
 * The \b taany_str2sint function converts the \p arg string to a <em>signed
 * int</em> and returns it into \p any->any_sint.
 * \n\n
 * The \b taany_str2uint function converts the \p arg string to a <em>unsigned
 * int</em> and returns it into \p any->any_uint.
 * \n\n
 * The \b taany_str2sint function converts the \p arg string to a <em>double</em>
 * and returns it into \p any->any_real.
 *
 * \return
 * The \b taany_str2str function always returns 0.
 * The other functions return 0 if conversion is successful, they return -2
 * if an underflow or overflow error occurs, otherwise they return -1.
 *
 * \param any Where the conversion of \p arg string is returned.
 * \param arg the string to convert.
**/
extern int  taany_str2str (taany_data* any, ta_cstr arg);
/*! 
 * \brief Convert string to signed int.
 * \copydetails taany_str2str(taany_data*,ta_cstr)
**/
extern int  taany_str2sint(taany_data* any, ta_cstr arg);
/*! 
 * \brief Convert string to unsigned int.
 * \copydetails taany_str2str(taany_data*,ta_cstr)
**/
extern int  taany_str2uint(taany_data* any, ta_cstr arg);
/*! 
 * \brief Convert string to double.
 * \copydetails taany_str2str(taany_data*,ta_cstr)
**/
extern int  taany_str2real(taany_data* any, ta_cstr arg);

/**@}**/
 
/************************************************************************/
/*! \defgroup taeh  error handler 
 *
 * This error handler was designed to help
 * 1) in printing well formated error or information multi-line messages,
 * 2) in selecting the verbosity and 3) in handling the location which
 * generated the message.
 * The main routine to print messages is \ref taeh_printf, for instance,
 * the lines bellow show how to generate a muti-line message:
 * \code
 *   // an informational message
 *   taeh_printf(TAEH_WARN,0,"<n>%s parameter is undefined,\n",parname);
 *   taeh_printf(TAEH_WARN,0,"<c>%s is set to %s.\n",parname,pardefval);
 *   taeh_printf(TAEH_WARN,0,"<m>use the -td switch to set it.\n");
 * \endcode
 * With the verbosity set to silence mode, these lines print nothing,
 * with the default verbosity they produce something like:
 * \code
 *   WARN  : tmpdir parameter is undefined,
 *         : tmpdir is set to /tmp.
 * \endcode
 * and with the verbosity set to verbose mode, this is printed:
 * \code
 *   WARN  : tmpdir parameter is undefined,
 *         : tmpdir is set to /tmp.
 *         : use the -td switch to set it.
 * \endcode
 * \n
 * The message level are defined by the \ref taeh_level type.
 * There are 4/5 standart levels and numerous user specific levels:
 *   - fatal: it is the most severe level, usualy (and by default)
 *     a message at this level can not be omitted and stops the
 *     execution of the program (see \ref TAEH_FATAL).
 *   - error: it is less severe than the fatal level, usually
 *     (and by default) messages at this level can not be omitted
 *     (see \ref TAEH_FATERR and \ref TAEH_ERROR).
 *   - warning: it is dedicated to important informational messages,
 *     messages at this level can be omitted (see \ref TAEH_WARN).
 *   - information: it is dedicated to less important informational
 *     messages as the warning level, usually (and by default)
 *     they are omitted.
 *   - user level: it is not really a level, these are just identifier
 *     (see \ref TAEH_USER).
 *     One can record a such level with the \ref taeh_recordUserlevel
 *     function.
 * \n
 * Each time, the handler is called, it reads its configuration
 * from the \ref gl_taeh_config global variable.
 * The configuration mainly consists of the verbosity and of defining
 * the active output streams. For instance the next line set the
 * verbosity to most verbose and disable all the output stream except the
 * syslog utility.
 * \code
 *   gl_taeh_config.level  =  TAEH_MAXPRINT ;
 *   gl_taeh_config.option =  TAEH_FL_SYSLOG ;
 *   gl_taeh_config.stream =  0;
 * \endcode
 * \n
 * The printing routines as \ref taeh_printf use as second argument
 * a \ref taeh_position structure which specifies the location in the
 * source files where a message is producted. For instance
 * \code
 *   int main(int argc, char**argv) {
 *     taeh_position pos;
 *     ...
 *     pos.file=argv[0] ; pos.line=0;
 *     ...
 *     taeh_printf(TAEH_WARN,&pos,"<n>%s parameter is undefined,\n",parname);
 *     ...
 *     pos.file="adir/afile" ;  pos.line=0;
 *     ...
 *     pos.line += 1;
 *     ...
 *     pos.line += 1;
 *     ...
 *     taeh_printf(TAEH_ERROR,&pos,"<n>%s variable is undefined.\n",varname);
 *     ...
 *     pos.line += 1;
 *     ...
 *     taeh_printf(TAEH_ERROR,&pos,"<n>%s variable is undefined,\n",varname);
 *     ...
 *   }
 * \endcode
 * produces something like:
 * \code
 *   myprog:WARN  : tmpdir parameter is undefined,
 *   WARN  :adir/afile:+  2: gnu variable is undefined.
 *   WARN  :adir/afile:+  3: gnat variable is undefined.
 * \endcode
 * \n
 * \anchor taehlocation The algorithm uses to print the message location is:
 *   -# Be \arg pos the location of the message. pos is set to the second
 *      argument of the printing function or to the \arg taeh_config.defpos
 *      variable when this second argument is null.
 *   -# if pos.file is null, no location is printed so the message looks as:
 * \code
 *   LEVEL: <data of printf>
 * \endcode
 *   -# if pos.file is not null and pos.line is null, the message looks as:
 * \code
 *   <pos.file>:LEVEL: <data of printf>
 * \endcode
 *   -# if pos.file and pos.line are both not null, a complete location is
 *    printed as shown bellow:
 * \code
 *   LEVEL:<pos.file>:<pos.line>: <data of printf>
 * \endcode
**/

#ifdef __cplusplus
extern "C" {
#endif

/*! \ingroup taeh
 *  \brief Level of message.
**/
typedef enum _taeh_level {
    TAEH_NOPRINT=0,
    TAEH_PRINT=1,
    /* standart error level */
    TAEH_FATAL=2, TAEH_FATERR=3, TAEH_ERROR=4, TAEH_WARN=5, TAEH_INFO=6, 
    /* 7, 8, 9, ... 100 reserved for internal and futur use */
    TAEH_IN=7,
    TAEH_LEXNOFILE=10,
    TAEH_TEST_LEVEL0=15,
    TAEH_TEST_LEVEL1=16,
    TAEH_TEST_LEVEL2=17,
    TAEH_TEST_LEVEL3=18,
    TAEH_DOC_FATAL=21, TAEH_DOC_FATERR=22, TAEH_DOC_ERROR=23,
    TAEH_DOC_WARN=24,  TAEH_DOC_INFO=25, 
    /* 1000 user levels from TAEH_USER */
    TAEH_USER=100 ,
    TAEH_MAXPRINT=TAEH_USER+1000
} taeh_level;

/*! \ingroup taeh
 *  \brief Location of message.
 *
 * The \p taeh_position type defines the location of a message.
 * Usually the location is the \p line line in the \p file file.
 * How location is really printed is explained in the
 * \ref taehlocation "introduction". */
typedef struct _taeh_position {
    cstr   file;
    uint32 line;
} taeh_position;
typedef const taeh_position* ctaeh_position;

/*!
 * \ingroup taeh
 * \brief error handler configuration.
 *
 * The \p taeh_config type configures the error handler.
 **/
typedef struct taeh_config {
    /*! \brief minimal level to print. */
    taeh_level level;
    /*! \brief how message are printed.
     *
     *  The \p options field must be 0 or a "bitwise-or" of the
     *  following symbolic constants:
     *  \ref TAEH_FL_STDERR, \ref TAEH_FL_STREAM, \ref TAEH_FL_SYSLOG,
     *  \ref TAEH_FL_EXIT, \ref TAEH_FL_COLOR, \ref TAEH_FL_ADDLF. */
    uint32     options;
    /*! \brief an extra stream for printing messages. */
    FILE*      stream;
    /*! \brief default location uses by the printing functions.
     *
     * The \p defpos field is used by the printing functions
     * (\ref taeh_printf, \ref taeh_printf_cont, ...) when their
     * position argument is null. */
    taeh_position defpos;
    /*! \brief Standard message to print before printing an
     *  error message.
     *
     * If the \b premess[l] function pointer is not null
     * it is called before printing a main message (not
     * a "cont" nor a "more" and nor a "premess" message)
     * of \b l level.
     * \n
     * The level is transmited to the function as first
     * parameter and
     * the \b premess_param field is transmited to the
     * function as second parameter. */
    void   (*premess[TAEH_INFO+1])(taeh_level,ta_cvoid);
    ta_cvoid premess_param;
} taeh_config;

/*! \ingroup taeh
 * \brief Enable print of standart error stream.
 *
 * This flag must be set in the \ref taeh_config::options of the 
 * \ref gl_taeh_config variable.
 * It enables the message print into the standart error stream. */
#define TAEH_FL_STDERR 0x0001

/*! \ingroup taeh
 * \brief Enable print of the \ref taeh_config::stream stream.
 *
 * This flag must be set in the \ref taeh_config::options of the 
 * \ref gl_taeh_config variable.
 * It enables the message print into stream defined by the
 * \ref taeh_config::stream of the \ref gl_taeh_config variable.
 * Notice that this flag has no effect if taeh_config::stream is null. */
#define TAEH_FL_STREAM 0x0002

/*! \ingroup taeh
 * \brief Enable sylog utility.
 * 
 * This flag must be set in the \ref taeh_config::options of the 
 * \ref gl_taeh_config variable.
 * It nables the print of messages with the syslog utility. */
#define TAEH_FL_SYSLOG 0x0004

/*! \ingroup taeh
 * \brief Enable exit on fatal error.
 *
 * This flag must be set in the \ref taeh_config::options of the 
 * \ref gl_taeh_config variable.
 * It enables exit on fatal error. */
#define TAEH_FL_EXIT 0x0010

/*! \ingroup taeh
 * \brief Color the level of message.
 * 
 * This flag must be set in the \ref taeh_config::options of the 
 * \ref gl_taeh_config variable.
 * It colors the level of messages using the VT100 code key. */
#define TAEH_FL_COLOR  0x0020

/*! \ingroup taeh
 * \brief Add a line feed before printing the message.
 *
 * This flag must be set in the \ref taeh_config::options of the 
 * \ref gl_taeh_config variable.
 * When this flag is set a line feed is printed before writing the
 * message on the standart error stream. The flag is automatically
 * cleared as soon as the message is printed in any output stream. */
#define TAEH_FL_ADDLF  0x0040

#define TAEH_FL_ALL    0x0077
/*!
 * \ingroup taeh
 * \brief current configuration of the error handler
 *
 * The \p gl_taeh_config variable defines the configuration of the error
 * handler. Its default initialisation prints messages of level
 * \ref TAEH_FATAL, \ref TAEH_FATERR, \ref TAEH_ERROR, and \ref TAEH_WARN
 * into the standart error stream and it exits with 1 for the fatal error.
**/
extern taeh_config gl_taeh_config;

/*! \ingroup taeh
 * \brief Print the leading line of a multiple line message.
 *
 * The \b taeh_printf function returns witout printing anything if
 * the severity level defined \p level parameter is less than the one
 * defined by the \p gl_taeh_config.level variable.
 * Otherwise the \b taeh_printf  function prints a message into the
 * streams defined and enabled by the \ref gl_taeh_config variable,
 * the printed message consists of a header followed by a body
 * as shown under:
 * \code
 *   <header><body>
 * \endcode
 * The header is built using the \p level and \p pos parameters and
 * the gl_taeh_config.defpos variable as explained in the
 * \ref taehlocation "introduction".
 * The body is built using the \p fmt format and the arguments following
 * as the standart printf fuction.
 *  
 * The \b taeh_printf function supports an extension.
 * When the 3 first characters of the \p fmt parameter are "<X>",
 * the behavior is altered as explained bellow:
 *   - X=n : the message is printed without the leading "<n>".
 *   - X=c : in this case the function is equivalent to the
 *           \ref taeh_printf_cont function,
 *           the leading "<c>" is not printed     
 *   - X=m : the message is printed as the \ref taeh_printf_more 
 *           function does. The leading "<m>" is not printed.     
 *   - X=N : same as X=n but the exit on fatal messages is disabled.
 *   - X=C : same as X=c but the exit on fatal messages is disabled.
 *   - X=M : same as X=m but the exit on fatal messages is disabled.
 *   - X=. : if X differs from the former value, the "<X>" string 
 *           is printed.
**/     
extern void taeh_printf(
    taeh_level level, ctaeh_position pos, cstr fmt, ...);
extern void taeh_vprintf(
    taeh_level level, ctaeh_position pos, cstr fmt, va_list args);

/*! \ingroup taeh
 * \brief Print an additional line of a multiple line message.
 *  
 *  The \b taeh_printf_cont function returns or prints the message
 *  like the \ref taeh_printf function except that the "<X>" extension
 *  is no recognized and that the message header is reduced to a few
 *  spaces followed by a colon and a space.
 * \code
 *     : <data of printf>
 * \endcode
 **/ 
extern void taeh_printf_cont(
    taeh_level level, ctaeh_position pos, cstr fmt, ...);
extern void taeh_vprintf_cont(
    taeh_level level, ctaeh_position pos, cstr fmt, va_list args);

/*! \ingroup taeh
 * \brief Print an additional line of a multiple line message.
 *  
 *  The \b taeh_printf_more function is similar to the \ref
 *  taeh_printf_cont function except that it decreases the severity
 *  level of the message.
 *  For instance, "taeh_printf_more(TAEH_ERROR,...)" is equivalent to
 *  "taeh_printf_cont(TAEH_WARN,...)",
 *  similary, "taeh_printf_more(TAEH_INFO,...)" is equivalent to
 *  "taeh_printf_cont(TAEH_INFO,...)".
**/
extern void taeh_printf_more(
    taeh_level level, ctaeh_position pos, cstr fmt, ...);
extern void taeh_vprintf_more(
    taeh_level level, ctaeh_position pos, cstr fmt, va_list args);

/*! \ingroup taeh
 * \brief Returns the number of fatal messages.
**/
extern int taeh_getNbFatal();

/*! \ingroup taeh
 * \brief Returns the number of error messages.
**/
extern int taeh_getNbError();

/*! \ingroup taeh
 * \brief Returns the number of warning messages.
**/
extern int taeh_getNbWarning();

/*! \ingroup taeh
 * \brief Returns the number of information messages.
**/
extern int taeh_getNbInfo();

extern void taeh_printExitMessage();

/*! \ingroup taeh
 * \brief Describe a specific message and assign it to a key.
 *
 * The \b taeh_recordUserlevel records a new message type defined by the
 * parameters.
 * This new message type can then be referenced in the print function
 * (\ref taeh_printf, ...) using the \arg key as level argument.
 *
 * In the next example, the number of gnat found will be printed
 * if the "-pg" switch is set and the "-s" switch is not set.
 * You don't need to test anymore to know if printing must be done
 * or not, this makes source code cleaner. 
 * \code
 *   ...
 *   #define GNATMESS ((taeh_level)TAEH_USER+10)
 *   ...
 *   int main(int argc,char**argv) {
 *     int nbgnat;
 *     ...
 *     taeh_recordUserlevel(GNATMESS,TAEH_NOPRINT,0,0,0);
 *     if ( -s in command line )
 *        taeh_config.level= TAEH_ERROR ;
 *     if ( -pg in command line )
 *        taeh_recordUserlevel(GNATMESS,TAEH_WARN,0,0,0);
 *     ...
 *     taeh_printf(GNATMESS,0,"%d gnat%s found\n",nbgnat,nbgnat?"s","");
 *     ...
 *   }
 * \endcode
 *
 * \param key The key which identifies this new message type.
 * \param level The message level of the new message type. It
 *        must be a valid level (TAEH_FATAL, ..., TAEH_INFO) or
 *        0, this last being equivalent to TAEH_INFO.
 * \param disopt The \p disopt parameter must be 0 or a "bitwise-or"
 *        of the following symbolic constants:
 *        \ref TAEH_FL_STDERR, \ref TAEH_FL_STREAM, ...
 *        The options set in this parameter are disabled when a message
 *        of this new type is printed.
 * \param label The \p label parameter defines the label string which
 *        is printed for identifying this level. A null value sets it
 *        to one the standart values (FATAL, ERROR, ... info).
 * \param color The \p label parameter defines the color which must be
 *        used to print the label string.
 *        A null value sets it to one the default colors.
 *        Notice that this parameter has no effect if color mode is disable
 *        when printing a message of this new type.
**/
extern void taeh_recordUserlevel(taeh_level key,
    taeh_level level, uint32 disopt, cstr label, cstr color);

/************************************************************************/

#ifdef TA_WATCHDOG
#   define tawd_cond_i_printf(cond,...) \
                    if (cond) tawd_perm_i_printf(__VA_ARGS__)
#   define tawd_cond_w_printf(cond,...) \
                    if (cond) tawd_perm_w_printf(__VA_ARGS__)
#   define tawd_cond_e_printf(cond,...) \
                    if (cond) tawd_perm_e_printf(__VA_ARGS__)
#   define tawd_cond_f_printf(cond,...) \
                    if (cond) tawd_perm_f_printf(__VA_ARGS__)
#else /* no watchdog */
#   define tawd_cond_i_printf(cond,...)
#   define tawd_cond_w_printf(cond,...)
#   define tawd_cond_e_printf(cond,...)
#   define tawd_cond_f_printf(cond,...)
#endif 

#   define tawd_perm_i_printf(...) tawd_i_printf(__FILE__,__LINE__,__VA_ARGS__)
#   define tawd_perm_w_printf(...) tawd_w_printf(__FILE__,__LINE__,__VA_ARGS__)
#   define tawd_perm_e_printf(...) tawd_e_printf(__FILE__,__LINE__,__VA_ARGS__)
#   define tawd_perm_f_printf(...) tawd_f_printf(__FILE__,__LINE__,__VA_ARGS__)

extern void tawd_i_printf( cstr file, int n, cstr fmt, ...);
extern void tawd_w_printf( cstr file, int n, cstr fmt, ...);
extern void tawd_e_printf( cstr file, int n, cstr fmt, ...);
extern void tawd_f_printf( cstr file, int n, cstr fmt, ...);
extern void tawd_i_vprintf(cstr file, int n, cstr fmt, va_list args);
extern void tawd_w_vprintf(cstr file, int n, cstr fmt, va_list args);
extern void tawd_e_vprintf(cstr file, int n, cstr fmt, va_list args);
extern void tawd_f_vprintf(cstr file, int n, cstr fmt, va_list args);

#ifdef __cplusplus
}
#endif 

/************************************************************************/
/**** dynamic table module                                           ****/

/*! \defgroup tadt  dynamically allocated table */

/*! \ingroup tadt */
typedef struct _tadt_data {
    int    dt_msize; /*!< \brief number of elements allocated in dt_array. */
    int    dt_usize; /*!< \brief number of elements present in dt_array. */
    short  dt_esize; /*!< \brief byte size of dt_array element. */
    /*! \brief number of elements added to dt_msize when
     * size increasing is required. */
    short  dt_incr;;
    void*  dt_array[0]; /*!< \brief elements of array. */
} *tadt_data;

/*! \ingroup tadt
 * \brief Returns the number of elements in the
 * dynamically allocated table.
**/
#define TADT_NB(t)     ((t)->dt_usize)
/*! \ingroup tadt
 * \brief Returns the pointer to the i th element of the
 * dynamically allocated table.
**/
#define TADT_PEL(t,i)  (((char*)((t)->dt_array))+(i)*((t)->dt_esize))
/*! \ingroup tadt \brief
 * \brief Returns the pointer to the first element of the
 * dynamically allocated table.
**/
#define TADT_LoopBeg(t)      ((char*)((t)->dt_array))
/*! \ingroup tadt
 * \brief Returns the pointer to the last plus 1 element of the
 * dynamically allocated table.
**/
#define TADT_LoopEnd(t)      TADT_PEL(t,TADT_NB(t))

#define TADT_First(t) (TADT_NB(t)!=0 ? TADT_PEL(t,0)            : (void*)0)
#define TADT_Last(t)  (TADT_NB(t)!=0 ? TADT_PEL(t,TADT_NB(t)-1) : (void*)0)

/*!
 * \ingroup tadt
 * \brief Creates an empty table.
 *
 * The \b tadt_create function creates an empty dynamically allocated table
 * the byte size of its elements being \p esize.
 * The table is created with \p mnb pre-allocated elements.
 * The table reallocation will be done by adding \p mincr elements to the table.
 * \return
 * The \b tadt_create function returns the created table.
 * \param esize The byte size of elements in the dynamic allocated table.
 * \param mnb   The number of elements that must be allocated.
 * \param mincr The number of new elements that will be allocated when
 *        more rooms is required.
**/
extern tadt_data tadt_create(int esize, int mnb, int mincr);

/*!
 * \ingroup tadt
 * \brief Duplicates a dynamically allocated table.
 *
 * The \b tadt_dup function creates a dynamically allocated table that
 * is a clone of the \p dt table.
 * \return
 * The \b tadt_dup function returns the created table.
 * \param dt  the table to duplicated.
**/
extern tadt_data tadt_dup(const tadt_data dt);

/*!
 * \ingroup tadt
 * \brief Frees a dynamically allocated table
 *
 * The \b tadt_free function frees all the memory used by the \p dt table.
 * \param dt  the table to free.
**/
extern void tadt_free(tadt_data dt);

extern void tadt_freep(tadt_data* dt);

/*!
 * \ingroup tadt
 * \brief Clones a dynamically allocated table
 *
 * The \b tadt_clone function clones the \p src table into the \p dest table.
 * The function assumes that element byte size of tables are identical.
 * \param dest  The cloned table.
 * \param src   The dynamically allocated table to clone.
**/
extern void tadt_clone(tadt_data* dest, const tadt_data src);

/*!
 * \ingroup tadt
 * \brief Appends a dynamic table to a dynamic table
 *
 * The \b tadt_append function appends the \p src table to the \a dest table.
 * The function assumes that element byte size of tables are identical.
 * \param dest The resulting dynamically allocated table.
 * \param src  The dynamically allocated table to append.
**/
extern void tadt_append(tadt_data* dest, const tadt_data src);

/*!
 * \ingroup tadt
 * \brief Adds a new element to the table and returns a pointer to it.
 *
 * The \b tadt_newp function adds at the end of the \p dt dynamically allocated
 * table a new element. The element is not initialized.
 * \return
 * The \b tadt_newp function returns the pointeur to the added element.
 * \param dt The dynamically allocated table.
**/
extern void* tadt_newp(tadt_data* dt);

/*!
 * \ingroup tadt
 * \brief Adds a new element to the table and copies \p *p to it.
 *
 * The \b tadt_putp function adds at the end of the \p dt dynamically allocated
 * table a new element. The element is initialized by copying the data pointed
 * to by the \p p pointer.
 * \param dt The dynamically allocated table.
 * \param p  The element to add.
**/
extern void tadt_putp(tadt_data* dt, void* p);

/*!
 * \ingroup tadt
 * \brief Appends/suppresses \p nb elements to a dynamically allocated table.
 *
 * The \b tadt_addNbEl function adds or suppresses elements in the \p dt table
 * depending on \p nb is positive or negative.
 *  - if \p nb is greater than zero then the function adds at the end of the \p
 *    dt dynamically allocated table \p nb uninitialized elements.
 *  - if \p nb is less than zero then the function suppresses the \p nb tailing
 *    elements of the \p dt dynamically allocated table.
 *    In this case:
 *    1) \p -nb must be less or egal to the number of elements in the
 *       table (eg: TADT_NB(*dt));
 *    2) the allocated memory of the table is left unchanged.
 *
 * \param dt The dynamically allocated table.
 * \param nb The number of element to append or to suppress depending
 *        on \p nb is less or greater than zero.
**/
extern void tadt_addNbEl(tadt_data* dt, int nb);

/*!
 * \ingroup tadt
 * \brief Suppresses \p nb consecutive elements of a dynamically allocated table.
 *
 * The \b tadt_addNbEl function suppresses the elements \p index, \p index+1,
 * ..., \p index+nb-1 of the \p dt table.
 * Furthermore \p index+nb must be less or equal than the number of elements in
 * the table (eg: TADT_NB(*dt)).
 * \param dt    The dynamically allocated table.
 * \param index The index in table of the first element to suppress.
 * \param nb    The number of elements to suppress.
**/
extern void tadt_delNbEl(tadt_data* dt, int index, int nb);

/*!
 * \ingroup tadt
 * \brief Sorts a dynamically allocated table.
 *
 * The \b tadt_sort member sorts the array using the comparison function
 * \p func.
 *
 * \param dt the dynamically allocated table.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be:\n
 *   int (*)(const X* l, const X*r);
**/
extern void tadt_sort(tadt_data dt, void* func);

/*!
 * \ingroup tadt
 * \brief Dichotomic search of an element in a sorted table.
 *
 * The \b tadt_bsearchPtr function searches for an element in the
 * table that is equal to the element pointed to by \p p
 * for the \p func comparison function.
 * The function assumes that the array is already sorted in the
 * order defined by the \p func comparison function.
 *
 * \return
 *   The \b tadt_bearchPtr function returns the element of table
 *   equal to the element pointed to by \p p if it exists,
 *   otherwise the function returns the null pointer.
 *
 * \param dt the dynamically allocated table.
 * \param p  the element to search for.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be:\n
 *   int (*)(const X* l, const X*r);
**/
extern void* tadt_bsearchPtr(tadt_data dt, void* p, void* func);

/*!
 * \ingroup tadt
 * \brief Dichotomic search of the position of an element in a sorted table.
 *
 * The \b tadt_bsearchIdx function assumes that the table is sorted in the
 * order defined by the \p func comparison function.
 * The \b tadt_bsearchIdx function computes the position (index) in the
 * table of the element pointed to by \p p.
 * This position indicates where the element must be inserted in the array
 * leaving the array sorted.
 * \return
     Be E the element pointed to by \p p, the function returns:
 *   - -1: all the table elements are greater than E.
 *   - TADT_NB(dt): all the table elements are less than E.
 *   - i: the i-1, i-2, i-3, ... array elements are less than E
 *     and the i+0, i+1, i+2, ... table elements are greater or equal to E.
 *   -
 *
 * \param dt The dynamically allocated table.
 * \param p  The element the position of which is searched.
 * \param func The comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be:\n
 *   int (*)(const X* l, const X*r);
**/
extern int tadt_bsearchIdx(tadt_data dt, void* p, void* func);

/*!
 * \ingroup tadt
 * \brief Inserts an element into a sorted table keeping it sorted.
 *
 * The \b tadt_putpS function assumes that the table is sorted
 * in the order defined by the \p func comparison function.
 * The \b tadt_putpS function inserts into the table the element
 * pointed to by \p p keeping the table sorted.
 * 
 * \param dt the dynamically allocated table.
 * \param p  the element to add.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be:\n
 *   int (*)(const X* l, const X*r);
**/
extern void tadt_putpS(tadt_data* dt, void* p, void* func);

/*!
 * \ingroup tadt
 * \brief Inserts an element into a sorted table keeping it sorted.
 *
 * The \b tadt_putpSU (S:sort ; U:unique) function assumes
 * that the table is sorted in the order defined by
 * the \p func comparison function.
 * If the element pointed to by \p p does not exist in the table,
 * the \b tadt_putpSU function inserts it into the table.
 * keeping the table sorted.
 * \return
 *   The \b tadt_putpSU function returns the pointer to the table 
 *   element equal to the element pointed to by \p p.
 *   So the boolean <b><tt> P==tadt_putpSU(dt,P,func)</tt></b>
 *   indicates that \p P has been inserted in table.
 *   
 * \param dt the dynamically allocated table.
 * \param p  the element to add.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be:\n
 *   int (*)(const X* l, const X*r);
**/
extern void* tadt_putpSU(tadt_data* dt, void* p, void* func);

/*!
 * \ingroup tadt
 * \brief Sorts a dynamically allocated table of pointers.
 *
 * The \b tadt_sort_2 function is similar to the 
 * \ref tadt_sort function except that the used comparison
 * function is defined as follow:
 * <tt>
 *   int cmp(void** l, void** r)
 *   { return func(*l,*r); }
 * </tt>
 * This function is useful for table the elements of which are pointers.
 *
 * \param dt the dynamically allocated table.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be:\n
 *   int (*)(const X* l, const X*r);
**/
extern void tadt_sort_2(tadt_data dt, void* func);

/*!
 * \ingroup tadt
 * \brief Dichotomic search of an element in a sorted table.
 *
 * The \b tadt_bsearchPtr_2 function is similar to the 
 * \ref tadt_bsearchPtr function except that the used comparison
 * function is defined as follow:
 * <tt>
 *   int cmp(void** l, void** r)
 *   { return func(*l,*r); }
 * </tt>
 * This function is useful for table the elements of which are pointers.
 *
 * \param dt the dynamically allocated table.
 * \param p  the element to search for.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be: int (*)(const X* l, const X*r);
**/
extern void* tadt_bsearchPtr_2(tadt_data dt, void* p, void* func);

/*!
 * \ingroup tadt
 * \brief Dichotomic search of the position of an element in a sorted table.
 *
 * The \b tadt_bsearchIdx_2 function is similar to the 
 * \ref tadt_bsearchIdx function except that the used comparison
 * function is defined as follow:
 * <tt>
 *   int cmp(void** l, void** r)
 *   { return func(*l,*r); }
 * </tt>
 * This function is useful for table the elements of which are pointers.
 *
 * \param dt the dynamically allocated table.
 * \param p  the element to search for.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be: int (*)(const X* l, const X*r);
**/
extern int tadt_bsearchIdx_2(tadt_data dt, void* p, void* func);

/*!
 * \ingroup tadt
 * \brief Inserts an element into a sorted table keeping it sorted.
 *
 * The \b tadt_putpS_2 function is similar to the 
 * \ref tadt_putpS function except that the used comparison
 * function is defined as follow:
 * <tt>
 *   int cmp(void** l, void** r)
 *   { return func(*l,*r); }
 * </tt>
 * This function is useful for table the elements of which are pointers.
 *
 * \param dt the dynamically allocated table.
 * \param p  the element to search for.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be: int (*)(const X* l, const X*r);
**/
extern void tadt_putpS_2(tadt_data* dt, void* p, void* func);

/*!
 * \ingroup tadt
 * \brief Inserts an element into a sorted table keeping it sorted.
 *
 * The \b tadt_putpSU_2 function is similar to the 
 * \ref tadt_putpSU function except that the used comparison
 * function is defined as follow:
 * <tt>
 *   int cmp(void** l, void** r)
 *   { return func(*l,*r); }
 * </tt>
 * This function is useful for table the elements of which are pointers.
 *
 * \param dt the dynamically allocated table.
 * \param p  the element to search for.
 * \param func the comparison function.
 *  If X is the type of the table of element, the prototype
 *  of this function must be: int (*)(const X* l, const X*r);
**/
extern void* tadt_putpSU_2(tadt_data* dt, void* p, void* func);

/************************************************************************/
/***** memory & dynamic array handler                               *****/

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/*************************************************************/
/***** basic memory allocation                           *****/

#define TAM_MALLOC(n)       malloc(n) 
#define TAM_MALLOCI(n)      calloc(1,n)
#define TAM_CALLOC(n0,n1)   calloc(n0,n1)
#define TAM_REALLOC(p,n)    realloc((void*)(p),n)
#define TAM_FREE(p)         free(p)
#define TAM_SetLVAL(p,n)    memset(((void*)(p)),(int)0, (ta_uint32)(n))
#define TAM_SetHVAL(p,n)    memset((void*)(p),(int)-1,(ta_uint32)(n))
#define TAM_SetCpy(pd,ps,n) memcpy((void*)(pd),(const void*)(ps),(ta_uint32)(n))
#define TAM_TypMALLOC(t)    (t *)TAM_MALLOC( sizeof(t))
#define TAM_TypMALLOCI(t)   (t *)TAM_MALLOCI(sizeof(t))
#define TAM_TypCALLOC(t,n)  (t *)TAM_CALLOC(sizeof(t),n)

#define TAM_SetVarLVAL(v)    TAM_SetLVAL(&(v),sizeof(v))

/************************************************************/
/***** fonctions de debug de la memoire.                *****/

/*! \defgroup tamd  memory debug handler 
 *
 * The TA memory debuger is usefull to track the programming
 *  errors in dynamic allocated memory blocks in one hand and in the
 *  the other hand to get statistic about amount of memory used during
 *  a program execution.
 *
 * When a programming error occurs, a message is printed, then the
 * \ref tamD_stop function is called and finaly the exit function is
 * called.
**/

/*! \ingroup tamd
 * The \b tamD_on function initializes the memory debuger.
 * The \p level parameter defines the error the handler tracks,
 * the allowed values are:
 *   - 0: don't active it.
 *   - 1: enable the track of invalid free.
 *   - 2: like 1 plus the track when a memory block is freed if the
 *        memory around it is not altered.
 *   - 3: like 2 but checking if the memory around blocks is altered
 *        is performed for all the allocated blocks, every time free
 *        is called.
 *   - 4: like 3  but checking if the memory around blocks is altered
 *        is also performed each time, the functions malloc, calloc and
 *        realloc are called.
 *
 * Notice
 *   that the levels 3 and 4 are CPU expensive,
 *   that the \ref tamD_stat function produces correct statistics at
 *   level 1, 2, 3 and 4 but not at level 0
 *   and that the level 2, 3 and 4 are equivalent to the level 1 if
 *   the \p isize parameter is 0.
 *
 * The \p isize parameter gives the size of borders that are added to
 * each memory block used to check if the memory around the blocks are
 * altered. The unit of the \p isize parameter is \p sizeof(int).
 *
 * The \p option parameter contains miscellaneous options:
 *  - the 20 less sgnificant bits (mask: 0x00FFFFF) gives the maximum
 *    number of blocks the \ref tamD_stat function dumps.
 *  - the 20 and 21 th bit (mask: 0x00300000) indicates which blocks
 *    the \ref tamD_stat function must dump:
 *      - 00 : print the unfreed memory blocks, starting by those
 *      allocated the earlier.
 *      - 10 : print the unfreed memory blocks, starting by those
 *      allocated the later.
 *      - 10 : print the unfreed memory blocks, starting by the greaters.
 *      - 11 : print the unfreed memory blocks, starting by the smaller.
 *  - the 8 most sgnificant bits (mask: 0xFF000000) gives the file
 *    descriptor into  \ref tamD_stat writes it messages. The zero value
 *    is equivalent to 2 and both match the the standart error.
 *
 * The subsequent arguments consist of a list from 1 to 6 interger the
 * last being a zero. Be X a such integer, the memry debuger will call
 * the \ref tamD_stop function when the X th allocation is performed.
 * \n
 * \attention Take care not to use this function in C++ program or C program linked
 * with C++ libraries.
**/
extern void tamD_on (int level,int isize,ta_uint32 option,...);

/*! \ingroup tamd
 * The \b tamD_von function is equivalent to the \ref tamD_on function
 * except that it is called with a va_list instead of a variable
 * number of arguments.
 * The \ref tamD_von function does not call the va_end macro. Consequently,
 * the value of ap is undefined after the call. The  application should call
 * va_end(ap) itself afterwards.
**/
extern void tamD_von(int level,int isize,ta_uint32 option,va_list ap);

/*! \ingroup tamd
 * The \b tamD_dump function configures the memory debuger to dump
 * all the memory allocation and free from the \p num th memory
 * operation included.
 * \n
 * \attention Take care not to use this function in C++ program or C program linked
 * with C++ libraries.
**/
extern void tamD_dump(ta_uint32 num);

/*! \ingroup tamd
 * The \b tamD_stat prints a summary of allocations and frees make from
 * the program start and lists optionally the unfreed blocks depending on
 * the initialization of the memory debuger performed by the \ref tamD_on
 * function.
 * \n
 * \attention Take care not to use this function in C++ program or C program linked
 * with C++ libraries.
**/
extern void tamD_stat();

/*! \ingroup tamd
 * The \b tamD_verif function checks if the boundaries of allocated blocks
 * are altered. When it found a block with altered boundaries, it prints
 * an error message and returns.
 * \n
 * \attention This function only exists in the libtamd.a and tamd.so libraries.
**/
extern void tamD_verif();

/*! \ingroup tamd
 * The \b tamD_stop function does absolutely nothing. It is jsut called
 * when the memory debugger detects errors.
 * Under a classic debugger as gdb, one can set a breakpoint in it to
 * suspend the execution of the program being debugged when a memory
 * allocation error occurs and then finds the location of the error.
 * \n
 * \attention This function only exists in the libtamd.a and tamd.so libraries.
**/
extern void tamD_stop();

/************************************************************/
/***** pointer comparison                               *****/

extern int voidScmp (const void* l, const void* r);
extern int voidSScmp(const void* const* l, const void* const* r);

/************************************************************/
/***** fonctions des tableaux dynamiques.               *****/

typedef struct _ta_memt_par {
    flag16  size_e; /* taille de l'element en char */
#define TAMT_IsAllocated	0x8000
    flag16  incr;   /* bit[0-14] : increment  augmentation
                     *              du tableau.
                     * bit[15]=1 : indique que le struct ment
                     *              a ete alloue.  */
    int32   user_size;  /* taille user du tableau (en element). */
    int32   nb;     /* taile reelle du tableau (en element). */
} ta_memt_par;

struct _ta_memt {
    ta_memt_par info;
    void*     t;
};
#define FCT_CAT2(a,b) a##b
#define TAMT_F(pt)              ((pt)->t)
#define TAMT_L(pt)              (((char*)(pt)->t)+((pt)->info.user_size*(pt)->info.size_e))
#define TAMT_NB(pt)             ((pt)->info.user_size)
#define TAMT_PEL(pt,i)          (((char*)((pt)->t))+((i)*(pt)->info.size_e))
#define TAMT_TY_PEL(ty,pt,i)    ((ty*)TAMT_PEL(pt,i))
#define TAMT_IND(pt,p)          ((((char*)(p))-((char*)((pt)->t)))/(pt)->info.size_e)
#define TAMT_FOR(pt,type,p) \
    for (p=(type*)TAMT_F(pt),FCT_CAT2(p,f)=(type*)TAMT_L(pt);p<FCT_CAT2(p,f);p++)
#define TAMT_FOR_SPC(pt,type,p) for ( p=(type*)TAMT_F(pt), \
    FCT_CAT2(p,f)=(type*)TAMT_L(pt);p<FCT_CAT2(p,f); \
    p = (type*) (((char*)p)+(pt)->info.size_e))
#define TAMT_LVALUE(pt,i,j) \
    MEM_LVALUE(TAMT_PEL(pt,i),(j-i)*(pt)->info.size_e)

/* init d'un tableau de 3  elements et d'increment 2        */
/* init d'un tableau de nb  elements et d'increment incr    */
/* libere le tableau dynamique associe a pt.                */


extern void tamt_init32p(ta_memt ** ppt, int32 size_e);
extern void tamt_init32(ta_memt * pt, int32 size_e);
extern void tamt_initp(ta_memt ** ppt, int32 size_e, int32 nb, int32 incr); 
extern void tamt_init(ta_memt * pt, int32 size_e, int32 nb, int32 incr); 
extern void tamt_free(ta_memt * pt);
extern void* tamt_FreeAndGetMem (ta_memt * pt);

/* obtention de nouvels elements */
extern void  tamt_vnewp(void* * pret,ta_memt * pt);
extern void* tamt_newp(ta_memt * pt);
extern int32 tamt_newi(ta_memt * pt);
extern void  tamt_AddNbEl(ta_memt* pt,int32 nb);
extern void  tamt_SetNbEl(ta_memt* pt,int32 nb);
extern void  tamt_putp(ta_memt * pt,void*  pel);

/* removing nb elements */
extern void  tamt_deliN(ta_memt * pt,int32 i,int32 nb);

/* sorted memt :  sort, put a element, search an element. */
extern void  tamt_S(ta_memt * pt, void* sort_fct);
extern void* tamt_putpS(ta_memt * pt,const void*  pnew, void* sort_fct);
extern void* tamt_putpS1(ta_memt * pt,const void* pnew, void* sort_fct);
extern int32 tamt_searchi(const ta_memt * pt,const void* pel,  void* sort_fct);
extern void* tamt_searchp(const ta_memt * pt,const void* pel,  void* sort_fct);
extern void* tamt_GrOfLep(const ta_memt * pt,const void* pel,void* sort_fct);
extern void* tamt_LeOfGrp(const ta_memt * pt,const void* pel,void* sort_fct);
extern void* tamt_Greaterp(const ta_memt * pt,const void* pel,void* sort_fct);
extern void* tamt_Lesserp(const ta_memt * pt,const void* pel,void* sort_fct);


/*!
 * \brief make \a d a clone of \a s.
 *
 * The \b tamt_cpy function clears the \a d array and then copies into it,
 * the \a s array.
 * The function assumes that the sizes of the \a d and \a s array
 * elements are the same.
 * \param d The destination array, it is the array to clone.
 * \param s The source array, it is the cloned array.
**/
extern void  tamt_cpy(ta_memt * d, const ta_memt * s);
/*!
 * \brief make \a d a clone of the first \a nb elements of \a s.
 *
 * The \b tamt_cpyNb function clears the \a d array and then copies
 * into it the \a nb first elements of the \a s array.
 * The function assumes that the sizes of the \a d and \a s array
 * elements are the same.
 * If \a nb is greater than the user size of the \a s array then
 * it is reduced to the \a s user size.
 * \param d The destination array, it is the array to clone.
 * \param s The source array, it is the cloned array.
 * \param nb The number of elements to copy.
**/
extern void  tamt_cpyNb(ta_memt * d, const ta_memt * s, int32 nb);

/*!
 * \brief append the \a s array to the \a d array.
 *
 * The \b tamt_app function appends the \a s array elements into
 * the \a d array.
 * The function assumes that the sizes of the \a d and \a s array
 * elements are the same.
 * \param d The destination array.
 * \param s The source array.
**/
extern void  tamt_app(ta_memt * d, const ta_memt * s);
/*!
 * \brief append the first \a nb elements of the \a s array
 *        to the \a d array.
 *
 * The \b tamt_appNb function appends the first \a nb elements of
 * the \a s array into the \a d array.
 * The function assumes that the sizes of the \a d and \a s array
 * elements are the same.
 * If \a nb is greater than the user size of the \a s array then
 * it is reduced to the \a s user size.
 * \param d The destination array.
 * \param s The source array.
 * \param nb The number of elements to copy.
**/
extern void  tamt_appNb(ta_memt * d, const ta_memt * s, int32 nb);

extern void  tamt_dup(ta_memt * d, const ta_memt * s);
extern void  tamt_dupNb(ta_memt * d, const ta_memt * s, int32 nb);
extern void  tamt_dupp(ta_memt ** d,const ta_memt * s);
extern void  tamt_duppNb(ta_memt ** d,const ta_memt * s, int32 nb);

/************************************************************/
/***** specific tables.                                 *****/

typedef struct _ta_StrbcMt {
    ta_memt strbc_mt;
} ta_StrbcMt;

#define STRBC_NB(ps)    TAMT_NB(&(ps)->strbc_mt)
#define STRBC_PEL(ps,i) TAMT_TY_PEL(ta_strbc,&(ps)->strbc_mt,i)
#define STRBC_FOR(ps,p) TAMT_FOR(&(ps)->strbc_mt,ta_strbc*,p)

extern void  tamt_SBCinit32p( ta_StrbcMt**  ppt);
extern void  tamt_SBCinit32(ta_StrbcMt*  pt);
extern void  tamt_SBCinitp(ta_StrbcMt** ppt, int32 nb, int32 incr);
extern void  tamt_SBCinit(ta_StrbcMt*  pt,  int32 nb, int32 incr);
extern void  tamt_SBCfree(ta_StrbcMt*  pt);
extern ta_strbc* tamt_SBCnewp(ta_StrbcMt*  pt);
extern void tamt_SBCAddNbEl( ta_StrbcMt* pt,int32 nb);
extern void tamt_SBCputp(ta_StrbcMt* pt,ta_strbc pel);
extern void tamt_SBCputpStr(ta_StrbcMt* pt,const char* str);
extern void tamt_SBCS( ta_StrbcMt*  pt);
extern void tamt_SBCputpS(ta_StrbcMt* pt,const ta_strbc str);
extern void tamt_SBCputpSStr(ta_StrbcMt* pt,const char* str);
extern ta_boolean tamt_SBCputpS1(ta_StrbcMt* pt,const ta_strbc str);
extern void  tamt_SBCputpS1Str(ta_StrbcMt* pt,const char* str);
extern ta_strbc* tamt_SBCsearchp(ta_StrbcMt* pt,const ta_strbc str);
extern ta_strbc* tamt_SBCsearchpStr(ta_StrbcMt* pt,const char* str);

/************************************************************/
/***** C++ interface.                                   *****/

/*************************************************************/
/***** basic memory allocation                           *****/

extern void* tam_malloc(size_t sz);
extern void  tam_SetAddr(void* addr);
extern void  tam_SL_SetFct(void* (*)(ta_SL_Main*,size_t));
extern void  tam_SL_SetMain(ta_SL_Main* psl);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

/*########################################################################
# Application :  Tools 'A'
# File        :  ./include/ta_string.h
# Revision    :  $Revision: 2.6.2.6 $
# Author      :  Ivan Aug� (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ########################################################################
# libta.a libtawd.a: library of Tools 'A'
# ########################################################################
# ######################################################################*/

/*****************************************************************************/
/*****************************************************************************/
/*! \defgroup String string modules */
/*****************************************************************************/
/*****************************************************************************/

/*****************************************************************************/
/*! \defgroup ext_str extension of the libc string handler
 *  \ingroup String 
*/
 

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/*!
 * \brief test if str is the null pointer or the empty string
 *
 * The \b strisnull CPP macro returns true (1) if the \p str pointer
 * is null or points to the empty string (""), otherwise it
 * returns  false (0).
**/
#define strisnull(str)  ( (str)==0 || (*((char*)(str)))==0 )

/*!
 * \brief test if str is the null pointer or a string containing only
 *  spaces (\\s), tabulations (\\t) or line-feed (\\n) characters.
 *
 * The \b strisempty CPP macro returns true (1) if the \p str pointer
 * is null or points to a string containing only spaces (\\s),
 * tabulations (\\t) or line-feed (\\n) characters.
 * Otherwise it returns  false (0).
**/
#define strisempty(str) ( strisnull(str) || \
    ( ( (*((char*)(str)))==' ' || (*((char*)(str)))=='\t' || \
        (*((char*)(str)))=='\n' ) && \
      ( (*(((char*)(str))+1))==0 || strisempty_f(((char*)(str))+1) ) ) )

/*!
 * \brief test if str is the null pointer or a string containing only
 *  spaces (\\s), tabulations (\\t) or line-feed (\\n) characters.
 *
 * The \b strisempty function returns true (1) if the \p str pointer
 * is null or points to a string containing only spaces (\\s),
 * tabulations (\\t) or line-feed (\\n) characters.
 * Otherwise it returns  false (0).
**/
extern int  strisempty_f(ta_cstr str);

/*!
 * \brief comparison of 2 string treating null pointers
 * as the empty string.
 *
 * The \b strcmpbis macro compares the 2 strings pointed to by \p l
 * and \p r as the standard \b strcmp function but treating
 * the null pointer as the empty string ("").
**/
#define strcmpbis(l,r)  ( (l) && *(l) ? ( (r) && *(r) ? ( \
    ((l)[0]-(r)[0])!=0 ? (l)[0]-(r)[0] : strcmp((l)+1,(r)+1)) : 1 ) \
    : ( (r) && *(r) ? -1 : 0) )
/*!
 * \brief comparison of 2 strings treating null pointers
 * as the empty string.
 *
 * The \b strcmpbis_f function is the similar to the \b strcmpbis
 * macro.
**/
extern int  strcmpbis_f(ta_cstr l, ta_cstr r);

extern char*  strcpyToUp( char* dest, ta_cstr src);
extern char*  strcpyToLow(char* dest, ta_cstr src);
extern void  strConvToUp( char * destSrc);
extern void  strConvToLow(char * destSrc);
extern char*  strcatInt32(char * destSrc, int32 nb);

/*! The \p strCBE function returns the sub-string of \p DestSrc string
 * without the leading and trailing space and tabulation characters.
 * Caution DestSrc is altered. */
extern char* strCBE(char* DestSrc);
/*! The \p strCarCBE function returns a pointer to the first word of
 * the \p DestSrc string, a word being a sequence of any characters
 * except of null, space and tabulation characters.
 * The function accepts a null \p DestSrc as an empty string.
 * The function returns the null pointer if no word is found in
 * the \p DestSrc string.
 * Caution the function alters the \p DestSrc string by setting to null
 * the character following the word. */
extern char* strCarCBE(char* DestSrc);
/*! The \p strCdrCB function returns a pointer to the second word of
 * the \p DestSrc string, a word being a sequence of any characters
 * except of null, space and tabulation characters.
 * The function accepts a null \p DestSrc as an empty string.
 * The function returns the null pointer if the second word does not
 * exist in the \p DestSrc string. */
extern cstr strCdrCB(cstr DestSrc);
/*! The \p strCarCBEcdrCB function returns the same value as
 * \p strCar(DestSrc) and sets \p cdr to the value returned
 * \p strCdrCB(DestSrc). */
extern char* strCarCBEcdrCB(char* DestSrc, char** cdr);

extern int16 strMinLen_0(char* prems, va_list ap);
extern int16 strMinLen(char* prems,...);
extern int16 strMinLen_T(char** table);

extern int16 strMaxLen_0(char* prems, va_list ap);
extern int16 strMaxLen(char* prems,...);
extern int16 strMaxLen_T(char** table);


extern ta_cstr strBasename(const char * src);
extern void    strcpyBasename(char * dest,const char * src,const char * suff);
extern void    strcpyDirname(char * dest,const char * src);
extern void    strcpySuffix(char * dest,const char * src);
extern void    strcpyWithoutSuff(char * dest,const char * src);
extern void    strcpyWithoutPost(char * dest,const char * src,const char * post);
extern void    strcpyWithoutPre(char * dest,const char * src,const char * pre);
extern int     strFind(const char* src,char * test);


extern int  strcpyReplace (char * dest, const char * src,...);
extern int vstrcpyReplace (char * dest, const char * src, va_list ap);
extern int tstrcpyReplace (char * dest, const char * src, char** table);

extern int strcpyReplace1  (char * dest, const char * src,...);
extern int vstrcpyReplace1 (char * dest, const char * src, va_list ap);
extern int tstrcpyReplace1(char * dest, const char * src, char** table);


/*****************************************************************************/
/* basic dynamic string handler.                                             */

/*! \defgroup ta_strbc basic dynamic string handler 
 *  \ingroup String 
 *
 * strbc handles dynamicaly allocated strings,
 * it provides most of the functions of the string handler of the standard
 * C library (strcpy, strcat, ...)
 * and of the \ref ext_str "its ta extensions".
 *
 * A dynamic string is defined by the \b ta_strbc type.
 * Most of the functions of this handler have a dynamic string as first
 * parameter called \p dest and return a dynamic string as for
 * instance the cpy function "ta_strbc tasbc_cpy(dest,src)".
 * These functions use and modify the \p dest argument and return it.
 * Because the modification of the \p dest string can induce a memory
 * reallocation
 * you must always reassigned the \p dest argument to the returned value.
 * So the statement "p=tasbc_cpy(p,x);" is correct
 * but "tasbc_cpy(p,x);" is incorrect
 * and "q=tasbc_cpy(p,x);" is dangerous because the \p variable must
 * no more be used.
 * \n
 * The next example shows how the handler must be used. Notice
 * the usage of the \b TASBC_STR macro to convert the the dynamic
 * string into \b char*,
 * and the memory free of the string after usage.
 * \code 
 * // initialize p to "left " and q to "right"
 * ta_strbc p=tasbc_init("left ");
 * ta_strbc q=tasbc_init("right");
 * // concate q to p into p
 * p=tasbc_cat(p,q);
 * // print p
 * printf("%s\n",TASBC_STR(p));
 * // free memory used by p and q
 * tasbc_free(p);
 * tasbc_free(q);
 * \endcode
**/

#define tasbc_null    0              /* the null or empty string (variable) */
#define TASBC_NULL    ((ta_strbc)0)  /* the null or empty string (static use) */
#define TASBC_STR(x)  ((char *)(x))  /* the string.  */
#define TASBC_FREE(x) ((char *)(x))  /* the string and free except string.*/
#define TASBC_LEN(x)  ( (x!=0) ? strlen(TASBC_STR(x)) : 0)/* length of string without the final 0. */
#define tasbc_init(s)  tasbc_cpy(0,(ta_strbc)(s))
#define tasbc_free(d)  tasbc_cpy(d,0)
extern void tasbc_free_f(ta_strbc str);
   
/*! \ingroup ta_strbc
 * \brief Copy a string
 *
 * The \b tasbc_cpy function overwrites the \p dest string with the \p src
 * string and returns it.
 * \n
 * Both \p dest and \p src may be tasbc_null.
 * \n
 * If \p src is tasbc_null then the memory allocated by the \p dest string
 * is released and \p dest is set to tasbc_null.
**/
extern ta_strbc tasbc_cpy(ta_strbc dest,const ta_strbc src);

/*! \ingroup ta_strbc
 *  \brief Concate two strings
 *
 * The \b tasbc_cat function concates the \p src string
 * to the \p dest string \p dest and returns it.
**/
extern ta_strbc tasbc_cat(ta_strbc dest,const ta_strbc src);

/*! \ingroup ta_strbc
 * \brief Concate an integer to a string
 *
 * The \b tasbc_catInt32 function converts the \p nb number in decimal,
 * and concates it to the \p dest string and returns it.
 * For instance, after the next instructions, the value of the \p str
 * string is "a string 32".
 * \code 
 * ta_strbc str=tasbc_init("a string ");
 * str=tasbc_catInt32(str,0x20);
 * \endcode
**/
extern ta_strbc tasbc_catInt32(ta_strbc dest,int32 nb);
  
/*! \ingroup ta_strbc
 *  \brief Duplicate a string
 *
 * The \b tasbc_dup function returns a clone of the \p src string.
 * It is a macro to "tasbc_cpy(tasbc_null,src)".
**/
extern ta_strbc tasbc_dup(const ta_strbc str);
#define tasbc_dup(src) tasbc_cpy(tasbc_null,src)

/*! \ingroup ta_strbc
 *  \brief Duplicate a string
 *
 * The \b tasbc_dup_f function returns a clone of the \p src string.
 * It is equivalent to "tasbc_dup(src)" and "tasbc_cpy(tasbc_null,src)",
 * but is a real C function.
**/
extern ta_strbc tasbc_dup_f(const ta_strbc str);

/*! \ingroup ta_strbc
 *  \brief string comparison
 *
 * The \b tasbc_cmp function compares the two strings \p s1 and \p s2.
 * It returns an integer less than, equal to, or greater than zero
 * if \p s1 is found, respectively, to be less than, to match, or be greater
 * than \p s2.
 * \n
 * The value of \p s1 and \p s2 must be diferent of tasbc_null.
 * \n
 * This function is implemented by a macro.
**/
extern int tasbc_cmp(const ta_strbc s1,const ta_strbc s2);
#define    tasbc_cmp(s0,s1)  strcmp(TASBC_STR(s0),TASBC_STR(s1))

/*! \ingroup ta_strbc
 *  \brief string comparison
 *
 * The \b tasbc_cmpbis function compares the two strings \p s1 and \p s2.
 * It returns an integer less than, equal to, or greater than zero
 * if \p s1 is found, respectively, to be less than, to match, or be greater
 * than \p s2.
 * \n
 * The value of \p s1 and \p s2 may be tasbc_null. In the comparison
 * order tasbc_null is equal to the empty string (eg: "").
 * \n
 * This function is implemented by a macro.
**/
extern int tasbc_cmpbis(const ta_strbc s1,const ta_strbc s2);
#define    tasbc_cmpbis(s0,s1)  strcmpbis(TASBC_STR(s0),TASBC_STR(s1))

/*! \ingroup ta_strbc
 *  \brief string comparison
 *
 * The \b tasbc_cmpbis_f function compares the two strings \p s1 and \p s2.
 * It returns an integer less than, equal to, or greater than zero
 * if s1 is found, respectively, to be less than, to match, or be greater
 * than \p s2.
 * \n
 * The value of \p s1 and \p s2 may be tasbc_null. In the comparison
 * order tasbc_null is equal to the empty string (eg: "").
 * \n
 * This function is a real C function.
**/
extern int tasbc_cmpbis_f(const ta_strbc s1,const ta_strbc s2);
  
/*! \ingroup ta_strbc
 *  \brief string comparison
 *
 * The \b tasbc_cmpbis_pt_f function compares the two strings pointed
 * to by \p s1 and \p s2.
 * It returns an integer less than, equal to, or greater than zero
 * if s1 is found, respectively, to be less than, to match, or be greater
 * than \p s2.
 * \n
 * The value of \p s1 and \p s2 may be tasbc_null. In the comparison
 * order tasbc_null is equal to the empty string (eg: "").
 * \n
 * This function is a real C function.
**/
extern int tasbc_cmpbis_pt_f(const ta_strbc* p1,const ta_strbc* p2);

#define     tasbc_cpyString(d,s)    tasbc_cpy(d,(ta_strbc)(s))
#define     tasbc_catString(d,s)    tasbc_cat(d,(ta_strbc)(s))
  
  
#define      tasbc_ConvToUp(DS)  strConvToUp(TASBC_STR(DS))
#define      tasbc_ConvToLow(DS)  strConvToLow(TASBC_STR(DS))


extern ta_strbc tasbc_cpyBasename(ta_strbc dest,const ta_strbc src,const char * suff);
extern ta_strbc tasbc_cpyDirnameStr(ta_strbc dest, ta_cstr src);
extern ta_strbc tasbc_cpyDirname(ta_strbc dest,const ta_strbc src);
extern ta_strbc tasbc_cpySuffix(ta_strbc dest,const ta_strbc src);
extern ta_strbc tasbc_cpyWithoutSuff(ta_strbc dest,const ta_strbc src);
extern ta_strbc tasbc_cpyWithoutPost(ta_strbc dest,const ta_strbc src,const char * post);
extern ta_strbc tasbc_cpyWithoutPre(ta_strbc dest,const ta_strbc src,const char * pre);
extern ta_strbc tasbc_cpyReplace_T(ta_strbc dest,const ta_strbc src,const char** table);
extern ta_strbc tasbc_cpyReplaceS_T(ta_strbc dest,const char* src,const char** table);
extern ta_strbc tasbc_cpyReplace_0(ta_strbc dest, const ta_strbc src, va_list ap);
extern ta_strbc tasbc_cpyReplace(ta_strbc dest, const ta_strbc src,...);
extern ta_strbc tasbc_cpyReplace_T(ta_strbc dest, const ta_strbc src,const char** table);

extern ta_strbc tasbc_cpyReplace_1_0(ta_strbc dest, const ta_strbc src, va_list ap);
extern ta_strbc tasbc_cpyReplace_1(ta_strbc dest, const ta_strbc src,...);
extern ta_strbc tasbc_cpyReplace_1_T(ta_strbc dest, const ta_strbc src, char**  table);

/*! \ingroup ta_strbc
 * \brief obsolete
**/
#define tasbc_cpyPrintf256 tasbc_cpyPrintf

/*! \ingroup ta_strbc
 * \brief obsolete
**/
#define tasbc_catPrintf256 tasbc_catPrintf

/*! \ingroup ta_strbc
 * \brief generate a string according to a format
 *
 * The \b tasbc_cpyPrintf function generates a string 
 * according to the \p fmt format as in printf(3).
 * The string \p dest is overwritten by the generated string and
 * returned.
 * \n
 * This function is optimized for generated string less than 1024
 * bytes. However it works with any size of the generated string.
 * In the following example the "hi" string is replaced by "hello
 * world".
 * \code
 * ta_strbc dest=tasbc_init("hi"); 
 * tasbc_cpyPrintf(dest,"%s world","hello");
 * \endcode
**/
extern ta_strbc tasbc_cpyPrintf(ta_strbc dest,ta_cstr mp_first,...);

/*! \ingroup ta_strbc
 *  \brief generate a string according to a format 
 *
 * The function \b tasbc_cpyvPrintf is equivalent to the function
 * \b tasbc_cpyPrintf, except that it is called with a va_list instead of
 * a variable number of arguments.
 * This function does not call the va_end macro.
 * Consequently, the value of \p ap is undefined after the  call.
 * The caller should call va_end(ap) itself afterwards.
**/
extern ta_strbc tasbc_cpyvPrintf(ta_strbc dest,ta_cstr fmt,va_list ap);

/*! \ingroup ta_strbc
 * \brief Print a string according to a format
 *
 * The \b tasbc_cpyPrintfLG function generates a string 
 * according to the \p fmt format as in printf(3).
 * The string \p dest is overwritten by the generated string and
 * returned. \n
 * The size of the generated string including the final null,
 * must not exceed \p dest_lg bytes.
**/
extern ta_strbc tasbc_cpyPrintfLG(int32 dest_lg,ta_strbc dest,ta_cstr fmt,...);

/*! \ingroup ta_strbc
 *  \brief generate a string according to a format 
 *
 * The function \b tasbc_cpyvPrintfLG is equivalent to the function
 * \b tasbc_cpyPrintfLG, except that it is called with a va_list instead
 * of a variable number of arguments.
 * This function does not call the va_end macro.
 * Consequently, the value of \p ap is undefined after the  call.
 * The caller should call va_end(ap) itself afterwards.
**/
extern ta_strbc tasbc_cpyvPrintfLG(int32 dest_lg,ta_strbc dest,ta_cstr fmt,va_list ap);

/*! \ingroup ta_strbc
 *  \brief generate a string according to a format 
 *
 * The \b tasbc_catPrintf function works the same way as the
 * \ref tasbc_cpyPrintf function, except that it concatenates the generated
 * string with \p dest intead of overwriting it.
**/
extern ta_strbc tasbc_catPrintf(ta_strbc dest,ta_cstr mp_first,...);

/*! \ingroup ta_strbc
 *  \brief generate a string according to a format 
 *
 * The function \b tasbc_catvPrintf is equivalent to the function
 * \b tasbc_catPrintf, except that it is called with a va_list instead of
 * a variable number of arguments.
 * This function does not call the va_end macro.
 * Consequently, the value of \p ap is undefined after the  call.
 * The caller should call va_end(ap) itself afterwards.
**/
extern ta_strbc tasbc_catvPrintf(ta_strbc dest,ta_cstr fmt,va_list ap);
 
/*! \ingroup ta_strbc
 *  \brief generate a string according to a format 
 *
 * The \b tasbc_catPrintfLG function works the same way as the
 * \ref tasbc_cpyPrintfLG function, except that it concatenates the generated
 * string with \p dest intead of overwriting it.
**/
extern ta_strbc tasbc_catPrintfLG(int32 dest_lg,ta_strbc dest,const char * fmt,...);

/*! \ingroup ta_strbc
 *  \brief generate a string according to a format 
 *
 * The function \b tasbc_cpyvPrintfLG is equivalent to the function
 * \b tasbc_cpyPrintfLG, except that it is called with a va_list instead
 * of a variable number of arguments.
 * This function does not call the va_end macro.
 * Consequently, the value of \p ap is undefined after the  call.
 * The caller should call va_end(ap) itself afterwards.
**/
extern ta_strbc tasbc_catvPrintfLG(int32 dest_lg,ta_strbc dest,const char * fmt, va_list ap);

  
/**********************************************************************************/
/*** formating handler. ***/
extern ta_strbc tasbc_cpyShiftR(ta_strbc str,int16 sh);
extern int16 tasbc_setShiftR(int16 shift);
extern char* tasbc_shiftR(ta_strbc* str);
#define TASTR_SH_DEFAULT	(~0)
#define TASTR_SH_TAB(i)		((1<<14)|(i))
extern ta_strbc tasbc_cpyListInit(ta_strbc dest,const char* RightSep,const char* ListTk, const char* LeftSep);
extern ta_strbc tasbc_cpyListAdd(ta_strbc dest,const char* ListToken,const char* RightSep,const ta_strbc ele, const char* LeftSep);
extern  ta_strbc tasbc_cpyListAddString(ta_strbc dest,const char* ListToken,const char* RightSep,const ta_strbc ele, const char* LeftSep);
extern  ta_strbc tasbc_cpyListEnd(ta_strbc dest,const char* RightSep,const char* ListTk,const char* LeftSep);
#if 0
/************************************************************************/
/************************************************************************/
/************************************************************************/ 
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
typedef void* ta_strnw; 
   
#define tasnw_null		0				/* the null or empty string (variable) */
#define TASNW_NULL		0				/* the null or empty string (static use) */
#define TASNW_STR(x)	(char *)(x)		/* the string of at basic string (ta_strbc). */
#define TASNW_FREE(x)	(char *)(x)		/* the string and free ta_strbc (except string).*/

#define TASNW_LEN1(x)	*((char *)x-2*sizeof(int))  /* number of allocated blocks */
#define TASNW_LEN2(x)	*((char *)x-sizeof(int))   /* length of string without the final 0. */

#define tasnw_free(d)	tasnw_cpy(d,0)

extern void tasnw_free_f(ta_strnw str);

extern ta_strnw tasnw_init(void);
   
extern ta_strnw tasnw_cpyString(ta_strnw dest,const char* src);   
extern ta_strnw tasnw_catString(ta_strnw dest,const char* src);  
   
/************************************************************************/
/*interface tasnw_cpy:                                                  */
/*type:	ta_strbc x ta_strbc -> ta_strbc                                 */
/*args:	une chaine source et une chaine de destination                  */
/*pre:	la taille de la destination doit etre suffisante pour y copier  */
/*      la chaine source sinon la fonction alloue la place m�moire      */
/*      n�cessaire                                                      */
/*post:	la chaine source est copi�e � l'emplacement d�sign� par dest    */
/************************************************************************/
extern ta_strnw tasnw_cpy(ta_strnw dest,const ta_strnw src);

/************************************************************************/
/*interface tasnw_cat:                                                  */
/*type:	ta_strbc x const ta_strbc -> ta_strbc                           */
/*args:	deux chaines � concat�ner                                       */
/*pre:	les deux chaines sont non nulles                                */
/*post:	renvoie la chaine dest � laquelle on a concat�n� la chaine sourc*/
/************************************************************************/
extern ta_strnw tasnw_cat(ta_strnw dest,const ta_strnw src);

/************************************************************************/
/*interface tasnw_catInt32:                                             */
/*type:	ta_strbc x int32 -> ta_strbc                                    */ 
/*args:	une chaine et un nombre de type int32                           */
/*pre:	                                                                */
/*post:	renvoie la chaine � laquelle on a concat�n� le nombre           */
/************************************************************************/
extern ta_strnw tasnw_catInt32(ta_strnw dest,int32 nb);

/************************************************************************/
/*interface tasbc_dup_f :                                               */
/*type:	ta_strbc -> ta_strbc                                            */
/*args:	une chaine                                                      */
/*pre:	la chaine est non nulle	                                        */
/*post:	duplique la chaine � un autre endroit                           */
/*	renvoie l'adresse de la chaine dupliqu�e                            */
/************************************************************************/
extern ta_strnw tasnw_dup_f(const ta_strnw str);

extern int tasnw_cmpbis_f(const ta_strnw p1,const ta_strnw* p2);
extern int tasnw_cmpbis_pt_f(const ta_strnw* p1,const ta_strnw* p2);

#define 		tasnw_cmp(s0,s1) 	    strcmp(TASNW_STR(s0),TASNW_STR(s1))
#define 		tasnw_cmpbis(s0,s1)     strcmpbis(TASNW_STR(s0),TASNW_STR(s1))
#define			tasnw_ConvToUp(DS)	strConvToUp(TASNW_STR(DS))
#define			tasnw_ConvToLow(DS)	strConvToLow(TASNW_STR(DS))

extern ta_strnw ta_FileReduceName(ta_strnw destsrc);

/************************************************************************/
/*interface  tasbc_cpyBasename :                                        */
/*type:	ta_strbc  x const ta_strbc x const char *-> ta_strbc            */
/*args:	une chaine de destination, une chaine source et une chaine      */
/*      representant un suffixe.                                        */
/*pre:	le suffixe est un suffixe de la chaine source.                  */
/*post:	ecrit dans la chaine de destination la chaine source a          */
/*      laquelle on a enlev� le suffixe et renvoie l'adresse de         */
/*      la nouvelle chaine cr��e.                                       */
/************************************************************************/
extern ta_strnw tasnw_cpyBasename(ta_strnw dest,
        const ta_strnw src,const char * suff);

/************************************************************************/
/*interface  tasbc_cpyDirname :                                         */
/*type:	ta_strbc x const ta_strbc -> ta_strbc                           */
/*args:	une chaine de destination et une chaine source.                 */
/*pre :	la source repr�sente le nom d'un r�pertoire	                    */
/*post:   recopie dans la destination le nom du r�pertoire ou un point  */
/************************************************************************/
extern ta_strnw tasnw_cpyDirname(ta_strnw dest,const ta_strnw src);

/************************************************************************/
/*interface tasbc_cpySuffix :                                           */
/*type:   ta_strbc x const ta_strbc -> ta_strbc                         */
/*args:   une chaine de destination ainsi qu'une chaine source          */
/*pre:    la chaine source comporte au moins un point                   */
/*post:   recopie dans la destination la chaine situ�e apr�s le dernier */
/*        point(en gardant ce point) ou rien s'il n'y a pas de point.   */
/************************************************************************/
extern ta_strnw tasnw_cpySuffix(ta_strnw dest,const ta_strnw src);

/************************************************************************/
/*interface tasbc_cpyWithoutSuff :                                      */
/*type:   ta_strbc x const ta_strbc -> ta_strbc                         */
/*args:   une chaine de destination ainsi qu'une chaine source          */
/*pre:    la chaine source comporte au moins un point                   */
/*post:   recopie dans la destination la chaine situ�e avant le dernier */
/*        point(en enlevant ce point) ou rien s'il n'y a pas de point.  */
/************************************************************************/
extern ta_strnw tasnw_cpyWithoutSuff(ta_strnw dest,const ta_strnw src);

/************************************************************************/
/*interface tasbc_cpyWithoutPost :                                      */
/*type:	ta_strbc x const ta_strbc x const char * -> ta_strbc            */
/*args:	une chaine de destination,une chaine source ainsi qu'une        */
/*      chaine repr�sentant unsuffixe de la source                      */
/*pre: 	le suffixe doit exactement suffixer la chaine source            */
/*post:	recopie dans la destination la chaine source � laquelle         */
/*      on a enlev� le suffixe sinon on copie int�gralement la chaine   */
/*      source dans la chaine de destination                            */
/************************************************************************/
extern ta_strnw tasnw_cpyWithoutPost(ta_strnw dest,
        const ta_strnw src,cstr post);

/************************************************************************/
/*interface tasbc_cpyWithoutPre :                                       */
/*type:	ta_strbc x const ta_strbc x const char * -> ta_strbc            */
/*args:	une chaine de destination,une chaine source ainsi qu'une        */
/*      chaine repr�sentant un prefixe de la source                     */
/*pre: 	le prefixe doit exactement prefixer la chaine source            */
/*post:	recopie dans la destination la chaine source � laquelle on a    */
/*      enlev� le prefixe sinon on copie int�gralement la chaine        */
/*      source dans la chaine de destination.                           */
/************************************************************************/
extern ta_strnw tasnw_cpyWithoutPre(ta_strnw dest,
            const ta_strnw src,cstr pre);


extern ta_strnw tasnw_cpyReplace_T(ta_strnw dest,
            const ta_strnw src,const char** table);
extern ta_strnw tasnw_cpyReplaceS_T(ta_strnw dest,
            const char* src,const char** table);

extern ta_strnw tasnw_cpyReplace_0(ta_strnw dest,
            const ta_strnw src, va_list ap);
extern ta_strnw tasnw_cpyReplace(ta_strnw dest,
            const ta_strnw src,...);
extern ta_strnw tasnw_cpyReplace_T(ta_strnw dest,
            const ta_strnw src,const char** table);

extern ta_strnw tasnw_cpyReplace_1_0(ta_strnw dest,
            const ta_strnw src, va_list ap);
extern ta_strnw tasnw_cpyReplace_1(ta_strnw dest,
            const ta_strnw src,...);
extern ta_strnw tasnw_cpyReplace_1_T(ta_strnw dest,
            const ta_strnw src, char**  table);


extern ta_strnw tasnw_cpyPrintf256_0(ta_strnw dest,cstr fmt,va_list ap);
extern ta_strnw tasnw_cpyPrintf256(ta_strnw dest,cstr fmt,...);

extern ta_strnw tasnw_cpyPrintfLG_0(int32 dest_lg,ta_strnw dest,
            const char * fmt, va_list ap);
extern ta_strnw tasnw_cpyPrintfLG(int32 dest_lg,
            ta_strnw dest, const char * fmt,...);

extern ta_strnw tasnw_catPrintf256_0(ta_strnw dest, cstr fmt, va_list ap);
extern ta_strnw tasnw_catPrintf256(ta_strnw dest, cstr fmt,...);

extern ta_strnw tasnw_catPrintfLG_0(int32 dest_lg,
            ta_strnw dest,const char * fmt, va_list ap);
extern ta_strnw tasnw_catPrintfLG(int32 dest_lg,
            ta_strnw dest,const char * fmt,...);

/*** formating handler. ***/
extern ta_strnw tasnw_cpyShiftR(ta_strnw str,int16 sh);
extern int16 tasnw_setShiftR(int16 shift);
extern char* tasnw_shiftR(ta_strnw* str);
#define TASTR_SH_DEFAULT	(~0)
#define TASTR_SH_TAB(i)		((1<<14)|(i))
extern ta_strnw tasnw_cpyListInit(ta_strnw dest,const char* RightSep,const char* ListTk, const char* LeftSep);
extern ta_strnw tasnw_cpyListAdd(ta_strnw dest,const char* ListToken,const char* RightSep,const ta_strnw ele, const char* LeftSep);
extern  ta_strnw tasnw_cpyListAddString(ta_strnw dest,const char* ListToken,const char* RightSep,const ta_strnw ele, const char* LeftSe);
extern  ta_strnw tasnw_cpyListEnd(ta_strnw dest,const char* RightSep,const char* ListTk,const char* LeftSep);

/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/ 
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
/************************************************************************/
#endif

/*** string set handler. ***/
extern ta_strbcWS* tastr_WorkSet_use(ta_strbcWS* pStrWS);
extern ta_strbcWS* tastr_WorkSet_new();
extern void tastr_WorkSet_free();
extern const char* tastr_WorkSet_getNoEat(const char * str);
extern const char* tastr_WorkSet_getEat(ta_strbc* str);

/*** Save/Load handler. ***/
extern void ta_SL_StringSaveObj(void* addr,ta_SL_Main* psl);
extern void ta_SL_StringLoadObj(void* addr,ta_SL_Main* psl);
extern void ta_SL_StringSaveRef(void* addr,ta_SL_Main* psl);
extern void ta_SL_StringLoadRef(void** addr,ta_SL_Main* psl);
extern void* ta_SL_StringNew(ta_SL_Main* psl);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif 

/************************************************************************/
#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif 

typedef const struct _ta_strdb* ta_strdb;

#define tasdb_null    0              /* the null or empty string (variable) */
#define TASDB_NULL    ((ta_strdb)0)  /* the null or empty string (static use) */
#define TASDB_STR(x)  ((const char *)(x))  /* the string.  */
#define TASDB_FREE(x) do{}while(0)  /* the string and free except string.*/
#define TASDB_LEN(x)  ( (x!=0) ? strlen(TASDB_STR(x)) : 0)/* length of string without the final 0. */

/*! \defgroup strhashtable string data base
 *  TA provides a string data base to associate data to
 *  string. The implentation is a fixed size table. The
 *  table store both the string and the associated data.
 *  The basic ussage is:
 *  \code
 *    // 2 string pointers
 *    char *p,*str:
 *    ...
 *    // init the table for 10000 strings
 *    tasdb_setHandler(0, 100000, 50, 8);
 *    ...
 *    // associate gnat to gnu
 *    str= tasdb_getDbstrAdd("gnu");
 *    strcpy(str-8,"gnat");
 *    ...
 *    p= ...
 *    ...
 *    // get value associated to p
 *    if ( tasdb_isStrdb(p) ) 
 *       printf("% s --> %s\n", p, p-8);
 *    else if ( str=tasdb_getDbstr(p) )
 *       printf(" %s --> %s\n", p, str-8);
 *    else
 *       printf(" %s --> undefined\n",p);
 *  \endcode 
**/

/*! \ingroup strhashtable
 *  \brief Hidden type defining a string data base. */
typedef struct _tastrdb_data* tastrdb_data;

/*! \ingroup strhashtable
 *  \brief Initialization of the string data base
 *
 * The \b tasdb_setHandler either initializes or changes the
 * current string data base depending on the \a h parameter.
 *
 * If the \a h parameter is not null, the function  sets
 * the current string data base to \a h.
 * The other parameter are ignored.
 * 
 * If the \a h parameter is null, a new string data base is created
 * using the \a snb, \a ssz  and \a dsz parameters.
 *   \arg snb: the maximum number of strings,
 *   \arg ssz: the average byte length of the strings,
 *   \arg dsz: the byte size of data associated to a string.
 *
 * The created string data base becomes the current one.
 * Notice that the amount of memory used by the data base is arround 
 * <tt> snb * ( ssz + dsz) </tt> bytes.
 *
 * The function returns the string data base which was the current
 * one before the call.
**/
extern tastrdb_data tasdb_setHandler(tastrdb_data h, int snb, int ssz, int dsz);

/*! \ingroup strhashtable
 * \brief get hash of a string.
 *
 * The \b tasdb_getHash function returns the hash code of the \a str string.
 * Furthermore it returns 0 if \a str is the null pointer.
**/
extern unsigned int tasdb_getHash(ta_cstr str);

/*! \ingroup strhashtable
 * \brief Test if a string is stored in the current string data base.
 *
 * The \b tasdb_isStrdb function returns true is the \a str string is
 * stored in the current string data base otherwise it returns false.
 * \n
 * This function must be handled carefully, it returns true if the str
 * belong to the address space of the data base and not if it is a string
 * of the data base. So if \p tasdb_isStrdb(x) is true,
 * \p tasdb_isStrdb(x+1) is probably true too.
**/
extern int tasdb_isStrdb(ta_cstr str);

/*! \ingroup strhashtable
 * \brief test if a string is present in the string data base.
 *
 * The \b tasdb_getDbstr function returns the pointer in the string data
 * base of the \a str string.
 * \n
 * If there is no string in the data base equal to the \a str string,
 * it returns the null pointer.
 * If \a str is null the function returns the null pointer.
**/
extern ta_strdb tasdb_getDbstr(ta_cstr str);

/*! \ingroup strhashtable
 * \brief test if a string is present in the string data base.
 *
 * The \b tasdb_getDbstrHash function returns the pointer in the string
 * data base of the \a str string assuming the hash code of the string is
 * \a hash. \n
 * If no string equal to the \a str string exists in the string data base,
 * the function returns the null pointer.
 * If \a str is null the function returns the null pointer.
**/
extern ta_strdb  tasdb_getDbstrHash(ta_cstr str, unsigned int hash);

/*! \ingroup strhashtable
 * \brief get the pointer of a string in the string data base.
 *
 * The \b tasdb_getDbstrAdd function returns the pointer in the string
 * data base of the \a str string. \n
 * If no string equal to the \a str exists in the string data base, it is
 * added and its associated data are set to null.
 * If \p str is null the function returns the null pointer.
**/
extern ta_strdb  tasdb_getDbstrAdd(ta_cstr str);

/*! \ingroup strhashtable
 * \brief get the pointer of a string in the string data base.
 *
 * The \b tasdb_getDbstrAddHash function returns the pointer in the
 * string data base of the \a str string assuming the hash code of
 * the string is \a hash. \n
 * If no string equal to the \a str string exists in the string data base,
 * it is added and its associated data are set to null.
 * If \a str is null the function returns the null pointer.
**/
extern ta_strdb  tasdb_getDbstrAddHash(ta_cstr str,
                    unsigned int hash);

/*** string data base.                                  ***/
/* data associated to db strings:                         */
/*   - there is no functions to get or store the data     */
/*     associated to a string.                            */
/*   - this data is stored in memory just before the      */
/*     the db string.                                     */
/*   - It is siseof(int) aligned and intitialized to null */
/*                                                        */
extern void         tasdb_stat(char* str, int oneORtwoLine);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

/*########################################################################
# Application :  Tools 'A'
# File        :  ./include/ta_noclone.h
# Revision    :  $Revision: 2.6.2.6 $
# Author      :  Ivan Aug� (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ########################################################################
# libta.a libtawd.a: library of Tools 'A'
# ########################################################################
#      - tanc:   no-clone set without reference counter
#      - tancr:  no-clone set with reference counter
#      - association of object & ident
# ######################################################################*/


#ifndef TA_NOCLONE_FILE_H
#define TA_NOCLONE_FILE_H

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/******************************************************************/
/***  Data Structures and Types                                 ***/

typedef void    (*tanc_DelFunc)(void*);
typedef int     (*tanc_CmpFunc)(void**,void**); 
typedef void    (*tanc_CpyFunc)(void*, const void*); 
typedef void* (*tanc_DupFunc)(const void*); 

/******************************************************************/
/***  tanc interface                                            ***/
/***                                                            ***/
/*** usage for a no-clone set of object of type X.              ***/
/***   void X_del(X*x)           { ... }                        ***/
/***   void X_cmp(X**x1,  X**x2) { ... }                        ***/
/***   void X_cpy(X*dest, X*src) { ... }                        ***/
/***   X*   X_dup(X*src)         { ... }                        ***/
/***                                                            ***/
/*** for a no-clone set without reference count:                ***/
/***   ncs= tanc_new(X_del,X_cmp);                              ***/
/*** [ tanc_SetCpy(ncs,X_cpy,sizeof(X)); ]                      ***/
/*** [ tanc_SetDup(ncs,X_dup); ]                                ***/
/***   px0= ....                                                ***/
/***   px0= tanc_NewEat(ncs,px0);                               ***/
/***   px1= .... // a) same as px0                              ***/
/***   px1= tanc_NewEat(ncs,px1);                               ***/
/*** at this point px0==px1 and object created in a) has been   ***/
/*** freed.                                                     ***/
/***   tanc_Delete(ncs,px1); // no effect                       ***/
/***   X x2;                                                    ***/
/***   px2= tanc_NewNoEat(ncs,&x2);                             ***/
/*** at this point if x2 is not in ncs, x2 is duplicated (using ***/
/*** X_dup or X_cpy) and the duplication is returned and stored ***/
/*** in ncs. If x2 is already in ncs, its image is returned.    ***/
/***   tanc_free(ncs);                                          ***/
/*** All the objects are delete (using the X_del) and the set   ***/
/*** too.                                                       ***/
/***                                                            ***/
/*** Note:                                                      ***/
/***   - to compare object X_cmp is used.                       ***/
/***   - to free object X_free is used.                         ***/
/***   - X_dup, X_cpy are needed by tanc_NewNoEat.              ***/
/***   - only one of them (X_dup, X_cpy) is needed.             ***/
/***                                                            ***/
/*** for a no-clone set with reference count:                   ***/
/***    It's like the former. But for each object in ncs there  ***/
/***    a counter. It is set to 1 when object is added to the   ***/
/***    set, incremented when it's returned by anc_NewEat or    ***/
/***    tanc_NewNoEat, decremented by tanc_Delete. When it      ***/
/***    reaches 0, the object is deleted.                       ***/
/***                                                            ***/
/*** Identifier use in set without reference count:             ***/
/***   px= tanc_NewEat(...                                      ***/
/***   tanc_SetId(ncs,px,"Ident of px");                        ***/
/*** No tanc_GetById(ncs,"Ident of px") returns px.             ***/
/***                                                            ***/
/*** Identifier use in set with reference count:                ***/
/***   It's the same but the reference counter associated to    ***/
/***   to px is incremented.                                    ***/
/***   So the usual sequency to put an identified object into   ***/
/***   ncs is:                                                  ***/
/***    px= tanc_NewEat(ncs,...)  // or tanc_NewNoEat(ncs,...)  ***/
/***    tanc_SetId(ncs,px,"id");  // cnt of px +2               ***/
/***    tanc_Delete(ncs,px);      // cnt of px +1               ***/

extern ta_noclone* tanc_new(tanc_DelFunc del,tanc_CmpFunc cmp);
extern ta_noclone* tancr_new(tanc_DelFunc del,tanc_CmpFunc cmp);
extern void tanc_SetCpy(ta_noclone* pnc,tanc_CpyFunc cpy,int16 sz);
extern void tanc_SetDup(ta_noclone* pnc,tanc_DupFunc dup);
extern void tanc_free(ta_noclone* pnc);

extern void* tanc_search(const ta_noclone* pnc,const void* pobj);
extern const void* tanc_NewEat(ta_noclone* pnc,void* pobj);
extern void* tanc_NewNoEat(ta_noclone* pnc,const void* pobj);
extern void tanc_Delete(ta_noclone* pnc,void* pobj);
extern void tanc_SetId(ta_noclone* pnc, const void* pobj,const char* id);
extern const void* tanc_GetById(const ta_noclone* pnc,const char* id);
extern const char* tanc_GetByName(const ta_noclone* pnc,const void* pobj);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

#if defined(FILE_CPP) || defined(__cplusplus)
template<class OBJECT>
class TA_Noclone {
public:
	TA_Noclone(
		void (*DelFunc)(OBJECT*pobj),
		int  (*CmpFunc)(OBJECT**pl, OBJECT**pr),
		ta_boolean WithRefCnt=TRUE
	) { if (WithRefCnt)
			val= tancr_new((tanc_DelFunc)DelFunc,(tanc_CmpFunc)CmpFunc);
		else
			val= tanc_new((tanc_DelFunc)DelFunc,(tanc_CmpFunc)CmpFunc);
	  }
	~TA_Noclone()	                           { tanc_free(val); }
	void SetDup(OBJECT* (*DupFunc)(const OBJECT*))
	  { tanc_SetDup(val,(tanc_DupFunc)DupFunc); }
	
	OBJECT*     search(const OBJECT*pobj) const
	  { return (OBJECT*) tanc_search(val,(const void*)pobj); }
	ta_noclone* getSet()	                   { return val; }
	OBJECT*     NewEat  (      OBJECT*pobj)
	  { return (OBJECT*) tanc_NewEat(val,pobj); }
	OBJECT*     NewNoEat(const OBJECT*pobj)
	  { return (OBJECT*) tanc_NewNoEat(val,(const void*)pobj); }
	void        Delete  (      OBJECT*pobj) { tanc_Delete(val,pobj); }
	void        SetId   (const OBJECT* pobj, const char*id)
	  { tanc_SetId(val,(const void*)pobj,id); }
	OBJECT*     GetById (      char*id)
      { return (OBJECT*) tanc_GetById(val,id); }

private:
	ta_noclone*		val;
};
#endif

#endif

#if 0

/*########################################################################
# Application :  Tools 'A'
# File        :  ./include/ta_error.h
# Revision    :  $Revision: 2.6.2.6 $
# Author      :  Ivan Aug� (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ########################################################################
# libta.a libtawd.a: library of Tools 'A'
# ########################################################################
# ######################################################################*/


#ifndef FILE_TA_ERROR_H
#define FILE_TA_ERROR_H

#ifdef VAR_FILE_TA_ERROR_H
#	define EXTERN
#else
#	define EXTERN extern
#endif

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/****************************************************************/
/* 	Cette structure elementaire decrit les donnees d'un message	*/
/* d'erreur d'une table composite.								*/

typedef struct _ta_ErrDef {
	flag16	err_flag;	/* type of error see ERR_xxx flags */
	int16	err_num;	/* num of error (index in table) */
	char*	err_mess;	/* format of error */
} ta_ErrDef; 

/****************************************************************/
/* 	Cette structure elementaire decrit la position d'une erreur	*/
/* dans un fichier.												*/

typedef struct _ta_ErrPos {
	int16	file;	/* identifier of file */
	int16	inst;	/* instruction in line */
	int32	line;	/* line in the file */
} ta_ErrPos;

/****************************************************************/
/* flag de definition des attribus des erreurs.					*/

#define ERR_NSRC  0x8000		/* affiche ni file, ni line.    */
#define ERR_NTYP  0x4000		/* aff. pas type.				*/
#define ERR_NNUM  0x2000		/* aff. pas num.				*/
#define ERR_NTYPNUM (ERR_NTYP|ERR_NNUM)
#define ERR_NHEAD   (ERR_NSRC|ERR_NTYP|ERR_NNUM)

#define ERR_MESS  0x02			/* specification de message */
#define ERR_WARN  0x04			/* specification d'avertissement */
#define ERR_ERROR 0x08			/* specification d'erreur */
#define ERR_STOP  0x10			/* flag d'arret si erreur fatale */
#define ERR_FATAL (ERR_ERROR|ERR_STOP)/* specif. erreur fatale */ 
#define ERR_LEVEL (ERR_MESS|ERR_WARN|ERR_ERROR|ERR_STOP)
 
/****************************************************************/
/*	Les erreur standart.										*/
#define ERM_STR		     0	/* "%s"							*/
#define ERW_STR		     1	/* "%s"							*/
#define ERE_STR		     2	/* "%s"							*/
#define ERF_STR		     3	/* "%s"							*/
#define ERM_SYS		     4	/* "SYSTEM : %s"				*/
#define ERW_SYS		     5	/* "SYSTEM : %s"				*/
#define ERE_SYS		     6	/* "SYSTEM : %s"				*/
#define ERF_SYS		     7	/* "SYSTEM : %s"				*/

#define ERF_NOMEM	    10	/* "no more memory"				*/
#define ERF_TOOMANY     11	/* "too many %s objects"		*/
#define ERE_OFILE       12	/* "can't open file %s"			*/
#define ERF_OFILE	    13	/* "can't open file %s"			*/
#define ERE_ODIR        14	/* "can't open file %s"			*/
#define ERF_ODIR        15	/* "can't open file %s"			*/
#define ERE_IO          16	/* "i/o error"					*/
#define ERF_IO          17	/* "i/o error"					*/
#define ERF_IOFILE      18	/* "i/o error with file %s"		*/
#define ERF_IODIR       19	/* "i/o error with dir %s"		*/

#define ERE_SYNTAX      20	/* "syntax error at or near "%s"" */
#define ERF_SYNTAX      21	/* "syntax error at or near "%s"" */
#define ERW_IdTrunc     22	/* "%s identificator truncated to %s" */

#define ERF_EnvVarNeed	30	/* "%s" must be defined */
#define ERF_LNZERO	    31	/* "SYSTEM : use of 0 for logarithm" */
#define ERF_InvOptComb2	32	/* %s0 & %s1 are exclusive switches */

#define ERF_TOOMANYERR	40	/* "too many errors 		*/
#define ERF_CANTCONT	41	/* "can't continue"			*/
#define ERW_ENDMESS     42	/* "error=%d warn=%d mess=%d"	*/
#define ERF_InfoLineLen	43	/* "line of file too long */
#define ERW_InfoInvalid	44	/* "invalid line into info file */

#define ERF_MAGIC       50	/* "uncompatible magic numbers" */
#define ERE_UKIND	    51	/* " "%d" unknown kind"			*/
#define ERF_UKIND	    52	/* " "%d" unknown kind"			*/


/****************************************************************/
/* the ta_error variable used to give parameters to the print	*/
/* handler.	steps for printing an error message are:			*/
/*   1) init the ta_error structure								*/
/*   2) call the taerr_print function.							*/
/* Note that after the call of taerr_print function, the		*/
/* ta_error variable is cleared.								*/

#define TAERR_IMAX 5
#define TAERR_BUFSZ 5000
typedef struct _ta_ErrPrint {
	char   buffer[TAERR_BUFSZ];	/* where error string is constructed*/
	long   val[TAERR_IMAX];		/* for %[-][0..9]d/x[0..IMAX-1]		*/
	double e[TAERR_IMAX];		/* %[-][0..9][.][0..9]e/f[0..IMAX-1]*/
	const char *s[TAERR_IMAX];	/* %[0..9]s[0..IMAX-1]				*/
	struct _ta_ErrPrintNuple {
		long	l0,l1,l2,l3;
	}     nuple[TAERR_IMAX];	/* for user format					*/
} ta_ErrPrint;

EXTERN ta_ErrPrint ta_error;
   

/****************************************************************/
/* 	Functions of error handler changing the configuration.		*/

extern void taerr_init(ta_ErrDef * table,flag32 head_format);
extern void taerr_end();
extern ta_boolean taerr_SetPrintWarn(ta_boolean pr);
extern ta_boolean taerr_SetPrintMess(ta_boolean pr);
extern void taerr_SetErrMax(int16 nb);
extern void taerr_SetUserLogNum(int16 nb);

extern void taerr_SetOutputExit(void (*p)(),void (*e)());
extern int32 taerr_GetNbError();
extern int32 taerr_GetNbWarn();
extern int32 taerr_GetNbMess();
extern void taerr_AddUserFormat(char ident,char* format);


/****************************************************************/
/* 	Function of error handler changing the position.			*/

extern void taerr_InitPos3(ta_ErrPos* ppos,const char* file,int32 line,int16 inst);
extern void taerr_GetDefaultPos(ta_ErrPos* ppos);
extern ta_boolean taerr_IsDefaultPos(ta_ErrPos* ppos);
extern void taerr_GetCurrPos(ta_ErrPos* ppos);
extern void taerr_SetCurrPos(const ta_ErrPos* ppos);
extern void taerr_SetCurrPos3(const char* file,int32 line,int16 inst);
extern void taerr_SetCurrTable(ta_ErrDef* table);
extern void taerr_IncrCurrPosLine();
extern char* taerr_FileNameOfPos(ta_ErrPos* ppos);
extern char* taerr_FileNameOfFile(int16 file);

extern void taerr_PushSetCurrPos(const ta_ErrPos* ppos);
extern void taerr_PushSetCurrPosF(FILE* pfile,const ta_ErrPos* ppos);
extern void taerr_PushSetCurrPos3(const char* file,int32 line,int16 inst);
extern void taerr_PushSetCurrPos3F(FILE* pfile,const char* file,int32 line,int16 inst);
extern FILE* taerr_PopCurrPos();

/****************************************************************/
/* 	Functions of error handler for printing messages.			*/

extern void taerr_print(flag16 ErrIndex,int16 table,flag32 HeaderKind, ta_ErrPos * ppos);
extern void taerr_std(flag16 ErrIndex);
extern void taerr_spc(flag16 ErrIndex);
extern void taerr_EndMess();
extern void taerr_EndWarn();

/****************************************************************/
/* 	Miscellaneous functions.									*/

extern int32 taerr_GetFileIdFromFile(const char * file);
extern const char* taerr_GetFileFromFileId(int32 id);

/****************************************************************/
/* 	Functions of error handler for printing basic messages.		*/

/* permanent watch dog */
#define taerr_pWD(num,str) { ta_ErrPos p; \
                             taerr_InitPos3(&p,__FILE__,__LINE__,0); \
                             ta_error.s[0]=str; \
                             taerr_print(num,0,ERR_NNUM,&p); }

/* optional watch dog */
#ifdef TA_WATCHDOG
#define taerr_oWD(num,str) taerr_pWD(num,str)
#else
#define taerr_oWD(num,str) 
#endif

/* permanent conditionnal watch dog */
#define taerr_cpWD(c,num,str) if ( c ) taerr_pWD(num,str)

/* optionnal conditionnal watch dog */
#ifdef TA_WATCHDOG
#define taerr_coWD(c,num,str) if ( c ) taerr_pWD(num,str)
#else
#define taerr_coWD(c,num,str) 
#endif

#define taerr_pWDm(str)		taerr_pWD(ERM_SYS,str)
#define taerr_pWDw(str)		taerr_pWD(ERW_SYS,str)
#define taerr_pWDe(str)		taerr_pWD(ERE_SYS,str)
#define taerr_pWDf(str)		taerr_pWD(ERF_SYS,str)

#define taerr_oWDm(str)		taerr_oWD(ERM_SYS,str)
#define taerr_oWDw(str)		taerr_oWD(ERW_SYS,str)
#define taerr_oWDe(str)		taerr_oWD(ERE_SYS,str)
#define taerr_oWDf(str)		taerr_oWD(ERF_SYS,str)

#define taerr_cpWDm(c,str)	taerr_cpWD(c,ERM_SYS,str)
#define taerr_cpWDw(c,str)	taerr_cpWD(c,ERW_SYS,str)
#define taerr_cpWDe(c,str)	taerr_cpWD(c,ERE_SYS,str)
#define taerr_cpWDf(c,str)	taerr_cpWD(c,ERF_SYS,str)

#define taerr_coWDm(c,str)	taerr_coWD(c,ERM_SYS,str)
#define taerr_coWDw(c,str)	taerr_coWD(c,ERW_SYS,str)
#define taerr_coWDe(c,str)	taerr_coWD(c,ERE_SYS,str)
#define taerr_coWDf(c,str)	taerr_coWD(c,ERF_SYS,str)

#define taerr_pUWDm(str)	{ ta_error.s[0]=str;taerr_std(ERM_STR); }
#define taerr_pUWDw(str)	{ ta_error.s[0]=str;taerr_std(ERW_STR); }
#define taerr_pUWDe(str)	{ ta_error.s[0]=str;taerr_std(ERE_STR); }
#define taerr_pUWDf(str)	{ ta_error.s[0]=str;taerr_std(ERF_STR); }

#undef EXTERN
#undef VAR_FILE_TA_ERROR_H

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

#endif	/* FILE_TA_ERROR_H */


#endif

/*########################################################################
# Application :  Tools 'A'
# File        :  ./include/ta_misc.h
# Revision    :  $Revision: 2.6.2.6 $
# Author      :  Ivan Aug� (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ########################################################################
# libta.a libtawd.a: library of Tools 'A'
# ########################################################################
# - arithmetic and conversion routines
# - pscanf
# - routines about file
# - routines to read configuration files
# ######################################################################*/


#ifndef FILE_TA_MISC_H
#define FILE_TA_MISC_H


#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/*** functions of ta_arith.c ***/
extern int32 arit_bsizeof(int32 xx);

extern flag32 arit_htou(register const char * p);
extern flag32 arit_otou(register const char * p);
extern flag32 arit_btou(register const char * p);

extern char* marit_utoh32(register char * p,const flag32* pval,int16 sz);
extern char* marit_utohxx(register char * p,const ta_uintxx* pval,int16 sz);
extern ta_boolean marit_cmp32(const flag32* pval0,const flag32* pval1,int16 sz);
extern ta_boolean marit_cmpxx(const ta_uintxx* pval0,const ta_uintxx* pval1,int16 sz);

/***********************************************************************/
/*** handler for parsing the arguments of the command line           ***/

/*!
 * \defgroup taclp  parsing the arguments of the command line
 * \ingroup tamisc
 * @{
**/

typedef struct _taclp_data taclp_data;

/*!
 * \brief Creates the current CLP.
 *
 * The \b taclp_init function creates the current CPL (Command Line Parser) 
 * and initializes it for parsing the command line that is defined
 * by the \p argc and \p argv parmeters.
 * \n\n
 * The \b taclp_init_EH is similar to the \b taclp_init but 
 *   - it also initializes the EH module setting 
 *     the default error level to \p level,
 *     and the name that is printing in error message to the basename
 *     of \p arg[0].
 *   - if \p withHVQ is not null, the default \em help, \em verbose
 *     and \em silence switches (see \ref taclp_configure) are added
 *     to the current CLP.
 *     Furthermore, when the command line is parsed,
 *     the error level of the EH module is automatically set depending of
 *     the verbose and silence switches.
 *
 * \param argc The number of strings that the \p argv array contains.
 * \param argv The argument strings.
 *      The first string (eg: argv[0]) is not an argument but the program
 *      name and must be defined (eg: different of null pointer).
 *      \p argv array must be null terminated (eg: argv[argc]==0).
 * \param level The error level for the EH module.
 * \param withHVS If it is not null then the standard \em help, \em verbose, \em
 *      silence switches are added to the current CLP.
**/
extern void taclp_init(int* argc, const char** argv);
/*! 
 * \brief Creates the current CLP and initializes the EH modules.
 * \copydetails taclp_init(int*,char**)
**/
extern void taclp_init_EH(int* argc, const char** argv,
                        taeh_level level, int withHVQ);

/*! 
 * \brief Configures the current CLP.
 *
 * The \b taclp_configure function allows to configure the current CLP.
 * The \p code parameter is a request code such as \ref TACLP_CFG_UsageAuto,
 * \ref TACLP_CFG_UsageStream.
 * The other parameters depend of the request.
 * The supported requests are presented in the \ref taclpconfigure section.
 *
 * \param code The asked request. The request are defined by the TACLP_CFG_...
 *             macros.
 * \param ...  The parameters of the request.
**/
extern void taclp_configure(ta_uint32 code, ...);
#define TACLP_CFG_UsageAuto     1
#define TACLP_CFG_UsageStream   2
#define TACLP_CFG_HelpAuto     11
#define TACLP_CFG_HelpStream   12
#define TACLP_CFG_HelpStrArg   13
#define TACLP_CFG_HelpStrMan   14
#define TACLP_CFG_HelpSw       16
#define TACLP_CFG_QuietSw      20
#define TACLP_CFG_VerbSw       21
#define TACLP_CFG_VerbGet      22
#define TACLP_CFG_VerbAuto     23

/*!
 * \brief Frees memory allocated by the CLP module.
 *
 * The \b taclp_free frees all the memory that the current CLP has allocated.
 * The only exception is the memory allocated for the switch arguments
 * that are defined as array that are not freed by default.
**/
extern void taclp_free();

/*!
 * \brief Adds a switch.
 *
 * The \b taclp_addSwitch function adds to the current CLP the description
 * of the switch given by the arguments.
 *
 * \param format The string that describes the switch,
 *      see the section \ref taclpformat for more information.
 * \param present When the command line is parsed by a function
 *     like \ref taclp_parse,
 *     the pointer to integer will be set to number of times the switch 
 *     is present.
 *     If this information is not required, \p present can be set to the null
 *     pointer.
 * \param ... Pointers that give the location where the switch parameters
 *     (defined in the \p format string) are stored when command the line
 *     is parsed.
 *     If the value of a switch parameter is not required, the corresponding
 *     pointer can be set to the null pointer.
**/
extern void taclp_addSwitch(ta_cstr format, int* present, ...);

/**
 * \brief Parse the command line.
 *
 * The \b taclp_parse function parses the command line of the current CLP.
 *
 * \return
 * The \b taclp_parse function returns 0, 1 or -1 or may exit with a
 * status of 0 or 1.
 *   - returns 0 :
 *      The command line is parsed successfully and the switch parameters
 *      (given in the \ref taclp_addSwitch function) are set.
 *      Furthermore it indicates that no switch flagged \qiaem{<h>}
 *      was invoked in the command line.
 *   - returns 1 :
 *      A switch flagged \qiaem{<h>} is invoked in the command line
 *      and the global option \em Automatic-Help is unset.
 *      Only the switch parameters (given in the \ref taclp_addSwitch function)
 *      corresponding to this switch) are updated.
 *   - returns -1 :
 *      The command line is incorrect,
 *      and no switch flagged \qiaem{<h>} was detected in the command line,
 *      and the global option Automatic-Usage is unset.
 *   - exits with status 0 :
 *      A  switch flagged \qiaem{<H} is invoked in the command line,
 *      and the global option \em Automatic-Help is set.
 *      In this case the help message is printed before exiting.
 *   - exits with status 1 :
 *      The command line is incorrect,
 *      and no switch flagged \qiaem{<h>} was detected in the command line,
 *      and the global option \em Automatic-Usage is set.
 *      In this case error messages and finally the usage message are
 *      printed before exiting.
**/
extern int taclp_parse();

/*!
 * \brief Prints the usage message.
 *
 * The \b taclp_printUsage function prints into the \p stream file the
 * usage message.
 * \n\n
 * The \b taclp_printHelp function prints into the \p stream file the
 * help message. The format of the help message is:
\verbatim
Usage: <usage-string>

<man-string>

Options:
  <switches> <args> : <switch-description>
  <switches> <args> : <switch-description>
  ...
\endverbatim
 *
 * \param stream The file where the messages is written.
 *      A null pointer indicates to use the default stream
 *      (see \ref taclp_configure).
 *       
**/
extern void taclp_printUsage(FILE* stream);
/*!
 * \brief Prints the help message.
 * \copydetails taclp_printUsage
**/
extern void taclp_printHelp(FILE* stream);

/**@}**/

/*** obsolete handler ***/
extern int ta_pscanf_AddSwitch_0(const char * format, va_list ap);
extern int ta_pscanf_AddSwitch(const char * format,...);

extern int  ta_pscanf_0(char ** argv,const char * format, va_list ap);
extern int  ta_pscanf(char ** argv,const char * format,...);
extern void ta_pscanf_getUsageString(char* dest);


/***********************************************************************/
/*** file utilities                                                  ***/

/*! \defgroup tafile some file utilies */

/*! \ingroup tafile
 * \brief Directory search path (hidden type).
 *
 * The \b ta_searchpath type define a directory search path,
 * it is a hiden type, it must be initialized by the 
 * \ref tasp_create function before usage.
**/
typedef struct _ta_searchpath* ta_searchpath;

/*! \ingroup tafile
 * \brief Default directory search path.
 *
 * The \b gl_ta_searchpath variable is a global variable, it
 * defined the default directory search path, the miscellaneous
 * ta handler uses to open stat files.
 * It can be null.
**/
extern ta_searchpath gl_ta_searchpath;

/*! \ingroup tafile
 * \brief Create a directory search path.
 *
 * The \b tasp_create functions creates a directory search path
 * and returns it.
 * The created path is defined by the \p sp variable and
 * the \p rdir directory indicates the top directory of
 * the relative directory names of \p sp.
 * \n
 * Notice that searching for file with a such defined path is
 * independent of the current working directory.
 * \param rdir The top directory used to resolve the relative
 *        names of \p sp. If it is null, the current working directory
 *        is used.
 * \param sp A list of directory names separated by colons (i.e. ":").
**/
extern ta_searchpath tasp_create(ta_cstr rdir, ta_cstr sp);

/*! \ingroup tafile
 * \brief Free allocated ressources
 *
 * The \b tasp_free function free all ressources used by the \p sp
 * search path. If \p sp is null then nothing is done, if the search
 * path is \ref gl_ta_searchpath then this global variable is set
 * to null after freeing.
 * \param sp A search path or null.
**/
extern void tasp_free(ta_searchpath sp);

/*! \ingroup tafile
 * \brief Search for a file using the global search path
 *
 * The \b tasp_stat function searches for the \p fn file using
 * the global search path defined by \ref gl_ta_searchpath.
 * for the relative name.
 * \n
 * If the \ref gl_ta_searchpath pointer is null, then the function
 * stats the \p fn filename such as.
 * \n
 * The function  returns -1 if the file is not found and 0 otherwise.
 * In this last case the \p ap_buffer and \p st_buffer buffers are
 * filled respectively with the absolute path of the file
 * and its information (same as standard stat function).
 * The returned absolute path is reduced with the
 * \ref taf_reduceNameStr function.
 * \n
 * When the byte size of a filename including the terminating
 * null byte is greater than "sizeof(ta_path)", the file is ignored
 * and a message is printed using the \ref taeh at level \ref TAEH_ERROR. 
 * So if the \p ap_buffer is not null, its byte size must be at least
 * "sizeof(ta_path)" bytes.
 * \n
 * \param fn   the file name to search for.
 * \param ap_buffer the buffer to get the absolute path of the file name.
 * \param st_buffer the buffer to get file information.
**/
extern int tasp_stat(
    ta_cstr fn, ta_path ap_buffer, struct stat* st_buffer);

/*! \ingroup tafile
 * \brief Search for a file using a search path
 *
 * The \b tasp_statSP function is similar to the \ref tasp_stat
 * function but it uses the \p sp search path instead of the
 * global search path.
 * \param sp   the search path to use.
 * \param fn   the file name to search for.
 * \param ap_buffer the buffer to get the absolute path of the file name.
 * \param st_buffer the buffer to get file information.
**/
extern int tasp_statSP(ta_searchpath sp,
    ta_cstr fn, ta_path ap_buffer, struct stat* st_buffer);

/*! \ingroup tafile
 * \brief Get the relative path of file.
 *
 * The \b tasp_getRelativePath function computes the relative path
 * of the \p fn filename using the relative directory of the
 * global search path. The relative path is copied into the
 * buffer given by the rp_buffer.
 *
 * On successful completion, the \b tasp_getRelativePath function returns
 * the number of characters writen into the \p rp_buffer buffer without
 * including the terminating null (eg: strlen(rp_buffer).
 * Otherwise the function returns -1, this occurs
 * when the global search path is undefined
 * or when fn is not an absolute path
 * or when the byte size of the relative path including the
 * terminating null byte is greater than "sizeof(ta_path)".
 *
 * \param fn   the absolute path of a file name.
 * \param rp_buffer the buffer to get the relative path of the file name.
**/
extern int tasp_getRelativePath(ta_path rp_buffer, ta_cstr fn);

/*! \ingroup tafile
 * \brief Get the relative path of file.
 *
 * The \b tasp_getRelativePathSP function is similar to the
 * \ref tasp_getRelativePath function but it uses the \p sp search path
 * instead of the global search path.
 * \param sp   the search path to use.
 * \param fn   the absolute path of a file name.
 * \param rp_buffer the buffer to get the relative path of the file name.
**/
extern int tasp_getRelativePathSP(ta_searchpath sp,
    ta_path rp_buffer, ta_cstr fn);

/* ! \ingroup tafile
 * \brief Open a file using \ref gl_ta_searchpath
 *
 * The \b tasp_fopen function returns the stream of the
 * \p fn file name according to \p mode.
 * If the \p fn file name is an absolute path then the function
 * is similar to the standard fopen function,
 * if the \p fn file name is a relative path and the
 * \ref gl_ta_searchpath is null then the function
 * is also similar to the standard fopen function, 
 * otherwise (relative path and \ref gl_ta_searchpath defined)
 * the following occurs:
 *   - open in read only mode: The \ref gl_ta_searchpath is used
 *     to locate the file.
 *   - open in write only mode:  It is open in
 *     the relative directory defined by the global search path
 *     if it is not null, otherwise in the current directory.
 *   - open in read/write mode: The \ref gl_ta_searchpath is used
 *     to locate the file. If it is not found, it is open as
 *     in write only mode (see just above).
 * \param fn   the file name to open.
 * \param mode same as fopen function.
extern FILE* tasp_fopen(ta_cstr fn, ta_cstr mode);
**/

/* ! \ingroup tafile
 * \brief Open a file using a ta_searchpath
 *
 * The \b tasp_fopenSP function is similar to the \ref tasp_fopen
 * function but it uses the \p sp search path instead of the
 * global search path.
 * \param sp   the search path to use.
 * \param fn   the file name to open.
 * \param mode same as fopen function.
extern FILE* tasp_fopenSP(ta_searchpath sp, ta_cstr fn, ta_cstr mode);
**/

// START 
/*! \ingroup tafile
 * \brief  gives the cannonical form of a path
 *
 * The \b taf_reduceNameStr() function reduces the \p file path applying the
 * next rules:
 *  - /./ are suppressed									
 *  - xx/../ are suppessed								
 *  - // are suppressed									
 *  - . is returned on null string.						
 *  .
 *
 * The reduced path is rewritten in the memory buffer pointed to by \p file. 
 * Notice that symbolic links are not treated and that the file might not
 * exist.
 * 
 * \return
 *   The function returns always the \p file pointer.
 * \param file   the path of a file
**/
extern char* taf_reduceNameStr(char* file);

/*! \ingroup tafile
 * \brief  gives the cannonical form of a path
 *
 * The \b taf_reduceNameSbc() function is similar to the \ref taf_reduceNameStr()
 * function.
**/
extern ta_strbc taf_reduceNameSbc(ta_strbc file);

/*! \ingroup tafile
 * \brief checks if a file exists
 *
 * The \b taf_doesFileExistStr() function returns TRUE if the \p file 
 * given in argument exists,FALSE otherwise
 * Note that TRUE is defined as the int 1 and FALSE the int 0
 *
 * \param file   the file to be checked
 * \return 
 *  The function returns an int which corresponds to the existence (or not)
 *  of the file
 *
**/
extern int taf_doesFileExistStr(const char* file);

/*! \ingroup tafile
 * \brief checks if the file exists
 * 
 * The \b taf_doesFileExistSbc() is similar to \ref taf_doesFileExist()
 * function
 *
 * \param file   the search path to use.
 * \return 
 *  The function returns TRUE of FALSE according to the existence of the file
**/
extern int taf_doesFileExistSbc(const ta_strbc file);

/*! \ingroup tafile
 * \brief checks if the file exists AND is a directory
 *
 * The \b taf_doesDirExistStr() function returns TRUE if the \p file given in
 * argument exists and is a directory, FALSE otherwise
 * Note that TRUE is defined ad the int 1 and FALSE as the int 0
 *
 *
 * \param file   the file to be checked
 * \return
 *  The function returns TRUE or FALSE according to the existence (or not)
 *  of the file
 *
**/
extern int taf_doesDirExistStr(const char* file);

/*! \ingroup tafile
 * \brief checks if the file exists AND is a directory
 *
 * The \b taf_doesDirExistSbc() function is similar to \ref 
 * taf_doesDirExistStr() function
 * 
 * \param file the file to be checked
 * \return
 *  The function returns TRUE or FALSE according to the existence of the 
 *  directory
**/
extern int taf_doesDirExistSbc(const ta_strbc file);

/*! \ingroup tafile
 * \brief checks if the file exists AND is regular
 *  
 * The \b taf_doesFileRegularStr() function returns TRUE if the \p file given
 * in argument exists and is regular, FALSE otherwise
 * Note that TRUE is defined as the int 1 and FALSE as the int 0
 * You can find more informations on what regular is by typing man fstat and
 * looking the "kind"
 *
 * \param file the file to be checked
 * \return 
 *  The function returns TRUE or FALSE according to the existence of the file 
 *  AND its regularity
**/
extern int taf_doesFileRegularStr(const char* file);

/*! \ingroup tafile
 * \brief checks if the file exists AND is regular
 *
 * The \b taf_doesFileRegularSbc() function returns is similar to \ref
 * taf_doesFileRegularStr() function
 *
 * \param file the file to be checked
 * \return 
 *  The function returns TRUE or FALSE according to the existence of the file 
 *  AND its regularity
**/
extern int taf_DoesFileRegularSbc(const ta_strbc file);

/*! \ingroup tafile
 * \brief checks if file exists AND if it is a directory, tries to create
 * directory if it does not exist 
 *
 * The \b taf_doesDirExistStr_CreateDir() function returns TRUE if
 * - the file exists AND it is a directory
 * - the file does not exists but the function manage to create the directory
 * .
 * In other situations, returns FALSE
 * Note that TRUE is defined ad the int 1 and FALSE as the int 0
 *
 * \param file the file to be checked
 * \return
 *  The function returns TRUE or FALSE according to conditions
 *  see description to have all the informations
**/
extern int taf_doesDirExistStr_CreateDir(const char* file);


/*! \ingroup tafile
 * \brief checks if file exists AND if it is a directory, tries to create
 * directory if it does not exist 
 *
 * The \b taf_doesDirExistSbc_CreateDir() function is similar to \ref 
 * taf_doesDirExistStr_CreateDir function 
 *
 * \param file   the file to be checked/built
 * \return
 *  The function returns TRUE or FALSE according to some tests done
 *  see description to have all the informations
**/
extern int taf_doesDirExistSbc_CreateDir(const ta_strbc file);
#define ta_FileIsDir_CreateDir taf_doesDirExistSbc_CreateDir

/*! \ingroup tafile
 * \brief checks if file exists, if not tries to create the entire path by 
 * making the missing directories
 * 
 * The \b taf_doesDirExistStr_CreatePath() : 
 * - returns TRUE if the file exists and is a directory
 * - if it does not exist, tries to create by creating all the directories
 * of the given path when they do not already exist, on success TRUE 
 * is returned, FALSE otherwise
 * Note that TRUE is defined ad the int 1 and FALSE as the int 0
 *
 * \param file the file to be checked/built
 * \return 
 *  The function returns TRUE or FALSE according to some tests done
 *  see description to have all the informations
**/
extern int taf_doesDirExistStr_CreatePath(const char* file);

/*! \ingroup tafile
 * \brief checks if file exists, if not tries to create the entire path by 
 * making the missing directories
 * 
 * The \b taf_doesDirExistSbc_CreatePath() is similar to \ref
 * taf_doesDirExistStr_CreatePath() function
 *
 * \param file the file to be checked/built
 * \return 
 *  The function returns TRUE or FALSE according to some tests done
 *  see description to have all the informations
**/
extern int taf_doesDirExistSbc_CreatePath(const ta_strbc file);

/*! \ingroup tafile
 * \brief Creates the directory of a file.
 *
 * The \b taf_doesDirOfFileExistStr_CreatePath()
 *     \b taf_doesDirOfFileExistSbc_CreatePath() check if the
 * the directory of the \p path file exits.
 * If the directory does not exist, the functions try to create it
 * using the 0755 permission.
 *
 * \return
 *  The functions return 0 if the directory of the \p file exists,
 *  otherwise they return -1.
 *  This last case occurs when the functions failed to create the directory.
 * \param path the file
**/
extern int taf_doesDirOfFileExistStr_CreatePath(ta_cstr path);
/*! \ingroup tafile
 * \brief Creates the directory of a file.
 * \copydetails taf_doesDirOfFileExistStr_CreatePath(ta_cstr)
**/
extern int taf_doesDirOfFileExistSbc_CreatePath(const ta_strbc path);

/*! \ingroup tafile
 * \brief open a file like fopen but prints an error message if it fails
 * \param file the file to be opened
 * \return a pointer on the file if it succeeded, NULL otherwise
**/
extern FILE* ta_FileOpenS_error(const char* file,const char* mask);

/*! \ingroup tafile
 * \brief open a file like fopen but prints a fatal message if it fails
 * \param file the file to be opened
 * \return a pointer on the file if it succeeded, NULL otherwise
**/
extern FILE* ta_FileOpenS_fatal(const char* file,const char* mask);

/* ! \ingroup tafile
 * \brief  //CHECK did not do this one
**/

extern ta_strbc ta_FileRelativeName(ta_strbc RelativeName,const char*  file,const char* InDir,const char* dir);

#define ta_FileReduceNameS taf_reduceNameStr
#define ta_FileReduceName taf_reduceNameSbc
#define ta_FileExistS taf_doesFileExistStr
#define ta_FileExist taf_doesFileExistSbc
#define ta_FileIsDirS taf_doesDirExistStr
#define ta_FileIsDir taf_doesDirExistSbc
#define ta_FileIsRegularS taf_doesFileRegularStr
#define ta_FileIsRegular taf_doesFileRegularSbc
#define ta_FileIsDirS_CreateDir taf_doesDirExistStr_CreateDir
#define ta_FileIsDirS_CreatePath taf_doesDirExistStr_CreatePath
#define ta_FileIsDir_CreatePath taf_doesDirExistSbc_CreatePath

/*** functions of ta_info.c ***/
extern ta_boolean taif_InitByEnvV(const ta_strbc fname,const char* EnvV_dir,ta_boolean hidden);
extern ta_boolean taif_InitByEnvV_S(const char* fname,const char* EnvV_dir,ta_boolean hidden);
extern ta_boolean taif_InitByFile(const ta_strbc fname);
extern ta_boolean taif_InitByFile_S(const char* fname);
extern ta_strbc taif_get_var(ta_strbc value,const ta_strbc varname);
extern ta_strbc taif_get_var_S(ta_strbc value,const char* varname);
extern void taif_free();

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

#endif /* end of  FILE_TA_MISC_H  */







/*########################################################################
# Application :  Tools 'A'
# File        :  ./include/ta_SaveLoad.h
# Revision    :  $Revision: 2.6.2.6 $
# Author      :  Ivan Aug� (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ########################################################################
# libta.a libtawd.a: library of Tools 'A'
# ########################################################################
# ######################################################################*/


#ifndef FILE_TA_SAVELOAD_H
#define FILE_TA_SAVELOAD_H

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/******************************************************************/
/*** Data structures. 				                            ***/
/*** The ta_SL_TableEle structure gives up to the Save/Load		***/
/*** kernel all the informations needed for a given type.		***/
/***															***/
/*** For a given type, those informations are essentially based ***/
/*** on 3 functions.											***/
/***    1) void          sl_save_<type>(type*,ta_SL_Main)		***/
/***    2) type*         sl_load_<type>(ta_SL_Main)				***/
/***	3) ta_SL_LongOid sl_tid_<type> (type*)					***/
/***															***/
/*** The rule of (1) is to write the data of the variable of	***/
/*** type "type" pointed to by "addresse" on to the output      ***/
/*** stream.                                                    ***/
/*** The rule of (2) is first to allocated memory, then to fill	***/
/*** up it (the memory location) using the input stream and		***/
/*** finally to return the memory location.	At this point note	***/
/*** that the memory location must be done using the "ta" memory***/
/*** handler (the "ta" MEM_MALLOC or the "ta" overloaded new).	***/
/*** The rule of (3) is for polymorphisme handling, it must		***/
/*** return the real type pointed to by addresse.               ***/
/***															***/
/*** You never must directly call theses functions but use the	***/
/*** following ones that will call them indirectly.              ***/
/***   1) ta_SL_SaveDat  (ta_SL_Main,type*,size)				***/
/***   2) ta_SL_LoadDat  (ta_SL_Main,type*,size)				***/
/***   3) ta_SL_SaveObj   (ta_SL_Main,type*,ta_SL_LongOid)		***/
/***   4) ta_SL_LoadObj   (ta_SL_Main,type*,ta_SL_LongOid)		***/
/***   5) ta_SL_SaveRef   (ta_SL_Main,type*,ta_SL_LongOid)		***/
/***   6) ta_SL_LoadRef   (ta_SL_Main,type**,ta_SL_LongOid)		***/
/***   7) ta_SL_SaveDatRef(ta_SL_Main,type*, size)				***/
/***   8) ta_SL_LoadDatRef(ta_SL_Main,type**,size)				***/
/***															***/
/*** For 1) 2) 3) 4) the variable is defined by:				***/
/***      type variable;										***/
/*** For 5) 6) 7) 8) the variable is defined by:				***/
/***      type* variable;										***/
/***															***/

typedef struct _ta_SL_TableEle {
	ta_SL_Oid   sl_LongOid;		/* Object identifier  */
	void*     sl_LoadFunc;	/* save function      */
	void*     sl_SaveFunc;	/* load function      */
	void*     sl_OidFunc;		/* real oid function  */
	void*     sl_SizeFunc;	/* real size function */
	int32       sl_sz;			/* sz of object.                       */
	ta_boolean	sl_Null;		/* set object to 0 before loading.     */
	ta_boolean	sl_All;			/* read sz byte before loading.        */
	short       sl_ShortOid;	/* internal use only.                  */
} ta_SL_TableEle;

/******************************************************************/
/*** functions to initialize and free the Save/Load handler     ***/

extern ta_SL_Table* ta_SL_TableNew();
extern void ta_SL_TableFree(ta_SL_Table* table);
extern void ta_SL_TableAdd1(ta_SL_Table* table,const ta_SL_TableEle* ele);
extern void ta_SL_TableAddN(ta_SL_Table* table,const ta_SL_TableEle* p);
extern ta_SL_Main* ta_SL_New(const ta_SL_Table* table,const char * FileName,ta_boolean DoLoad);
extern ta_SL_Main* ta_SL_Reinit(ta_SL_Main* psl,const char * FileName,ta_boolean DoLoad);
extern void ta_SL_Free(ta_SL_Main* psl);

/******************************************************************/
/*** basic functions to Save and Load from/to the file			***/

/* to use inside Save/Load function */
extern ta_SL_TableEle* ta_SL_GetObjDef(ta_SL_Main* psl,ta_SL_Oid LongOid);
extern void ta_SL_Pop(ta_SL_Main* psl);
extern void ta_SL_Push(ta_SL_Main* psl);
extern void ta_SL_ChgRef(ta_SL_Main* psl, const void* r_org,const void* r_new);

extern void ta_SL_SaveDat(ta_SL_Main* psl,const void* addr,int32 size);
extern void ta_SL_LoadDat(ta_SL_Main* psl,void* addr,int32 size);
extern void ta_SL_SaveObj(ta_SL_Main* psl,const void* addr,ta_SL_Oid LongOid);
extern void ta_SL_LoadObj(ta_SL_Main* psl,void* addr,ta_SL_Oid LongOid);
extern void ta_SL_SaveRef(ta_SL_Main* psl,const void* addr,ta_SL_Oid LongOid);
extern void ta_SL_LoadRef(ta_SL_Main* psl,void** paddr,ta_SL_Oid LongOid);
extern void ta_SL_SaveDatRef(ta_SL_Main* psl,const void* addr,int32 size);
extern void ta_SL_LoadDatRef(ta_SL_Main* psl,void** paddr,int32 size);

/******************************************************************/
/*** OID of ta objects                                          ***/

#define TA_SL_id_ta_StaStr     "TA:BscStr"    /* static  (char t[N]) */
#define TA_SL_id_ta_DynStr     "TA:CharE"     /* dynamic (char *) */
#define TA_SL_id_ta_strbc      "TA:ta_strbc"
#define TA_SL_id_TA_Strbc      "TA:TA_Strbc"
#define TA_SL_id_ta_strdb      "TA:ta_strdb"
#define TA_SL_id_TA_Strdb      "TA:TA_Strdb"
#define TA_SL_id_taeh_position "TA:taeh_position"

/******************************************************************/
/*** functions that Save/Load the ta objects					***/

/* add to table the ta Save/load table */
extern void ta_SL_TableAdd_TA(ta_SL_Table* table);

/* add to table the ta Save/load table */
extern void ta_SL_MtOSaveObj(const ta_memt * pt,ta_SL_Main* psl,ta_SL_Oid LongOid);
extern void ta_SL_MtOLoadObj(ta_memt * pt,ta_SL_Main* psl,ta_SL_Oid LongOid);
extern void ta_SL_MtRSaveObj(const ta_memt * pt,ta_SL_Main* psl,ta_SL_Oid LongOid);
extern void ta_SL_MtRLoadObj(ta_memt * pt,ta_SL_Main* psl,ta_SL_Oid LongOid);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

#endif /* end of  FILE_TA_SAVELOAD_H */
/*########################################################################
# Application :  Tools 'A'
# File        :  ./include/ta_band.h
# Revision    :  $Revision: 2.6.2.6 $
# Author      :  Ivan Aug� (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ########################################################################
# libta.a libtawd.a: library of Tools 'A'
# ########################################################################
# ######################################################################*/


#ifndef FILE_TA_BAND_H
#define FILE_TA_BAND_H


#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif


/* definition d 'une plage de bits */

typedef flag8 ta_band0;
typedef struct _ta_band {
    ta_band0 l;			/* bit de poids faible de la plage */
    ta_band0 h;			/* bit de poids fort de la plage */
} ta_band;				/* (TA_HVALUE,TA_HVALUE) est la plage nulle. */

#define BAND_LEFT_MIN	((TA_band0)(0))
#define BAND_RIGHT_MAX	((ta_band0)(127))
#define BAND_UNDEFINED	((ta_band0)(-1))

/******* definition de quelques masques.*******/
	/* mets des 1 du bit num i au bit num j inclus. 
	 * 0<=i et i<=j et j<TA_FLAGXXBITSZ. */
#define PUT_1_BiBj(i,j)	((ta_uintxx)((((ta_uintxx)-1)<<(i)))&(((ta_uintxx)-1)>>((TA_FLAGXXBITSZ-1)-(j))))
	/* met les i bits de poids failbles a 1. */
#define PUT_1_NLOWBIT(i)	((ta_uintxx)(((ta_uintxx)-1)>>(TA_FLAGXXBITSZ-(i))))
	/* get value of v[j:i] (included) -> [(j-i)-1:0] lsb adjust */
#define GET_V_BiBj(v,i,j) ((ta_uintxx)(((ta_uintxx)(v)) & PUT_1_BiBj(i,j))>>(i))

	/* put a new value for the bits that are indicated in the mask */
#define V_PUT_MSK(v,newval,mask)		(v = ((v & ~(mask)) | ((newval) & (mask))))
#define V_GET_MSK(v,mask)				(v & (mask))
#define V_IS_MSK(v,isval,mask)			(V_GET_MSK(v,mask) == (isval))
#define V_ISNOT_MSK(v,isval,mask)		(V_GET_MSK(v,mask) != (isval))
#define V_ISIN_MSK(v,isval,mask)		((V_GET_MSK(v,mask) & (isval)) != 0)
#define V_ISNOTIN_MSK(v,isval,mask)		((V_GET_MSK(v,mask) & (isval)) == 0)

/******* definition sur les plages.	*******/
#define BAND_INIT_NULL(x) (x).h=(x).l=BAND_UNDEFINED
#define BAND_INIT_BY_SZ(bd,sz) {(bd).h=(sz)-1; (bd).l=0; }
#define BAND_SHIFT(bd,sh) {(bd).h+=(sh); (bd).l+=(sh); }
#define BAND_IS_NULL(x) (((x).h == BAND_UNDEFINED)&&((x).l == BAND_UNDEFINED))
#define BAND_SIZE(x) (BAND_IS_NULL(x) ? 0 : (x).h - (x).l + 1)
#define BAND_X_IN_BD(x,y) (((y.l<=(x))&&((x)<=y.h)))
#define BAND_B1_EQ_B2(x,y) (((x.l==y.l)&&(x.h==y.h)))
#define BAND_B1_IN_B2(x,y) (((x.l>=y.l)&&(x.h<=y.h)))
#define BAND_B1_B2_OVERLAP(x,y) (((x).h>=(y).l)&&((y).h>=(x).l))
#define BAND_B1_B2_NOOVERLAP(x,y) (((x).h<(y).l)||((y).h<(x).l))

	/* les bits i de la plage x sont a 1. (si i <=TA_FLAGXXBITSZ). */
#define BAND_Mask(x)	PUT_1_BiBj(x.l,x.h)
#define BAND_NOT_IN_VALUE(x)((x.l>=SZ_VALUE0_BIT))	
#define BAND_GET_V(v,x)	GET_V_BiBj(v,x.l,x.h)


/******* some functions *******/
extern int tabd_cmp(ta_band bd1,ta_band bd2);
extern ta_boolean tabd_AreOverlapping(ta_band bd1,ta_band bd2);
extern ta_boolean tabd_BD1InBD2(ta_band bd1, ta_band bd2);
extern ta_boolean tabd_GetIntersection(ta_band* bd1,ta_band bd2);
extern ta_boolean tabd_SzInBd(int16 sz,ta_band bd);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

/******* C++ interface *******/
#if defined(FILE_CPP) || defined(__cplusplus)

class TA_Band {
public:
	/* constructors */
	TA_Band()								{ BAND_INIT_NULL(band); }
	TA_Band( const TA_Band& x)				{ band=x.band;}
	TA_Band( int16 lsb, int16 msb)			{ Init(lsb,msb); }
	TA_Band( ta_band x)			            { band=x; }
	TA_Band*   dup()						{ return new TA_Band(*this); }
	TA_Band&   operator=(const TA_Band& x)	{ band=x.band; return *this; }
	operator   const ta_band()  const       { return band; }
	operator   const ta_band*() const       { return &band; }
	/* basic features */
	ta_band0   getLSB()  const				{ return band.l; }
	ta_band0   getMSB()  const				{ return band.h; }
	int32      getSize() const				{ return BAND_SIZE(band); }
	void       Init(const TA_Band& x)       { band=x.band; }
	void       Init(int16 lsb, int16 msb) 	{ band.l=lsb; band.h=msb; }
	void       InitToEmpty() 				{ BAND_INIT_NULL(band); }
	ta_boolean isEmpty() const				{ return BAND_IS_NULL(band); }
	/* intersection & ...  */
	int        cmp(const TA_Band& x) const	{ return tabd_cmp(band,x.band); }
	ta_boolean isBdIncluded(const TA_Band& bd)const{ return tabd_BD1InBD2(bd.band,band); }
	ta_boolean isOverlaping(const TA_Band& x) const{ return tabd_AreOverlapping(band,x.band); }
	ta_boolean getIntersection(const TA_Band& x) { return tabd_GetIntersection(&band,x.band); }
	/* other */
	flag32	   mask32()						{ return BAND_Mask(band); }
	char*      str(char* s)                 { sprintf(s,"[%d:%d]",getMSB(),getLSB()); return s;}
private:
	ta_band band;
};

#endif /* FILE_CPP */

#endif /* FILE_TA_BAND_H */


/*****************************************************************************/
/* 128 arithmetic.                                                           */

/*! \defgroup ta_strbc 128 bit arithmetic 
 *  \ingroup tamisc 
 *
**/


#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/*** long numeric type. ***/
/* max size of a field and of a constant */

#define TA_A128_BitSz        128     /* size in bits (next in ta_uintxx) */
#define TA_A128_FlagxxSz     ((TA_A128_BitSz+TA_FLAGXXBITSZ-1)/TA_FLAGXXBITSZ)
#define TA_A128_HalfFlagxxSz (TA_A128_FlagxxSz*2)

/***************************************************************************/
/**** initialization and conversion                                     ****/

extern ta_uintxx* ta_a128_LowVal();
extern ta_uintxx* ta_a128_HighVal();
extern ta_uintxx* ta_a128_MaskNlsb(ta_uint128 r,int16 nb);

/* ta_uintxx is 32 bits                                                       */
#if TA_FLAGXXBITSZ==32
#define ta_a128_cast2U32(x)          ((ta_uint32)x[0])
#define ta_a128_InitBy1Flagxx(r,val) (r[0]=(val),r[1]=r[2]=r[3]=0)
#define ta_a128_InitBy1Flag32(r,val) (r[0]=(val),r[1]=r[2]=r[3]=0)
#define ta_a128_InitBy1Int32(r,val)  ( ((int32)(val))>=0 ? \
	(r[0]=(val),r[1]=r[2]=r[3]=0) : (r[0]=(val),r[1]=r[2]=r[3]=TA_FLXX_HVALUE) )
#define ta_a128_InitBy4Flag32(r,val4,val3,val2,val1)\
	{r[0]=(val1);r[1]=(val2);r[2]=(val3);r[3]=(val4);}
#define ta_a128_InitBy4Int32 (r,val4,val3,val2,val1)\
	ta_a128_InitBy4Flag32(r,val4,val3,val2,val1)
extern ta_uintxx* ta_a128_InitByFlag32M(ta_uint128 r,const ta_uint32* op1,int sz);
extern ta_uint32* ta_a128_cvtToFlag32(ta_uint128 op);
extern ta_uint32* ta_a128_cpyToFlag32(ta_uint32* r,const ta_uint128 op1);
extern ta_uint32* ta_a128_cpyToFlag32Sz(ta_uint32* r,const ta_uint128 op1,int sz);
#endif

/* ta_uintxx is 64 bits                                                       */
#if TA_FLAGXXBITSZ==64
#define ta_a128_cast2U32(x)          ((ta_uint32)x[0])
#define ta_a128_InitBy1Flagxx(r,val) (r[0]=(val),r[1]=0)
#define ta_a128_InitBy1Flag32(r,val) (r[0]=(val),r[1]=0)
#define ta_a128_InitBy1Int32(r,val) ( ((int32)(val))>=0 ? \
	(r[0]=(val),r[1]=0) : (r[0]=(val),r[1]=TA_FLXX_HVALUE) )
extern void  ta_a128_InitBy4Flag32(ta_uint128 r, ta_uint32 val4,ta_uint32 val3,ta_uint32 val2, ta_uint32 val1);
#define ta_a128_InitBy4Int32	ta_a128_InitBy4Flag32
extern ta_uintxx* ta_a128_InitByFlag32M(ta_uint128 r,const ta_uint32* op1,int sz);
extern ta_uint32* ta_a128_cvtToFlag32(ta_uint128 op);
extern ta_uint32* ta_a128_cpyToFlag32(ta_uint32* r,const ta_uint128 op1);
extern ta_uint32* ta_a128_cpyToFlag32Sz(ta_uint32* r,const ta_uint128 op1,int sz);
#endif

/***************************************************************************/
/**** comparison                                                        ****/

extern int ta_a128_UCmp(const ta_uint128 l,const ta_uint128 r);
extern int ta_a128_SCmp(const ta_uint128 l,const ta_uint128 r);
extern int ta_a128_UCmpSz(const ta_uint128 l,const ta_uint128 r,int sz);
extern int ta_a128_SCmpSz(const ta_uint128 l,const ta_uint128 r,int sz);

/***************************************************************************/
/**** logic operators.                                                  ****/
extern ta_uintxx* ta_a128_Cpy(ta_uint128 r,const ta_uint128 op1);
extern ta_uintxx* ta_a128_Not(ta_uint128 r,const ta_uint128 op1);
extern ta_uintxx* ta_a128_And(ta_uint128 r,const ta_uint128 op1,const ta_uint128 op2);
extern ta_uintxx* ta_a128_Or(ta_uint128 r,const ta_uint128 op1,const ta_uint128 op2);
extern ta_uintxx* ta_a128_Xor(ta_uint128 r,const ta_uint128 op1,const ta_uint128 op2);

/***************************************************************************/
/**** shift operators.                                                  ****/
extern ta_uintxx* ta_a128_UShL(ta_uint128 r,const ta_uint128 op1,int32 sh);
extern ta_uintxx* ta_a128_UShR(ta_uint128 r,const ta_uint128 op1,int32 sh);
extern ta_uintxx* ta_a128_SShL(ta_uint128 r,const ta_uint128 op1,int32 sh);
extern ta_uintxx* ta_a128_SShR(ta_uint128 r,const ta_uint128 op1,int32 sh);

/***************************************************************************/
/**** mask operators.                                                   ****/
extern ta_uintxx* ta_a128_Mask1BlBh(ta_uint128 r,flag16 l,flag16 h);
extern ta_uintxx* ta_a128_Mask1Band(ta_uint128 r,const ta_band bd);
extern ta_uintxx* ta_a128_Mask1Nl(ta_uint128 r,flag16 n);
extern ta_uintxx* ta_a128_Mask1Nh(ta_uint128 r,flag16 n);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

/*########################################################################
# Application :  Tools 'A'
# File        :  ./include/ta_format.h
# Revision    :  $Revision: 2.6.2.6 $
# Author      :  Ivan Aug� (Email: auge@iie.cnam.fr)
# ########################################################################
#
# This file is part of the Tools 'A'
# Copyright (C) Laboratoire LIP6 - D�partement ASIM
# Universit� Pierre et Marie Curie
#
# This program is free software; you can redistribute it  and/or modify it
# under the  terms of the GNU  General Public License  as published by the
# Free Software Foundation;  either version 2 of the License,  or (at your
# option) any later version.
#
# Tools 'A' software is  distributed in the hope  that it  will be useful,
# but  WITHOUT  ANY  WARRANTY ;  without  even  the  implied  warranty  of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
# Public License for more details.
#
# You should have received a copy  of the GNU General Public License along
# with the GNU C Library; see the  file COPYING. If not, write to the Free
# Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
#
# ########################################################################
# libta.a libtawd.a: library of Tools 'A'
# ########################################################################
#    - taol_xxx : list generator
# ######################################################################*/


#ifndef FILE_TA_FORMAT_H
#define FILE_TA_FORMAT_H

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/***********************************************************************/
/*** taol: list generator                                            ***/
/***                                                                 ***/
/*** parameters of list:                                             ***/
/***   list:= u_head item item item u_tail                           ***/
/***   item:= i_left u_ihead i_middle u_itail i_end                  ***/
/***                                                                 ***/
/*** Example 1:             | taol_init5Str("z <= ..", 1,            ***/
/***                        |       0,        "when", ",\n");        ***/
/***   z <= select x with   | taol_setITLast(                        ***/
/***   v0 when c0,          |        0, "when", "other;\n");         ***/
/***   v1 when c1,          | taol_addITStr("v0","c0");              ***/
/***   ...,                 | taol_addITStr("v1","c1");              ***/
/***   vn when other;       | ...                                    ***/
/***                        | taol_addITStr("vn",0);                 ***/
/***                        | list=taol_cpyFree(list);               ***/
/***                                                                 ***/
/*** Example 2:                                                      ***/
/***   switch (x) {         | taol_init5Str("switch (x) {",  "}",    ***/
/***     case c0 :          |       "case",   ":\n",  "break;\n");   ***/
/***       v0               | taol_setITLast(                        ***/
/***       break;           |       "default", ":\n", "break;\n");   ***/
/***    case c1 :           | taol_addITStr("c0","v0");              ***/
/***       v1               | taol_addITStr("c1","v1");              ***/
/***       break;           | ...                                    ***/
/***  ...                   | taol_addITStr(0,"vn");                 ***/
/***    default :           | list= taol_cpyFree(list);              ***/
/***      vn                                                         ***/
/***      break;                                                     ***/
/***   }                                                             ***/
/***                                                                 ***/
/*** Example 3:                                                      ***/
/***                        | taol_init5Str("port (",  ");",         ***/
/***   port ... (           |       0,        0,      ",\n");        ***/
/***     port0,             | taol_setITLast(                        ***/
/***     port1,             |       0,        0,      ",\n");        ***/
/***     ...                | taol_addITStr("port0",0);              ***/
/***     portN;             | taol_addITStr("port1",0);              ***/
/***   );                   | taol_addITStr("portN",0);              ***/
/***                        | list= taol_cpyFree(list);              ***/
/***                                                                 ***/
/*** Example 4:            | taol_init5Str(0,  0,                    ***/
/***   if ( c0 ) {         |       "} else if (",  ") {\n",  "\n");  ***/
/***     v0                | taol_setITLast(                         ***/
/***   } else if (c1) {    |       "} else ",      "{",      "\n}"   ***/
/***     v1                | taol_setITFirst(                        ***/
/***   } else if (c2) {    |       "if (",  ") {\n",  "\n");         ***/
/***       v2              | taol_addITStr("c0","v0");               ***/
/***   } else {            | taol_addITStr("c1","v1");               ***/
/***       v3              | taol_addITStr("c1","v1");               ***/
/***   }                   | taol_addITStr("c1","v1");               ***/
/***                       | list=taol_cpyFree(list);                ***/
/***                                                                 ***/

extern void taol_init2Str(const char* head,const char* tail);
extern void taol_init5Str(const char* head,const char* tail,const char* left,const char* mid,const char* right);
extern void taol_setITFirst(const char* left,const char* mid,const char* right);
extern void taol_setITCurr(const char* left,const char* mid,const char* right);
extern void taol_setITLast(const char* left,const char* mid,const char* right);
extern void taol_addITStr(const char* head,const char* tail);
extern ta_strbc taol_cpyFree(ta_strbc str);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

#endif /* FILE_TA_FORMAT_H */

/***********************************************************************/
/*** ta network utilities                                            ***/

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/*! \defgroup tanet some network utilies */

/*! \ingroup tanet
 * The \b tanet_isDotedIPV4 function returns 1 if the \p addr argument
 * corresponds to an IPV4 address in doted format,
 * otherwise it returns 0. */
extern int tanet_isDotedIPV4(ta_cstr addr);

/*! \ingroup tanet
 * The \b tanet_dotedmask2int function converts the \a mask given under
 * doted format into "/n" notation.
 *
 * On success, the fonction returns the \em n of the "/n" notation,
 * otherwise it returns -1.
**/
extern int tanet_dotedmask2int(ta_cstr mask);

typedef struct _ta_ipv6 {
    int  size;
    unsigned char data[16];
} ta_ipv6;

/*! \ingroup tanet
 * The \b tanet_IPV6toStr function returns into the memory buffer
 * pointed to by the \a addr pointer the doted format of \a ip.
 * The \a size field of the \a ip parameter is ignored.
**/
extern void tanet_IPV6toStr(char* addr, ta_ipv6* ip);

/*! \ingroup tanet
 * The \b tanet_IPV6FromMac function return the ip corresponding to 
 * the \a mac parameter with the network prefix \a prefix.
 * \param mac the source MAC address
 * \param prefix the net of the returned address
**/
extern ta_ipv6 tanet_IPV6FromMac(ta_cstr prefix, ta_cstr mac);

/*! \ingroup tanet
 * The \b tanet_parseIPV6 function converts the \a addr given under
 * doted format with "/n" notation into the \a ip structure.
 *
 * On success, the fonction returns the \em 0 otherwise it returns -1.
 * \param ip the buffer where the conversion is returned. It can be the
 *      null pointer.
**/
extern int tanet_parseIPV6(ta_cstr addr, ta_ipv6* ip);

/*! \ingroup tanet
 * The \b tanet_getIPV6mask function return the mask corresponding to 
 * the \a ip parameter.
 * \param ip the source IPv6 address.
**/
extern ta_ipv6 tanet_getIPV6mask(ta_ipv6* ip);

/*! \ingroup tanet
 * The \b tanet_getIPV6net function return the network corresponding to 
 * the \a ip parameter.
 * \param ip the source IPv6 address.
**/
extern ta_ipv6 tanet_getIPV6net(ta_ipv6* ip);

/*! \ingroup tanet
 * The \b tanet_getIPV6bcast function return the broadcast corresponding to 
 * the \a ip parameter.
 * \param ip the source IPv6 address.
**/
extern ta_ipv6 tanet_getIPV6bcast(ta_ipv6* ip);

/*! \ingroup tanet
 * The \b tanet_sendmail function sends a mail to the user defined by the
 * \p to parameter using the SMTP server runing on \p host:port host.
 * \n
 * The mail is built using the \p from parameter for the reply address,
 * the \p subject parameter for the subject, the other parameters
 * \p text, \p textfile and \p binfile for the body.
 * More precisely, the body is built chaining the next rules: 
 *   - if \p text is not null, it is copied as such to the body.
 *   - if \p textfile is not null, the content of the
 *     \p textfile file is appended to the body.
 *   - if \p binfile is not null, a small message containing a 'C' decoder
 *     program is appended to the email body and then the content of the
 *     \p binfile file is encoded and the encoded data are appended to the
 *     body. 
 *
 * The format of the \p host parameter can be human (eg: host.subnet.net)
 * or in doted format (eg: 193.54.195.51), in this last case,
 * the \b tanet_sendmail function does not invoke the \b gethostbyname
 * function of the \b libc library.
 * \par
 * The \b tanet_sendmail function returns 0 if the mail has been
 * successfully transmited to the SMTP server, otherwise it returns -1.
**/
extern int tanet_sendmail(cstr host, int port,
    cstr from, cstr to, cstr subject,
    cstr text, cstr textfile, cstr binfile);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

/************************************************************************/
/*** some escape sequencies for vt100, xtem, linux vt, ...            ***/
/***                                                                  ***/

/* move cursor & main sequences */
extern char gl_tavt_clearScreen[];
extern char gl_tavt_cursHome[];
extern char gl_tavt_cursUp[];
extern char gl_tavt_cursDown[];
extern char gl_tavt_cursLeft[];
extern char gl_tavt_cursRight[];
extern char gl_tavt_cursCol70[];
extern char gl_tavt_cursVisible[];
extern char gl_tavt_cursInvisible[];

/* Back/for ground color */
extern char gl_tavt_reset[];
extern char gl_tavt_BgBlack[];
extern char gl_tavt_BgRed[];
extern char gl_tavt_BgGreen[];
extern char gl_tavt_BgYellow[];
extern char gl_tavt_BgBlueD[];
extern char gl_tavt_BgRosa[];
extern char gl_tavt_BgBlueL[];
extern char gl_tavt_BgWhite[];

extern char gl_tavt_FgBlack[];
extern char gl_tavt_FgRed[];
extern char gl_tavt_FgGreen[];
extern char gl_tavt_FgYellow[];
extern char gl_tavt_FgBlueD[];
extern char gl_tavt_FgRosa[];
extern char gl_tavt_FgBlueL[];
extern char gl_tavt_FgWhite[];

/************************************************************************/
/*** handler for executing commands                                   ***/
/***                                                                  ***/

#include <sys/time.h>

/* maximum number of pipes */
#define TARC_NBPIPE  8

typedef struct _ta_runcmd_pb {
  /** must be initialize by user **/
    /* 'i': init ; 'r': run ; 'e' : end */
    void (*pb_func)(char c, struct _ta_runcmd_pb* param);
    int    pb_pipe;     /* pipe number for stdin */
  /** initialised by ta (date of start) */
    struct timeval pb_time0;
    
  /** misc. variables, pbs can use them as they intend. **/
    struct timeval pb_time1;
    struct timeval pb_time2;
    unsigned long  pb_ulong1; 
    unsigned long  pb_ulong2; 
    long           pb_int1; 
    long           pb_int2; 
} ta_runcmd_pb;

typedef struct _ta_runcmd {
	int    status;		/* return code of command */
	cstr   outputs;		/* outputs of the command */
	uint32 mainopt;     /* configuration see TARC_MOPT_xxx */
	uint32 inopt;       /* configuration see TARC_IOPT_xxx */
	uint32 outopt;      /* configuration see TARC_OOPT_xxx */
	uint32 erropt;      /* configuration see TARC_EOPT_xxx */
	cstr   inputs;		/* strings to send to stdin of command */
    cstr   chgDir;      /* run the command in this directory */
    cstr*  chgEnv;      /* run the command with this environment */
    int    status_ok[2];/* 2 others good status values */
    int    lastpid;     /* pid of last command started (ro) */
    cstr   inFile;      /* file to read  for YAKA_EXEC_inFile */
    cstr   outFile;     /* file to write for YAKA_EXEC_OutToFile */
    int    inMap[TARC_NBPIPE] ; /* map fidMap[i] to the ith input pipe */
    int    outMap[TARC_NBPIPE]; /* map fidMap[i] to the ith output pipe */
    int    stdoutTOstderr;
    ta_runcmd_pb pb;    /* progress bar parameters */
    int (*function)(int,char**);
    int   function_argc;
    /* next are used internaly */
    uint32 flag;
    int    status_seq;  /* or of the status of the commands run
                         * from the last uex_command_sequenceBegin.
                         * uex_command_sequenceEnd clears it. */
} ta_runcmd;

extern ta_runcmd gl_tarc_conf;

/* flags of mainopt */
#define TARC_MOPT_VERBOSE	      0x00000001  /* print the command */
#define TARC_MOPT_VERBOSE_SC      0x00000003  /* print the cmd + sub cmd */
#define TARC_MOPT_BackGround      0x00000010 
#define TARC_MOPT_ProgBar         0x00000020 

/* flags for inopt (one of the next) */            
#define TARC_IOPT_InToAll		  0x0000FF00
#define TARC_IOPT_InClose	      0x00000100
#define TARC_IOPT_InFile          0x00000200
#define TARC_IOPT_InMem           0x00000400
#define TARC_IOPT_InPipe(p)      (0x00000800|(p&(TARC_NBPIPE-1)))

/* flags for outopt (one of the next) */            
#define TARC_OOPT_OutToAll	      0x00FF0000
#define TARC_OOPT_OutToMem        0x00010000
#define TARC_OOPT_OutToNull       0x00020000
#define TARC_OOPT_OutToFile       0x00040000
#define TARC_OOPT_OutToPipe(p)   (0x00080000|(p&(TARC_NBPIPE-1)))

/* flags for erropt (one of the next) */            
#define TARC_EOPT_ErrToAll	      0xFF000000
#define TARC_EOPT_ErrToMem        0x01000000
#define TARC_EOPT_ErrToNull       0x02000000
/* stream (s) is input pipe (p)  &  stream (s) is output pipe (p) */
/*
#define YAKA_EXEC_StrInPipe(s,p)  (0x04000000|((p)<<30)|((s)<<28))
#define YAKA_EXEC_StrOutPipe(s,p) (0x08000000|((p)<<30)|((s)<<28))
#define YAKA_EXEC_StrInPipeAll    YAKA_EXEC_StrInPipe(3,3)
#define YAKA_EXEC_StrOutPipeAll   YAKA_EXEC_StrOutPipe(3,3)
*/

/* execute a command as in shell */
extern void   tarc_config_init();
extern uint32 tarc_setStdin (uint32 to);
extern uint32 tarc_setStdout(uint32 to);
extern uint32 tarc_setStderr(uint32 to);

extern int    tarc_commandT(cstr com, cstr args[]);
extern int    tarc_command0(cstr com, va_list args);
extern int    tarc_command( cstr com, ...);
extern int    tarc_function(int (*func)(int,char**), cstr com, ...);
/*
extern int    tarc_commandS(cstr str);
*/

/*
extern int    tarc_simulate(int status,cstr fmt,...);
*/
extern void tarc_sequenceBegin(
    uint32 mainopt, uint32 inopt, uint32 outopt, uint32 erropt,
    cstr fmt, ...);
extern int  tarc_sequenceEnd();

/*** for progress bars ***/
/* read the stdin stream until the last complet item,
 * copies the item into _buffer and return the strlen
 * of the item. */

extern int  tarc_command_pb_read_1(char* _buffer);

/**
 * - read the input stream until the last complet item and at least
 *   1/16 of second is spent after the last echantillon time.
 * - last is set to the return time.
 * - The first call returns the 1th echantillon.
**/

extern void tarc_command_pb_read_2(
    char* _buffer, struct timeval* last);

/************************************************************************/

/*! \defgroup tatest Autotest handler
 * \ingroup tamisc
 *
 * This module provides features for printing test results.
**/

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

/* \ingroup tatest */
typedef struct _ta_autotest* ta_autotest;
/* \ingroup tatest */
extern ta_autotest tatest_create(ta_cstr mess);
/* \ingroup tatest */
extern void tatest_delete(ta_autotest ptest);
/* \ingroup tatest */
extern void tatest_pushEle(ta_autotest ptest, ta_cstr mess);
/* \ingroup tatest */
extern void tatest_popEle(ta_autotest ptest);
/* \ingroup tatest */
extern void tatest_popEleM1(ta_autotest ptest);
/* \ingroup tatest */
extern void tatest_popEleSelf(ta_autotest ptest);
/* \ingroup tatest */
extern void tatest_addEleScore(ta_autotest ptest, int sc_max, int sc_got);
/* \ingroup tatest */
extern double tatest_getEleScore(ta_autotest ptest);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

/************************************************************************/
/*! \defgroup tamisc Miscellaneous handlers */

/*! \defgroup taoutput Output handler
 * \ingroup tamisc
 *
 * This module allows to nest output streams
 * the stream can be files or memory buffers, the handler provides
 * writing primitives which mask the stream type.
 * \n
 * The next example illustrates the usage of this module,
 * the \p buf1 and \p buf2 buffers will contain respectively the
 * "gnu2gnu" and "bee" strings and the "gnat.dat" file will contain
 * "gnat2gnat".
 * \code 
 * #include <ta.h>
 * void* buf1;
 * void* buf2;
 *
 * // declaration of an output stack
 * ta_outstreams outstreams=taout_create();
 *
 * // push a memory output stream and write gnu into it.
 * taout_push_mem(outstreams);
 * taout_write(outstreams,'gnu",3);
 *
 * // push a file output stream and write gnat into it.
 * taout_push_file("gnat.dat");
 * taout_printf("%s","gnat");
 *
 * // push a memory output stream and write bee into it.
 * taout_push_mem(outstreams);
 * taout_printf("bee");
 *
 * // get data of current stream and close it
 * taout_pop_getdata(outstreams,&buf2);
 *
 * // print gnat in the current stream and then close it
 * taout_printf("%s","2gnat");
 * taout_pop(outstreams);
 * 
 * // print gnu in the current stream gets its data and then close it
 * taout_printf("%s","2gnu");
 * taout_pop_getdata(outstreams,&buf1);
 * 
 * // free the ressources
 * taout_free(outstreams);
 * free(buf1);
 * free(buf2);
 * \endcode 
**/

#if defined(FILE_CPP) || defined(__cplusplus)
extern "C" {
#endif

struct _ta_outstreams;

/*! \ingroup taoutput
 * \brief Stack of output streams.
 *
 * The \b ta_outstreams type define a stack of output streams.
 * it is a hiden type, it must be initialized by the 
 * \ref taout_create function before usage.
**/
typedef struct _ta_outstreams* ta_outstreams;

/*! \ingroup taoutput
 * \brief Creation of an output stream stack
 *
 * The \b taout_create function creates an output stream stack and returns
 * it. The stack is created empty. After usage, it must be freed with 
 * the \ref taout_free function.
**/
extern ta_outstreams taout_create();

/*! \ingroup taoutput
 * \brief Free an output stream stack
 *
 * The \b taout_free function frees all the memory allocated by the
 * \p outstreams stack.
 * If the stack is not empty, the opened streams are closes.
**/
extern void taout_free(ta_outstreams outstreams);
 
/*! \ingroup taoutput
 * \brief Push a file into a stack of output streams.
 *
 * The \b taout_push_file function pushs in the \p outstreams stack of
 * output files the \p fn file which becomes the current output stream.
 * If the \p fn file exists then it is truncated.
 * \n
 * If the \p fn file is opened succesfully in read/write mode then
 * the function returns 0, otherwise it returns -1 and \p outstreams
 * stack is left unchanged.
**/
extern int taout_push_file(ta_outstreams outstreams, ta_cstr fn);
 
/*! \ingroup taoutput
 * \brief Push a file into a stack of output streams.
 *
 * The \b taout_push_stream function pushs in the \p outstreams stack of
 * output files the \p stream file which becomes the current output stream.
**/
extern void taout_push_stream(ta_outstreams outstreams, FILE* stream);
 
/*! \ingroup taoutput
 * \brief Push a file into a stack of output streams.
 *
 * The \b taout_push_stream_noclose function pushs in the \p outstreams
 * stack of output files the \p stream file
 * which becomes the current output stream.
 * The stream will no be closed when this output stream is poped or
 * when the stack is freed.
**/
extern void taout_push_stream_noclose(
    ta_outstreams outstreams, FILE* stream);

/*! \ingroup taoutput
 * \brief Push a memory stream into a stack of output streams.
 *
 * The \b taout_push_mem function pushs in the \p outstreams stack of
 * output files a memory stream which becomes the current output stream.
 * This memory stream is dynamically allocated and data written into it
 * can be get with the functions
 * \ref taout_getdata and \ref taout_pop_getdata.
**/ 
extern void taout_push_mem(ta_outstreams outstreams);

/*! \ingroup taoutput
 * \brief Get data of current stream.
 *
 * The \b taout_getdata function allocates a memory buffer and
 * copies into it the data already written into the current stream
 * of the \p outstreams stack of output streams.
 * The buffer is returned in \p *data and the caller is responsible for
 * freeing it.
 * \n
 * The function returns the number of bytes copied into the buffer.
 * when zero is returned *data is set to zero.
**/ 
extern int taout_getdata(ta_outstreams outstreams, void**data);

/*! \ingroup taoutput
 * \brief Close the current stream and get its data.
 *
 * The \b taout_pop_getdata function allocates a memory buffer and
 * copies into it the data already written into the current stream
 * of the \p outstreams stack of output streams.
 * The buffer is returned in \p *data and the caller is responsible for
 * freeing it.
 * \n
 * Furthermore the \b taout_pop_getdata function closes the current
 * stream and pop the \p outstreams stack setting the current stream
 * to the top of the stack if the stack is not empty. If the
 * stack becomes empty then no current stream is defined.
 * \n
 * The function returns the number of bytes copied into the buffer.
 * when zero is returned *data is set to zero.
**/ 
extern int taout_pop_getdata(ta_outstreams outstreams, void**data);

/*! \ingroup taoutput
 * \brief Close the current stream.
 *
 * The \b taout_pop function closes the current
 * stream and pop the \p outstreams stack setting the current stream
 * to the top of the stack if the stack is not empty. If the
 * stack becomes empty then no current stream is defined.
**/ 
extern void taout_pop(ta_outstreams outstreams);

/*! \ingroup taoutput
 * \brief Write data into the current stream.
 *
 * The \b taout_write function writes \p n bytes of data
 * from the memory location given by \p ptr into the current
 * stream of the \p outstreams stack of output streams.
 * \n
 * On success, the number of bytes written are returned (zero indicates
 * nothing was written).  On error, -1  is  returned
**/
extern int taout_write(ta_outstreams outstreams, const void* ptr, int n);

/*! \ingroup taoutput
 * \brief Write data into the current stream.
 *
 * The \b taout_printf function prints data into the current
 * stream of the \p outstreams stack of output streams.
 * The data printed are generated using the \p fmt format
 * and the remaining arguments as in printf(3).
 * \n
 * On success, zero is returned, on error, -1 is returned.
**/
extern int taout_printf(ta_outstreams outstreams, ta_cstr fmt, ...);

/*! \ingroup taoutput
 * \brief Write data into the current stream.
 *
 * The \b taout_vprintf function performs the same task as
 * \ref taout_printf function with the difference that it takes
 * a set of arguments which have been obtained using the stdarg(3)
 * variable argument list macros.
**/
extern int taout_vprintf(ta_outstreams outstreams, ta_cstr fmt, va_list ap);

/*! \ingroup taoutput
 * \brief appendNull the current stream.
 *
 * The \b taout_appendNull function changes the end of the current
 * stream of the \p outstreams stack of output streams.
 *  - If the current stream is empty, it is left unchanged.
 *  - If c is the null character then c is appended to the current stream
 *    if the last character of the current stream is not already the null
 *    character.
 *  - If c is not the null character then the end of the current stream is
 *    modified as shown below, x and y being a character different
 *    of c and '\\0'.
 *     - the current stream ends with "c\0" : nothing is appended.
 *     - the current stream ends with "xc"  : "\0" is appended.
 *     - the current stream ends with "x\0" : "x\0" is replaced by "xc\0".
 *     - the current stream ends with "\0x" : "c\0" is appended.
 *     - the current stream ends with "cx" : "c\0" is appended.
 *     - the current stream ends with "xy" : "c\0" is appended.
 *  -
**/
extern void taout_appendNull(ta_outstreams outstreams, char c);

/*! \ingroup taoutput
 * \brief appendNull the current stream.
 *
 * The \b taout_nbOpenedStream function returns the numbers of opened
 * streams of the \p outstreams stack of output streams.
**/
extern int taout_nbOpenedStream(ta_outstreams outstreams);

/*! \ingroup taoutput
 * \brief Autotest of output handler.
 *
 * The \b taout_autotest function checks the output handler. It
 * is only available in the debug version of the TA library.
**/
extern void taout_autotest(ta_autotest ts);

#if defined(FILE_CPP) || defined(__cplusplus)
}
#endif

/************************************************************************/
/*** light C++ interface                                             ***/

#ifdef FILE_CPP

class TA_Strbc;
typedef const TA_Strbc* cTA_Strbc;
typedef const TA_Strbc& rTA_Strbc;

class TA_Strbc {
	ta_strbc val;

public:		/*** constructor ****/
	TA_Strbc(const char* p=0)     { val=tasbc_init(p); }
	TA_Strbc(const TA_Strbc&	x) { val=tasbc_cpy(0,x.val); }
	~TA_Strbc()                   { val=tasbc_free(val); }
	TA_Strbc& operator =(const char* src)     { cpy(src); return *this; }
	TA_Strbc& operator =(const TA_Strbc& src) { cpy(src); return *this; }
	          operator const char *()   const { return TASBC_STR(val); }
	          operator const ta_strbc() const { return val; }

public:		/*** cat & cpy function ***/
	TA_Strbc& operator +=(const TA_Strbc& src) { cat(src); return *this; }
	TA_Strbc& operator +=(const char* src)      { cat(src); return *this; }
	TA_Strbc& operator +=(int32 nb)             { cat(nb);  return *this; }
	TA_Strbc  operator + (const TA_Strbc& src) {
                                              TA_Strbc tmp(*this); tmp.cat(src); return tmp; }
	TA_Strbc  operator + (const char* src) { TA_Strbc tmp(*this); tmp.cat(src); return tmp; }
	TA_Strbc  operator + (int32 nb)        { TA_Strbc tmp(*this); tmp.cat(nb); return tmp; }

	TA_Strbc& cpy(const char * src)        { val=tasbc_cpyString(strbc(),src);return *this; }
	TA_Strbc& cat(const char * src)        { val=tasbc_catString(strbc(),src); return *this; }
	TA_Strbc& cat(int32 n)                 { val=tasbc_catInt32(strbc(),n);return *this; }

public:		/*** basic handler. ***/
	int32      len()      const  { return val!=0 ? TASBC_LEN(val) : 0; }
	char *     str()      const  { return TASBC_STR(val); }
	ta_strbc   strbc()    const  { return val; }
	char *     str_free()        { char *ret=str(); val=0; return ret; }
	TA_Strbc& ConvToUp()         { strConvToUp(str()); return *this; }
	TA_Strbc& ConvToLow()        { strConvToLow(str()); return *this; }

public:		/*** file name handler. ***/
	TA_Strbc& ReduceFileName()
		{ val=taf_reduceNameSbc(strbc()); return *this; }
	TA_Strbc& Basename(const char* suff)
		{ val = tasbc_cpyBasename(val,val,suff); return *this; }
	TA_Strbc& Dirname()
		{ val = tasbc_cpyDirname(val,val); return *this; }
	TA_Strbc& Suffix()
		{ val = tasbc_cpySuffix(val,val); return *this; }

	TA_Strbc& cpyBasename(const TA_Strbc& src, const char* suff)
		{ val = tasbc_cpyBasename(strbc(),src.strbc(),suff); return *this; }
	TA_Strbc& cpyDirname(const TA_Strbc& src)
		{ val = tasbc_cpyDirname(strbc(),src.strbc()); return *this; }
	TA_Strbc& cpySuffix(const TA_Strbc& src)
		{ val = tasbc_cpySuffix(strbc(),src.strbc()); return *this; }

public:		/*** complexe handler. ***/
    TA_Strbc& cpyReplace(const char * src,...);
	TA_Strbc& cpyReplace_0(const char * src, va_list ap);
    TA_Strbc& cpyReplace_T(const char * src,const char ** table);
	TA_Strbc& cpyReplace(const char* src,const char** t)
		{ val= tasbc_cpyReplaceS_T(val,src,t); return *this; }

#define cpyPrintf256 cpyPrintf
    TA_Strbc& cpyPrintfLG(int32 dest_lg,const char * fmt,...);
    TA_Strbc& cpyvPrintfLG(int32 dest_lg,const char * fmt, va_list ap);
    TA_Strbc& cpyPrintf(const char * fmt,...);
    TA_Strbc& cpyvPrintf(const char * fmt, va_list ap);

#define catPrintf256 catPrintf
	TA_Strbc& catPrintfLG(int32 dest_lg,const char * fmt,...);
	TA_Strbc& catvPrintfLG(int32 dest_lg,const char * fmt,va_list ap);
    TA_Strbc& catPrintf(const char * fmt,...);
    TA_Strbc& catvPrintf(const char * fmt, va_list ap);

public:		/*** formating handler. ***/
	TA_Strbc& cpyShiftR(		int16 sh=4)
		{ val = tasbc_cpyShiftR(strbc(), sh); return *this; }
	TA_Strbc& cpyListInit(	char* RightSep, char* ListToken, char* LeftSep);
	TA_Strbc& cpyListInit(	);
	TA_Strbc& cpyListAdd (	char* ListToken, char* RightSep, char* ele, char* LeftSep);
	TA_Strbc& cpyListAdd (	char* ele);
	TA_Strbc& cpyListEnd (	char* RightSep, char* ListToken, char* LeftSep);
	TA_Strbc& cpyListEnd (	);

public:		/*** comparaison. ***/
	int			cmp(const char* s)     const { return strcmpbis(this->str(), s); }
	int			cmp(const TA_Strbc& s) const { return tasbc_cmpbis(val, s.val); }
	static int	cmp(const TA_Strbc& s1,const TA_Strbc&s2) { return s1.cmp(s2); }
	static int	cmp(const TA_Strbc* s1,const TA_Strbc*s2) { return s1->cmp(*s2); }
	static int	cmp(const TA_Strbc*const* s1,
                    const TA_Strbc*const* s2) { return (*s1)->cmp(**s2); }
	int			operator==(const TA_Strbc&	s) const { return (cmp(s) == 0); }
	
public:		/* Save/Load features */
	TA_Strbc(     ta_SL_Main*psl);
	void    SL_save(ta_SL_Main*psl) const;
	ta_cstr SL_id() const;
};

class TA_Strdb {
protected:
	 ta_strdb val;

public:		/*** constructor ****/
	TA_Strdb()                    { val=0; }
	TA_Strdb(ta_strdb x) : val(x) { }
	TA_Strdb(const char* p)       { val=tasdb_getDbstrAdd(p); }
	TA_Strdb& operator =(const char* src)     { val=tasdb_getDbstrAdd(src); return *this; }
	          operator const char *() const { return TASDB_STR(val); }
	          operator ta_strdb()     const { return val; }

public:		/*** basic handler. ***/
	int32      len()      const  { return TASDB_LEN(val); }
	const char*str()      const  { return *this; }
	ta_strdb   key()      const  { return *this; }
	TA_Strdb& cpyBasename(const TA_Strdb& src, const char* suff) {
        int   l= src.len()+1; char* tmp= (char*)malloc(l);
        strcpyBasename(tmp,src.str(),suff);
		val = (ta_strdb)tasdb_getDbstrAdd(tmp); free(tmp); return *this; }
	TA_Strdb& cpyDirname(const TA_Strdb& src) {
        int   l= src.len()+1; char* tmp= (char*)malloc(l);
        strcpyDirname(tmp,src);
		val = (ta_strdb)tasdb_getDbstrAdd(tmp); free(tmp); return *this; }

public:		/*** comparaison. ***/
	int			cmp(const char* s)     const { return strcmpbis(this->str(), s); }
	int			cmp(const TA_Strdb& s) const { return strcmpbis(this->str(), s.str()); }
	static int	cmp(const TA_Strdb& s1,const TA_Strdb&s2) { return s1.cmp(s2); }
	static int	cmp(const TA_Strdb* s1,const TA_Strdb*s2) { return s1->cmp(*s2); }
	static int	cmp(const TA_Strdb*const* s1,
                    const TA_Strdb*const* s2) { return (*s1)->cmp(**s2); }
	
public:		/* Save/Load features */
	TA_Strdb(     ta_SL_Main*psl);
	void  SL_save(ta_SL_Main*psl) const;
	char* SL_id() const;
};

#endif /* FILE_CPP */


/************************************************************************/

/*! \defgroup array Dynamic arrays 
 *  This module provides miscellaneous containers the data-structure of
 *  which is a simple array.
 *  The array is dynamically allocated and so it can be reallocated when
 *  array grows up.
 *  All these containers are based on the generic classes
 *  \ref TA_DatMemt, \ref TA_ObjMemt, \ref TA_RefMemt.
**/

/*! \defgroup genarray dynamic array base classes
 *  \ingroup array 
 *  \brief generic classes handling dynamic arrays.
 *
 *  This module provides the generic containers the data-structure of
 *  which is a simple array.
 *  The array is dynamically allocated and so it can be reallocated when
 *  array grows up.
 *  These generic classes generate a few codes because they are
 *  a C++ encapsulation of the C \ref ta_memt or \ref tadt_data type.
 *  There are 3 containers depending of how memory of array elements is handled.
 *    - \ref TA_DatMemt : the container elements are pure data,
 *      no memory allocation/free, no constructor/destructor call 
 *      are performed when elements are added or suppressed or when array is
 *      deleted.
 *    - \ref TA_ObjMemt : the container elements are C++ object,
 *      no memory allocation/free are performed but Constructor/destructor
 *      are called when elements are added or suppressed.
 *      When the array is deleted the "obj" destructor is run for all the
 *      array elements before deleting the array.
 *      \attention The "obj" class must have a default constructor.
 *    - \ref TA_RefMemt : the container elements are C++ object reference,
 *      memory allocation/free are performed and Constructor/destructor
 *      are called when elements are added or suppressed.
 *      When the array is deleted the "obj" destructor is run and the
 *      memory is freed for all the array elements before deleting the array.
 *      \attention The "obj" class must have a duplication member which returns
 *      a reference.
 *    - \ref TA_RoRefSet, \ref TA_WkRefSet, \ref TA_MmRefSet :
 *      the container elements are C++ object reference. These classes are
 *      useful to manipulate objects reference sets.
**/

/*! \defgroup stringMT string arrays
 *  \ingroup array 
 *  \brief Misceallenous string containers
 * 
 *  This module provides string container classes based on the dynamic 
 *  array containers \ref genarray.
 *  \n
 *  Their class names are TAxS_Xxxx where
 *  `x' is `D', 'O' or 'R' which indicates the base class (respecively:
 *  \ref TA_DatMemt, \ref TA_ObjMemt, \ref TA_RefMemt),
 *  and where `Xxxx' indicates the type of array elements. The supported
 *  type are \ref cstr, \ref TA_Strbc, \ref TA_Strdb. 
 *    - class \ref TADS_Cstr :  AAAA
 *    - class \ref TAOS_Strbc : AAAA
 *    - class \ref TAOS_Strdb : AAAA
**/

/*! \defgroup keyvalMT (key,val) couple arrays
 *  \ingroup array 
 *  \brief Misceallenous containers of (key,value) couples
 *
 *  Their class names are TAxS_YyyyZzzz where
 *  `x' is 'O' or 'R' which indicates the base class (respecively:
 *   \ref TA_ObjMemt, \ref TA_RefMemt),
 *  and where `Yyyyy' and `Zzzzz'indicate the type of the key and of
 *  the value of the array elements.
 *  The supported type are \ref TA_Strbc and \ref TA_Strdb for the key and
 *  \ref TA_Strbc, \ref TAOS_Strbc for the value.
 *    - class \ref TAOS_StrbcStrbc  : AAAA
 *    - class \ref TAOS_StrdbOSStrdb : AAAA
**/

#endif /* FILE_TA_H */

