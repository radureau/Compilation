/**************************************************************************
 *   $cours: lex/yacc
 * $section: projet
 *      $Id: vmmain.c 386 2016-02-09 08:16:58Z ia $
 * $HeadURL: svn://lunix120.ensiie.fr/ia/cours/lex-yacc/src/vm1/vm/vmmain.c $
 *  $Author: Ivan Auge (Email: auge@ensiie.fr)
**************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>

#include <ta.h>

#include "vm.h"

typedef struct _TparamTMO {
    char  base; // d h
    char  kind; // b w l
    Taddr addr;
    int   nb;
} TparamTMO;

typedef struct _Tparam {
    const char* streamName;
    FILE* stream;
    int   optBigEndian;
    int   optStopAfterNbInst;
    int   optStopAfterNbInst_nb;

    int   optTraceInstAddr;
    int   optTraceReg;
    TparamTMO* optTraceMem;
    int        optTraceMem_sz;

    int   optDisassemble;
    Taddr optDisassembleStartAddr;
    Taddr optDisassembleEndAddr;

    int   optGCD1;
    int   optGCD2;
} Tparam;

static void main_trace_instAddr(const Tvm*vm, Taddr addr);
static void main_trace_regs(const Tvm*vm);
static void main_trace_mem(const Tvm*vm, TparamTMO* segs, int nbsegs);

static char gcd1_data_0[] = {
   20,  11,   0,   0,   0,   0,   0,   0,   0,   0,   0,  47,  17,   0,  48,   3,
    0,  47,  23,   0,  48,   7,   0,  27,  34,   0,  32,   3,   0,  48,   7,   0,
   69,   0,  31,  45,   0,  32,   3,   0,  48,   7,   0,  57,   0,  11,  23,   0,
   32,   3,   0,  32,   3,   0,  48,   7,   0,  11,  23,   0,  32,   7,   0,  32,
    7,   0,  48,   3,   0,  51,  75,   0,  48,   3,   0,  52,  
};
static char gcd2_data_0[] = {
   20,   0,   4,  27,  10,   0,-127, -78,  29,   0,  31,  23,   0,-127, -78,  17,
    0,  11,   3,   0, -94, -94,-111,  11,   3,   0,-127,-127, -78,  19,  34,   0,
 -128,-111,  40
};
static char gcd2_data_1024[] = {
   47,   4,   4,-111,  47,  10,   4,  48,  -4,   3,  18,  17,   4,-126,  16,  -4,
    3,  36,  22,   4,   3,   0,  51,  26,   4,-112,  52,
};

static void main_parse_cmdline(Tparam*param, int*argc, const char**argv)
{
    memset(param,0,sizeof(*param));
    
    taclp_init_EH(argc,argv,TAEH_WARN,1);

    int option_le,option_be;
    taclp_addSwitch("%s     <help>vm data file</help>",NULL,&param->streamName); 
    taclp_addSwitch("-le    <help>little endian (default)</help>",&option_le);
    taclp_addSwitch("-be    <help>big endian </help>",&option_be);
    int option_nbi,option_nbi_value;
    taclp_addSwitch("-stop %i" 
            "<par>#</par>" "<help>exit after # instructions</help>"
            ,&option_nbi,&option_nbi_value);
    taclp_addSwitch("-taddr <help>enable tracing of instruction addresses</help>",
            &param->optTraceInstAddr);
    taclp_addSwitch("-treg  <help>enable register tracing</help>",&param->optTraceReg);
    int optiom_tmem; char** optiom_tmem_str;
    taclp_addSwitch("-tmem %s+" 
            "<par>a1B#1K [a2B#2K ...]</par>"
            "<help>enable tracing of #i K starting at the ai addresse,\n"
            "                 with B in d|h (decimal|hexadecimal), "
                             "with K in b|w|l (byte|word|long)\n"
            "                 for instance: -tmem 10h8c 20d3l</help>"
            ,&optiom_tmem,&optiom_tmem_str);
    int option_disass; unsigned int* option_disass_addr;
    taclp_addSwitch("-d %u* <par> addr [addr2]</par>"
                          "<help> disassemble from addr to addr2</help>"
                    ,&option_disass,&option_disass_addr);
    taclp_addSwitch("-gcd1 <help>load VM with a GCD prgram</HELP>",
            &param->optGCD1);
    taclp_addSwitch("-gcd2 <help>load VM with an other GCD prgram</HELP>",
            &param->optGCD2);
    taclp_parse();

    if ( option_le && option_be )
        taeh_printf(TAEH_FATAL,0,"-le and -ge switches are exclusive.\n");

    if (option_be) param->optBigEndian = 1;

    if ( param->optGCD1==0 && param->optGCD2==0 ) {
      if ( param->streamName==0 )
        taeh_printf(TAEH_FATAL,0,"no vm data file is specified.\n");
      if ( (param->stream=fopen(param->streamName,"r"))==0 )
        taeh_printf(TAEH_FATAL,0,"can not open %s file for reading (%s).\n",
                param->streamName,strerror(errno));
    } 

    if ( option_nbi ) {
        param->optStopAfterNbInst=1;
        if ( option_nbi_value <=0 ) 
          taeh_printf(TAEH_FATAL,0,"%d is an inavalid value for -stop switch.\n",
                option_nbi_value);
        param->optStopAfterNbInst_nb=option_nbi_value;
    } else
        param->optStopAfterNbInst=0;

    if ( optiom_tmem ) {
        char** pstr;
        int i,addr,nb;
        char f,k;
        for ( i=0,pstr=optiom_tmem_str; *pstr ; i+=1,pstr+=1) {
            if ( sscanf(*pstr,"%d%c%d%c",&addr,&f,&nb,&k)!=4 ||
                 (f!='x' && f!='d') || (k!='b' && k!='w' && k!='l') ) 
                taeh_printf(TAEH_FATAL,0,
                    "\"%s\" bad formated %d th string in -tmb switch"
                    " (expected: addrFnbK with F=x|d and K=b|w|l.\n"
                    ,*pstr, i+1);
            param->optTraceMem = (TparamTMO*)realloc(param->optTraceMem,
                    sizeof(param->optTraceMem[0])*(param->optTraceMem_sz+1));
            param->optTraceMem[param->optTraceMem_sz].base   = f;
            param->optTraceMem[param->optTraceMem_sz].kind   = k;
            param->optTraceMem[param->optTraceMem_sz].addr   = addr;
            param->optTraceMem[param->optTraceMem_sz].nb     = nb;
            param->optTraceMem_sz+=1;
        }
    }

    if (option_disass!=0) {
        if ( option_disass_addr==0 && *option_disass_addr!=0 && *option_disass_addr!=1 && *option_disass_addr!=2 ) {
            taeh_printf(TAEH_FATAL,0,
                "\"-d addr [addr2]\" switch is bad formated.\n");
        }
        param->optDisassemble = 1;
        if ( *option_disass_addr==0 ) {
            param->optDisassembleStartAddr = 0;
            param->optDisassembleEndAddr    = VM_MemSize;
        } else {
            param->optDisassembleStartAddr = option_disass_addr[1];
            if ( *option_disass_addr==2 )
                param->optDisassembleEndAddr    = option_disass_addr[2];
            else
                param->optDisassembleEndAddr    = VM_MemSize;
        }
    }
}

static void main_run(const Tparam* param, Tvm* vm);
static void main_disassemble(const Tparam* param, const Tvm* vm);

int main(int argc, const char** argv)
{
    const char* reason;
    Tparam param;

    // parse the command line
    main_parse_cmdline(&param,&argc,argv);

    // Initialization of the VM
    Tvm *vm;
    if ( param.optGCD1 ) {
        vm=vm_new(param.stream,&reason,param.optBigEndian);
        memcpy(vm->mem,gcd1_data_0,sizeof(gcd1_data_0));
    } else if ( param.optGCD2 ) {
        vm=vm_new(param.stream,&reason,param.optBigEndian);
        memcpy(vm->mem,gcd2_data_0,sizeof(gcd2_data_0));
        memcpy(vm->mem+1024,gcd2_data_1024,sizeof(gcd2_data_1024));
    } else {
        if ( (vm=vm_new(param.stream,&reason,param.optBigEndian)) == 0 ) {
            taeh_printf(TAEH_FATAL,0,
                    "<n> fail to initialize the VM from %s file: %s\n",
                    param.streamName,reason);
            taeh_printf(TAEH_FATAL,0,"<c> reason: %s\n",reason);
        }
        if (param.streamName!=0)
            fclose(param.stream);
    }
    
    if ( param.optDisassemble ) {
        // disassemble the memory of VM
        main_disassemble(&param,vm);
    } else {
        // running the VM starting at at addresse 0
        main_run(&param,vm);
    }

    return 0;
}

static void main_run(const Tparam* param, Tvm* vm)
{
    const char* reason;
    Taddr addr=0x0000,contaddr;
    Tinst *inst;

    int nbinst;
    for (nbinst=0 ; param->optStopAfterNbInst==0 ||
            nbinst<param->optStopAfterNbInst_nb ; nbinst+=1) {
        if (param->optTraceInstAddr) main_trace_instAddr(vm,addr);
        if (param->optTraceReg)      main_trace_regs(vm);
        if (param->optTraceMem)      main_trace_mem(vm, param->optTraceMem,
                                                       param->optTraceMem_sz);
        reason=0;
        if ( (inst=vm_getInst(vm,addr,&reason)) == 0 ) {
            taeh_printf(TAEH_FATAL,0,"<n> %04x: invalid instruction : %s\n",
                    addr,reason);
        }
        if ( (reason=inst->run(&contaddr,vm,inst)) !=0 ) {
            //taeh_printf(TAEH_WARN,0,"<n> %04x: %s\n",addr,reason);
            break;
        }
        vm_freeInst(inst);
        addr=contaddr;
    }

    // print end message
    taeh_printf(TAEH_WARN,0,
            "<n> %04x: machine stoped after %d instructions,\n",
            addr,nbinst);
    if ( reason ) 
        taeh_printf(TAEH_WARN,0,
            "<c> %04x: %s.\n", addr,reason);
}

static void main_disassemble_printAddr(Taddr addr,int printAddr, int printStar)
{
    if (printStar) printf("%5s:\n","****");
    if (printAddr) printf("%5d: ",addr&0x7FFF);
}

static void main_disassemble_printData(const Tvm* vm, Taddr from, Taddr to, int lf)
{
    Taddr addr;
    int   i;

    for (addr=from ; addr<to ; addr++)
        printf(" %02x",vm->mem[addr]&0xFF);
    if ( lf!=0 ) 
        printf("\n");
    else for ( i=to-from ; i<16 ; i++)
        printf("   ");
}

static void main_disassemble_dumpMem(const Tvm* vm, Taddr from, Taddr to)
{
    char  buffer[16];
    int   bufferHasData;
    Taddr end,addr=from;

    if ( (addr%16)!=0 ) {
        main_disassemble_printAddr(addr,1,0);
        end = (addr&~(16-1)) + 16;
        if (end>to) end=to;
        main_disassemble_printData(vm,addr,end,1);
        addr = end;
    }

    bufferHasData = 0;
    int starMustBePrinted = 0;
    while ( addr<to ) {
        end = addr + 16;
        if (end>to) end=to;
        if ( bufferHasData && strncmp(buffer,vm->mem+addr,end-addr)==0 ) {
            starMustBePrinted = 1;
            addr=end;
            continue;
        }
        strncpy(buffer,vm->mem+addr,end-addr);
        bufferHasData = 1;
        main_disassemble_printAddr(addr,1,starMustBePrinted);
        main_disassemble_printData(vm,addr,end,1);
        if ( starMustBePrinted )
            starMustBePrinted = 0;
        addr += 16;
    }
    main_disassemble_printAddr(0,0,starMustBePrinted);
}

static int main_disassemble_dumpInst_EA(
    const char* str, const Tvm* vm, Taddr addr, const Teffaddr* ea)
{
    int len; // EA length in memory
    char value[100];
    switch (ea->mode) {
      case ADMO_IMM: len=1+ea->length; sprintf(value,"%Ld",    ea->u.value); break;
      case ADMO_DM:  len=1+2;          sprintf(value,"DM=%Ld",ea->u.addr);  break;
      case ADMO_IM:  len=1+2;          sprintf(value,"IM=%Ld",ea->u.addr);  break;
      case ADMO_DR:  len=1;            sprintf(value,"R%d",    ea->u.reg);   break;
      case ADMO_IR:  len=1;            sprintf(value,"(R%d)",  ea->u.reg);   break;
      case ADMO_IRpp:len=1;            sprintf(value,"(R%d)++",ea->u.reg);   break;
      case ADMO_ppIR:len=1;            sprintf(value,"++(R%d)",ea->u.reg);   break;
      default:       len=1;             sprintf(value,"???");
    }
    main_disassemble_printAddr(addr,1,0);
    main_disassemble_printData(vm,addr,addr+len+2,0);
    printf(" : %7s%5s:%5s\n","",str,value);
    return len;
}

static Taddr main_disassemble_dumpInst(const Tvm* vm, const Tinst* inst, Taddr addr)
{
    char*n; int isNext,isNext2,isDest,isSrc1,isSrc2;
    switch (inst->codeop) {
        case COOP_ADD : n="add" ; isNext=1; isNext2=0; isDest=1; isSrc1=1; isSrc2=1; break;
        case COOP_SUB : n="sub" ; isNext=1; isNext2=0; isDest=1; isSrc1=1; isSrc2=1; break;
        case COOP_MUL : n="mul" ; isNext=1; isNext2=0; isDest=1; isSrc1=1; isSrc2=1; break;
        case COOP_MOV : n="mov" ; isNext=1; isNext2=0; isDest=1; isSrc1=1; isSrc2=0; break;
        case COOP_JMP : n="jmp" ; isNext=1; isNext2=0; isDest=0; isSrc1=0; isSrc2=0; break;
        case COOP_JE  : n="je"  ; isNext=1; isNext2=1; isDest=1; isSrc1=1; isSrc2=0; break;
        case COOP_JL  : n="jl"  ; isNext=1; isNext2=1; isDest=1; isSrc1=1; isSrc2=0; break;
        case COOP_JLE : n="jel" ; isNext=1; isNext2=1; isDest=1; isSrc1=1; isSrc2=0; break;
        case COOP_CALL: n="call"; isNext=1; isNext2=1; isDest=0; isSrc1=0; isSrc2=0; break;
        case COOP_RET : n="ret" ; isNext=0; isNext2=0; isDest=0; isSrc1=0; isSrc2=0; break;
        case COOP_IN  : n="in"  ; isNext=1; isNext2=0; isDest=1; isSrc1=0; isSrc2=0; break;
        case COOP_OUT : n="out" ; isNext=1; isNext2=0; isDest=1; isSrc1=0; isSrc2=0; break;
        case COOP_STOP: n="stop"; isNext=0; isNext2=0; isDest=0; isSrc1=0; isSrc2=0; break;
        default:        n="???" ; isNext=0; isNext2=0; isDest=0; isSrc1=0; isSrc2=0; break;
    }

    /* op next */
    int i,len=1 + isNext * 2;
    main_disassemble_printAddr(addr,1,0);
    main_disassemble_printData(vm,addr,addr+len,0);
    printf(" : %4s%s",n,
        inst->easz==1 ? ".b" : inst->easz==2 ? ".w" :  inst->easz==3 ? ".l" : "  ");
    if (isNext) printf(" %5s=%5d","next",inst->nextaddr);
    printf("\n");
    /* Effective addressse */
    if (isDest) len += main_disassemble_dumpInst_EA("dest",vm,addr+len,&inst->destEA);
    if (isSrc1) len += main_disassemble_dumpInst_EA("src", vm,addr+len,&inst->srcEA1);
    if (isSrc2) len += main_disassemble_dumpInst_EA("src", vm,addr+len,&inst->srcEA2);
    for (i=0 ; i<inst->srcEAmore_sz ; i++)
        len += main_disassemble_dumpInst_EA("src", vm,addr+len,inst->srcEAmore+i);
    /* next2 */
    if (isNext2) {
        main_disassemble_printAddr(addr+len,1,0);
        main_disassemble_printData(vm,addr+len,addr+len+2,0);
        printf(" : %7s%5s=%5d\n","","next2",inst->nextaddr2);
    }

    /* addressse of the next instruction */
    return addr + inst->length;
}

static void main_disassemble(const Tparam* param, const Tvm* vm)
{
    const char* reason;
    Taddr addr,contaddr;
    Tinst *inst;

    for ( addr=param->optDisassembleStartAddr ;
            addr<param->optDisassembleEndAddr ; ) {
        reason=0;
        contaddr = addr;
        while ( contaddr<param->optDisassembleEndAddr && (inst=vm_getInst(vm,contaddr,&reason))==0 ) {
            contaddr += 1;
        }
        if ( contaddr!=addr ) {
            main_disassemble_dumpMem(vm,addr,contaddr);
        }
        addr = contaddr;
        if ( inst!=0 ) {
            addr=main_disassemble_dumpInst(vm,inst,contaddr);
            vm_freeInst(inst);
        }
    }
}

static void main_trace_instAddr(const Tvm*vm, Taddr addr)
{
    FILE*stream=stdout;
    int i;
    fprintf(stream,"%04Lx/%05Ld: instruction addresse: ",addr,addr);
    for (i=0 ; i<16 ; i++)
        fprintf(stream," %02x",vm->mem[addr+i]&0xff);
    fprintf(stream,"\n");
}

static void main_trace_regs(const Tvm*vm)
{
    FILE*stream=stdout;
    int i,j;
    for (i=0 ; i<VM_NbReg ; ) {
        fprintf(stream," ");
        for (j=0 ; j<8 && i<VM_NbReg ; j+=1,i+=1) 
            fprintf(stream," R%02d=%08Lx",i,vm->regs[i]);
        fprintf(stream,"\n");
    }
}

static void main_trace_mem(const Tvm*vm, TparamTMO* segs, int nbsegs)
{
    FILE*stream=stdout;
    int i,j;
    for (i=0 ; i<nbsegs ; i++,segs++) {
        Taddr addr=segs->addr;
        fprintf(stream,"  %04Lx/%05Ld:%c", addr,addr,segs->base);
        for (j=0 ; j<segs->nb ; j++) {
          switch (segs->kind) {
            case 'b': if ( segs->base=='x' )
                fprintf(stream,"%3s%02x","",(*(unsigned char*)(vm->mem+addr))&0xff);
              else
                fprintf(stream,"%1s%4d","",(*(char*)(vm->mem+addr)));
              addr += 1;
              break;
            case 'w': if ( segs->base=='x' )
                fprintf(stream,"%6s%04x","",(*(unsigned short*)(vm->mem+addr))&0xffff);
              else
                fprintf(stream,"%3s%7d","",(*(short*)(vm->mem+addr)));
              addr += 2;
              break;
            case 'l': if ( segs->base=='x' )
                fprintf(stream,"%12s%08x","",(*(unsigned int*)(vm->mem+addr))&0xffffffff);
              else
                fprintf(stream,"%8s%12d","",(*(int*)(vm->mem+addr)));
              addr += 4;
              break;
          }
        }
        fprintf(stream,"\n");
    }
}

