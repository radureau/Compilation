/**************************************************************************
 *   $cours: lex/yacc
 * $section: projet
 *      $Id: vm.h 386 2016-02-09 08:16:58Z ia $
 * $HeadURL: svn://lunix120.ensiie.fr/ia/cours/lex-yacc/src/vm1/vm/vm.h $
 *  $Author: Ivan Auge (Email: auge@ensiie.fr)
**************************************************************************/

#define VM_MemSize  (1<<15)
#define VM_NbReg    16
#define VM_StackSize 16

typedef unsigned long Taddr;
typedef unsigned char Tuchar;
struct  _Tvm;
struct  _Tinst;
typedef long (*Tfuncread) (const struct _Tvm*,Taddr);
typedef void (*Tfuncwrite)(      struct _Tvm*,Taddr,long value);

typedef struct _Tvm {
    char mem[VM_MemSize];
    long regs[VM_NbReg];
    Tfuncread  read[4];        // read[0]==NULL, read[1/2/3]: read char/word/long
    Tfuncwrite write[4];       // same as read
    int        byteSize[4];    // byteSize[] {0, 1, 2, 4} according to read and write

    Taddr      stack[VM_StackSize]; // stack for call and ret
    int        stack_sz;            // number of elements in stack
} Tvm;
#define READ_CHAR 1
#define READ_WORD 2
#define READ_LONG 3
#define READ_ADDR 2

typedef enum _TcodeOp {
    COOP_ADD=1,  COOP_SUB=2, COOP_MUL=3, COOP_MOV=4,
    COOP_JMP=5,  COOP_JE=6,  COOP_JL=7,  COOP_JLE=8,
    COOP_CALL=9, COOP_RET=10, COOP_IN=11,  COOP_OUT=12,
    COOP_STOP=13
} TcodeOp;

typedef enum _Taddrmode {
    ADMO_IMM=0, ADMO_DM=1, ADMO_IM=2,
    ADMO_DR=4,  ADMO_IR=5, ADMO_IRpp=6, ADMO_ppIR=7
} Taddrmode;

typedef struct _Teffaddr {
    Tuchar  controlByte;
    int     length;         // byte size of EA
    int     last;
    Taddrmode   mode;
    union {
        int   reg;
        Taddr addr;
        long  value;
    } u;
} Teffaddr;

/* Returns 0 if OK otherwise an error message describing the problem. */
extern const char* vm_loadEA (long*data, Tvm*vm, const Teffaddr* ea, int easz);
extern const char* vm_storeEA(long data, Tvm*vm, const Teffaddr* ea, int easz);

/* Returns 0 and the addresse of the next instruction into *contaddr if OK
 * otherwise it returns an error message describing the problem. */
typedef const char* (*TfuncInstRun)(Taddr* contaddr,
        struct _Tvm*vm, const struct _Tinst* pi);

typedef struct _Tinst {
   int          length;  // byte length of instruction
   TfuncInstRun run;
   TcodeOp      codeop; 
   int          easz;    // size of ef: 0/1/2/3=not used/char/word/long 
   Taddr        nextaddr;
   Teffaddr     destEA;
   Teffaddr     srcEA1;
   Teffaddr     srcEA2;
   Teffaddr*    srcEAmore;
   int          srcEAmore_sz;
   Taddr        nextaddr2;
} Tinst;

/* Returns 0 if OK otherwise an error message describing the problem. */
extern const char* vm_verifAddr(const Tvm* vm, Taddr addr);
/* Returns the VM if OK otherwise 0 with *reason sets to an error message
 * describing the problem.
 * CAUTION: the returned VM is dynamically allocated so the
 *          caller must free it after usage with vm_free */
extern Tvm*        vm_new(FILE* stream, const char** reason, int bigEndian);
extern void        vm_free(Tvm*vm);
/* Returns the instruction that starts at addresse addr if OK otherwise
 * returns 0 with *reason sets to an error message describing the problem.
 * CAUTION: the returned instruction is dynamically allocated so the
 *          caller must free it after usage with vm_freeInst */
extern Tinst*      vm_getInst(const Tvm* vm, Taddr addr, const char** reason);
extern void        vm_freeInst(Tinst* i);
/* Returns a strings describing the binary of pi instruction.
 * CAUTION: the returned string is dynamically allocated so the
 *          caller must free it after usage with free */
extern char*       vm_dumpInst(const Tvm*vm, const Tinst*pi, Taddr addr);
