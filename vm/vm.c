/**************************************************************************
 *   $cours: lex/yacc
 * $section: projet
 *      $Id: vm.c 389 2016-02-09 15:00:22Z ia $
 * $HeadURL: svn://lunix120.ensiie.fr/ia/cours/lex-yacc/src/vm1/vm/vm.c $
 *  $Author: Ivan Auge (Email: auge@ensiie.fr)
**************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <ta.h>

#include "vm.h"

/***********************************************************************
 * Miscellaneous
**/

const char* vm_verifAddr(const Tvm* vm, Taddr addr)
{
    static char buffer[100];
    if ( addr >= sizeof(vm->mem) ) {
        sprintf(buffer,"addresse error (0x04x)",addr);
        return buffer;
    }
    return 0;
}

const char* vm_loadEA (long*data, Tvm*vm, const Teffaddr* ea, int easz)
{
#undef VERIF_ADDR
#define VERIF_ADDR(vm,addr,easz)  \
    if ( (reason=vm_verifAddr(vm,addr+vm->byteSize[easz]-1)) !=0 ) return reason
    const char* reason;
    Taddr pointer;
    switch (ea->mode) {
        case ADMO_IMM:
            *data = ea->u.value;
            break;
        case ADMO_DM:
#if 0
printf("DM: addr=%04Lx easz=%d : %02x %02x %02x %02x : %08Lx\n",
ea->u.addr,easz,
vm->mem[ea->u.addr+0],
vm->mem[ea->u.addr+1],
vm->mem[ea->u.addr+2],
vm->mem[ea->u.addr+3],
vm->read[easz](vm,ea->u.addr));
#endif
            VERIF_ADDR(vm,ea->u.addr,easz);
            *data = vm->read[easz](vm,ea->u.addr); 
            break;
        case ADMO_IM:
            VERIF_ADDR(vm,ea->u.addr,READ_ADDR);
            pointer = vm->read[READ_ADDR](vm,ea->u.addr); 
            VERIF_ADDR(vm,pointer,easz);
            *data = vm->read[easz](vm,pointer); 
            break;
        case ADMO_DR:
            *data = vm->regs[ea->u.reg]; 
            break;
        case ADMO_IR:
            pointer = vm->regs[ea->u.reg];
            VERIF_ADDR(vm,pointer,easz);
            *data = vm->read[easz](vm,pointer); 
            break;
        case ADMO_IRpp:
            pointer = vm->regs[ea->u.reg];
            VERIF_ADDR(vm,pointer,easz);
            *data = vm->read[easz](vm,pointer); 
            vm->regs[ea->u.reg] += vm->byteSize[easz];
            break;
        case ADMO_ppIR:
            vm->regs[ea->u.reg] += vm->byteSize[easz];
            pointer = vm->regs[ea->u.reg];
            VERIF_ADDR(vm,pointer,easz);
            *data = vm->read[easz](vm,pointer); 
            break;
        default:
           taeh_printf(TAEH_FATAL,0,"detected BUG in %s function with mode=%d\n",
                "vm_loadEA",ea->mode);
    }
    return 0;
}

const char* vm_storeEA(long data, Tvm*vm, const Teffaddr* ea, int easz)
{
#undef VERIF_ADDR
#define VERIF_ADDR(vm,addr,easz)  \
    if ( (reason=vm_verifAddr(vm,addr+vm->byteSize[easz]-1)) !=0 ) return reason
    static char buffer[1000];
    const char* reason;
    Taddr pointer;
    switch (ea->mode) {
        case ADMO_IMM:
            sprintf(buffer,"invalid instruction (try to write immediat EA).");
            return buffer;
        case ADMO_DM:
            VERIF_ADDR(vm,ea->u.addr,easz);
            vm->write[easz](vm,ea->u.addr,data); 
            break;
        case ADMO_IM:
            VERIF_ADDR(vm,ea->u.addr,READ_ADDR);
            pointer = vm->read[READ_ADDR](vm,ea->u.addr); 
            VERIF_ADDR(vm,pointer,easz);
            vm->write[easz](vm,pointer,data); 
            break;
        case ADMO_DR:
            vm->regs[ea->u.reg] = data; 
            break;
        case ADMO_IR:
            pointer = vm->regs[ea->u.reg];
            VERIF_ADDR(vm,pointer,easz);
            vm->write[easz](vm,pointer,data);
            break;
        case ADMO_IRpp:
            pointer = vm->regs[ea->u.reg];
            VERIF_ADDR(vm,pointer,easz);
            vm->write[easz](vm,pointer,data);
            vm->regs[ea->u.reg] += vm->byteSize[easz];
            break;
        case ADMO_ppIR:
            vm->regs[ea->u.reg] += vm->byteSize[easz];
            pointer = vm->regs[ea->u.reg];
            VERIF_ADDR(vm,pointer,easz);
            vm->write[easz](vm,pointer,data);
            break;
        default:
           taeh_printf(TAEH_FATAL,0,"detected BUG in %s function with mode=%d\n",
                "vm_storeEA",ea->mode);
    }
    return 0;
}

/***********************************************************************
 * initialization
**/

static long vm_read_char(const Tvm*vm, Taddr addr)
    { return vm->mem[addr]&0xff; }
static void vm_write_char(Tvm*vm, Taddr addr, long value)
    { vm->mem[addr]=value; }
static long vm_read_word_le(const Tvm*vm, Taddr addr)
    { return ((vm->mem[addr+0]&0xff)<<0) | ((vm->mem[addr+1]&0xff)<<8); }
static void vm_write_word_le(Tvm*vm, Taddr addr, long value)
    { vm->mem[addr+0] = (value>>0)&0xFF;  vm->mem[addr+1] = (value>>8)&0xff; }
static long vm_read_word_be(const Tvm*vm, Taddr addr)
    { return ((vm->mem[addr+0]&0xff)<<8) | ((vm->mem[addr+1]&0xff)<<0); }
static void vm_write_word_be(Tvm*vm, Taddr addr, long value)
    { vm->mem[addr+0] = (value>>8)&0xFF;  vm->mem[addr+1] = (value>>0)&0xff; }
static long vm_read_long_le(const Tvm*vm, Taddr addr)
    { return ((vm->mem[addr+0]&0xff)<< 0) | ((vm->mem[addr+1]&0xff)<< 8) +
             ((vm->mem[addr+2]&0xff)<<16) | ((vm->mem[addr+3]&0xff)<<24); }
static void vm_write_long_le(Tvm*vm, Taddr addr, long value)
    { vm->mem[addr+0] = (value>> 0)&0xFF;  vm->mem[addr+1] = (value>> 8)&0xff;
      vm->mem[addr+2] = (value>>16)&0xFF;  vm->mem[addr+3] = (value>>24)&0xff; }
static long vm_read_long_be(const Tvm*vm, Taddr addr)
    { return ((vm->mem[addr+0]&0xff)<<24) | ((vm->mem[addr+1]&0xff)<<16) +
             ((vm->mem[addr+2]&0xff)<< 8) | ((vm->mem[addr+3]&0xff)<< 0); }
static void vm_write_long_be(Tvm*vm, Taddr addr, long value)
    { vm->mem[addr+0] = (value>>24)&0xFF;  vm->mem[addr+1] = (value>>16)&0xff;
      vm->mem[addr+2] = (value>> 8)&0xFF;  vm->mem[addr+3] = (value>> 0)&0xff; }

Tvm*        vm_new(FILE* stream, const char** reason, int bigEndian)
{
    int status;
    Tvm* vm = (Tvm*)malloc(sizeof(*vm));

    memset(vm,0,sizeof(*vm));

    // read the vm data
    if (stream!=0) {
        int c;
        status = fread(vm->mem,1,sizeof(vm->mem),stream);
        if ( status==0 ) {
            *reason = "not data found";
            free(vm);
            return 0;
        }
        status = fread(&c,1,sizeof(c),stream);
        if ( status!=0 ) {
            *reason = "to many data found";
            free(vm);
            return 0;
        }
    }

    // set the read write and byteSize tables
    vm->read    [1] = vm_read_char;
    vm->write   [1] = vm_write_char;
    vm->byteSize[1] = 1;
    vm->byteSize[2] = 2;
    vm->byteSize[3] = 4;
    if ( bigEndian ) {
        vm->read    [2] = vm_read_word_be;
        vm->write   [2] = vm_write_word_be;
        vm->read    [3] = vm_read_long_be;
        vm->write   [3] = vm_write_long_be;
    } else {
        vm->read    [2] = vm_read_word_le;
        vm->write   [2] = vm_write_word_le;
        vm->read    [3] = vm_read_long_le;
        vm->write   [3] = vm_write_long_le;
    }

    return vm;
}

void        vm_free(Tvm*vm)
{ if (vm) free(vm); }

/***********************************************************************
 * run
**/

const char* vm_run_add(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    int i;
    const char* reason;
    long dest,src;

    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA1,pi->easz))!=0 )
        return reason;
    dest=src;
    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA2,pi->easz))!=0 )
        return reason;
    dest +=src;
    for ( i=0 ; i<pi->srcEAmore_sz ; i+=1) {
        Teffaddr* ea=pi->srcEAmore+i;
        if ( (reason=vm_loadEA(&src,vm,ea,pi->easz))!=0 )
            return reason;
        dest +=src;
    }
    
    if ( (reason=vm_storeEA(dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_sub(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    int i;
    const char* reason;
    long dest,src;

    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA1,pi->easz))!=0 )
        return reason;
    dest=src;
    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA2,pi->easz))!=0 )
        return reason;
    dest -=src;
    for ( i=0 ; i<pi->srcEAmore_sz ; i+=1) {
        Teffaddr* ea=pi->srcEAmore+i;
        if ( (reason=vm_loadEA(&src,vm,ea,pi->easz))!=0 )
            return reason;
        dest -=src;
    }
    
    if ( (reason=vm_storeEA(dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_mul(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    int i;
    const char* reason;
    long dest,src;

    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA1,pi->easz))!=0 )
        return reason;
    dest=src;
    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA2,pi->easz))!=0 )
        return reason;
    dest *=src;
    for ( i=0 ; i<pi->srcEAmore_sz ; i+=1) {
        Teffaddr* ea=pi->srcEAmore+i;
        if ( (reason=vm_loadEA(&src,vm,ea,pi->easz))!=0 )
            return reason;
        dest *=src;
    }
    
    if ( (reason=vm_storeEA(dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_mov(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;
    long dest;

    if ( (reason=vm_loadEA(&dest,vm,&pi->srcEA1,pi->easz))!=0 )
        return reason;
    
    if ( (reason=vm_storeEA(dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_jmp(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;

    *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_je(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;
    long dest,src;

    if ( (reason=vm_loadEA(&dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA1,pi->easz))!=0 )
        return reason;
    
    if ( dest==src )
        *contaddr=pi->nextaddr2;
    else
        *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_jl(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;
    long dest,src;

    if ( (reason=vm_loadEA(&dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA1,pi->easz))!=0 )
        return reason;
    
    if ( dest<src )
        *contaddr=pi->nextaddr2;
    else
        *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_jle(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;
    long dest,src;

    if ( (reason=vm_loadEA(&dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    if ( (reason=vm_loadEA(&src,vm,&pi->srcEA1,pi->easz))!=0 )
        return reason;
    
    if ( dest<=src )
        *contaddr=pi->nextaddr2;
    else
        *contaddr=pi->nextaddr;
    return 0;
}

static const char* vm_push(Tvm*vm,Taddr addr)
{
    if ( vm->stack_sz == VM_StackSize )
        return "stack overflow on call instruction";
    vm->stack[vm->stack_sz++] = addr;
    return 0;
}

static const char* vm_pop(Tvm*vm,Taddr*addr)
{
    if ( vm->stack_sz == 0 )
        return "stack underflow on ret instruction";
    vm->stack_sz -=1;
    *addr = vm->stack[vm->stack_sz];
    return 0;
}

const char* vm_run_call(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;

    if ( (reason=vm_push(vm,pi->nextaddr)) != 0 )
        return reason;
    *contaddr=pi->nextaddr2;
    return 0;
}

const char* vm_run_ret(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;

    if ( (reason=vm_pop(vm,contaddr)) != 0 )
        return reason;
    return 0;
}

const char* vm_run_in(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;
    long dest;
    char buf[100];

    printf("vm in? ");
    scanf("%s",buf);
    if ( ! isatty(0) ) {
        printf("%s\n",buf);
    }
    sscanf(buf,"%Ld",&dest);

    if ( (reason=vm_storeEA(dest,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_out(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    const char* reason;
    long src;

    if ( (reason=vm_loadEA(&src,vm,&pi->destEA,pi->easz))!=0 )
        return reason;

    if ( pi->easz==1 ) {
        if ( src >= 0x20 )
            printf("vm out: %c\n",src);
        else
            printf("vm out: %02x\n",src);
    } else
        printf("vm out: %08Lx\n",src);

    *contaddr=pi->nextaddr;
    return 0;
}

const char* vm_run_stop(Taddr* contaddr, Tvm*vm, const Tinst* pi)
{
    *contaddr=pi->nextaddr;
    return "program terminated";
}

/***********************************************************************
 * Decode
**/

typedef struct _TinstParam {
    Tuchar codeop_raw;
    int   isNextAddr;
    int   isDestEA;
    int   isSrcEA1; 
    int   isSrcEA2; 
    int   isSrcEAmore; 
    int   isNextAddr2;
} TinstParam;

static const char* vm_decodeCodeop(
        TfuncInstRun* run, TcodeOp* codeop,int* easz,TinstParam* p,long byte)
{
    static char buffer[100];
    *codeop=byte>>2;
    *easz=byte&3;
    memset(p,0,sizeof(*p));
    p->codeop_raw = byte;
    
    // instructions without easz
    switch (*codeop) {
      case COOP_JMP:
        *run = vm_run_jmp;
        p->isNextAddr = 1;
        return 0;
      case COOP_CALL:
        *run = vm_run_call;
        p->isNextAddr = 1;
        p->isNextAddr2 = 1;
        return 0;
      case COOP_RET:
        *run = vm_run_ret;
        return 0;
      case COOP_STOP:
        *run = vm_run_stop;
        return 0;
    }
 
    // instructions with easz
    if ( *easz==0 )
        goto invalid_codeop;

    // instructions with easz and 2 EA source or more 
    switch (*codeop) {
      case COOP_ADD:
        *run = vm_run_add; goto others_1;
      case COOP_SUB:
        *run = vm_run_sub; goto others_1;
      case COOP_MUL:
        *run = vm_run_mul;
others_1:
        p->isNextAddr = 1;
        p->isDestEA   = 1;
        p->isSrcEA1   = 1;
        p->isSrcEA2   = 1;
        p->isSrcEAmore= 1;
        return 0;
    }
    
    // instructions with easz and exactly 1 EA source
    switch (*codeop) {
      case COOP_JE:
        p->isNextAddr2 = 1;
        *run = vm_run_je; goto others_2;
      case COOP_JL:
        p->isNextAddr2 = 1;
        *run = vm_run_jl; goto others_2;
      case COOP_JLE:
        p->isNextAddr2 = 1;
        *run = vm_run_jle; goto others_2;
      case COOP_MOV:
        *run = vm_run_mov;
others_2:
        p->isNextAddr = 1;
        p->isDestEA   = 1;
        p->isSrcEA1   = 1;
        p->isSrcEA2   = 0;
        p->isSrcEAmore= 0;
        return 0;
    }
   
    // instructions with easz and 1 dest and 0 EA source
    switch (*codeop) {
      case COOP_IN:
        *run = vm_run_in;
        p->isNextAddr = 1;
        p->isDestEA   = 1;
        return 0;
      case COOP_OUT:
        *run = vm_run_out;
        p->isNextAddr = 1;
        p->isDestEA   = 1;
        return 0;
    }

invalid_codeop:
    sprintf(buffer,"0x%02x is an invalid code operation\n",*codeop);
    return buffer;
}

static const char* vm_fetchEffAddr(Teffaddr* ea, const Tvm* vm, Taddr addr, int easz)
{
    static char buffer[1000];
    const char* reason=0;
    memset(ea,0,sizeof(*ea));
       
    ea->controlByte = vm->read[READ_CHAR](vm,addr);
    ea->length      = vm->byteSize[READ_CHAR];
    ea->last        = (ea->controlByte&0x10)!=0;
    
    Taddrmode mode = (ea->controlByte&0xE0)>>5;
    switch (mode) {
        case ADMO_IMM :
            if ( (ea->controlByte&0x0f)!=0 )
                goto invalid_controlbyte;
            ea->mode = mode;
            ea->u.value = vm->read[easz](vm,addr+ea->length);
            ea->length += vm->byteSize[easz];
            break;
        case ADMO_DM :
        case ADMO_IM :
            if ( (ea->controlByte&0x0f)!=0 )
                goto invalid_controlbyte;
            addr = vm->read[READ_ADDR](vm,addr+ea->length);
            if ( (reason=vm_verifAddr(vm,addr)) != 0 )
                return reason;
            ea->mode = mode;
            ea->u.addr = addr;
            ea->length += vm->byteSize[READ_ADDR];
            break;
        case ADMO_DR :
        case ADMO_IR :
        case ADMO_IRpp :
        case ADMO_ppIR :
            ea->mode = mode;
            ea->u.reg = ea->controlByte&0xf;
            ea->length += 0;
            break;
        default:
            goto invalid_controlbyte;
    }

    return 0;

invalid_controlbyte:
    sprintf(buffer,"0x%02x is an invalid control byte for effective addresse",mode);
    return buffer;
}


Tinst* vm_getInst(const Tvm* vm, Taddr addr, const char** reason)
{
    static char buffer[1000];
    Tinst i;
    long tmp;
    TinstParam p;

    memset(&i,0,sizeof(i));

    // fetch codeop and length
    tmp = vm->read[READ_CHAR](vm,addr);
    if ( (*reason=vm_decodeCodeop(&i.run,&i.codeop,&i.easz,&p,tmp)) != 0 )
        goto error;
    i.length += vm->byteSize[READ_CHAR];

    // fetch next addresse if needed
    if ( p.isNextAddr ) {
       i.nextaddr = vm->read[READ_ADDR](vm,addr+i.length);
       if ( (*reason=vm_verifAddr(vm, i.nextaddr)) != 0 )
            goto error;
       i.length += vm->byteSize[READ_ADDR];
    }

    int eanum=0;
    // fetch effective addresse of destination if needed
    if ( p.isDestEA ) {
       if ( (*reason=vm_fetchEffAddr(&i.destEA, vm, addr+i.length, i.easz)) != 0 )
            goto error;
       i.length += i.destEA.length;
       if ( (i.destEA.last==1 && p.isSrcEA1!=0) ||
            (i.destEA.last==0 && p.isSrcEA1==0) )
           goto invalid_last_in_controlbyte;
       eanum +=1;
    }

    // fetch the 1th effective addresse of source if needed
    if ( p.isSrcEA1 ) {
       if ( (*reason=vm_fetchEffAddr(&i.srcEA1, vm, addr+i.length, i.easz)) != 0 )
            goto error;
       i.length += i.srcEA1.length;
       if ( (i.srcEA1.last==1 && p.isSrcEA2!=0) ||
            (i.srcEA1.last==0 && p.isSrcEA2==0) )
           goto invalid_last_in_controlbyte;
       eanum +=1;
    }

    // fetch the 2nd effective addresse of source if needed
    if ( p.isSrcEA2 ) {
       if ( (*reason=vm_fetchEffAddr(&i.srcEA2, vm, addr+i.length, i.easz)) != 0 )
            goto error;
       i.length += i.srcEA2.length;
       if ( (i.srcEA2.last==0 && p.isSrcEAmore==0) )
           goto invalid_last_in_controlbyte;
       eanum +=1;
    }

    // fetch optional effective addresses of source if needed
    Teffaddr* ea;
    if ( p.isSrcEAmore &&  i.srcEA2.last==0 ) do {
        i.srcEAmore_sz += 1;
        i.srcEAmore = (Teffaddr*) realloc(i.srcEAmore,i.srcEAmore_sz*sizeof(*i.srcEAmore));
        ea = i.srcEAmore + i.srcEAmore_sz -1;
       if ( (*reason=vm_fetchEffAddr(ea, vm, addr+i.length, i.easz)) != 0 ) {
            free(i.srcEAmore);
            goto error;
       }
       i.length += ea->length;
    } while ( ea->last==0 );

    // fetch next2 addresse if needed
    if ( p.isNextAddr2 ) {
       i.nextaddr2 = vm->read[READ_ADDR](vm,addr+i.length);
       if ( (*reason=vm_verifAddr(vm, i.nextaddr2)) != 0 )
            goto error;
       i.length += vm->byteSize[READ_ADDR];
    }

    Tinst* ret= (Tinst*)malloc(sizeof(*ret));
    *ret=i;
    return ret;

    
invalid_last_in_controlbyte:
    sprintf(buffer,
        "unexpected %d value for bit B4 (last) of controlbyte of %d effective adresse\n",
        eanum==0 ? i.destEA.last : eanum==1 ? i.srcEA1.last : i.srcEA2.last, eanum+1);
   *reason = buffer;

error:
    return 0;
}

void        vm_freeInst(Tinst* i)
{
    if (i==0) return;
    if (i->srcEAmore!=0) free(i->srcEAmore);
    free(i);
}

