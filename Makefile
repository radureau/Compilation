all: vm/vmpico code/aspico

vm/vmpico:
	(cd vm; make vmpico 2>/dev/null)

code/aspico:
	(cd code; make)

test: all
	find exemples -name "*.asm" -print -exec sh -c 'code/aspico {} ; echo 3 1 2 3 4 5 6 | vm/vmpico picode ; echo "" ; echo "--------------" ; echo ""' \; ; rm picode

clean:
	@(cd code; make clean)
	@rm -f vm/vmpico
