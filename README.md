# Projet de compilation

## Composition du groupe

| Nom                     | Prénom                   | Email                        |
| -------------           |-------------             |-------------                 |
| *RADUREAU*              | *Gabriel*                | *gabriel.radureau@ensiie.fr* |
| *SIJOUMI*               | *Aimen*					 | *aimen.sijoumi@ensiie.fr*    |

## Présentation
Ce projet a consisté à la mise en place d'un compilateur nommé aspico. Ce compilateur compile des fichiers assembleurs pour la VM Pico.

## Les fichiers sources
```
/
| code/
  | aspico.ml
  | ass_prog.ml
  | ass_prog.mli
  | backend.ml
  | backend•mli
  | lexer.mll
  | Makefile
  | parser.mly
| exemples/
  | ...
| vm/
  | ...
Makefile
...
```

## La compilation

Il est possible de compiler seulement les fichiers sources en allant dans le dossier code et en tapant make :

```
your-computeur:code/$ make
```

De plus, il est aussi possible de compiler à la fois les fichiers sources correspondant au compilateur et aux sources de la VM Pico :

```
your-computeur:/$ make
```

Afin de tester tous les fichiers assembleur dans le dossier exemple, on peut taper la commande suivante :

```
your-computeur:/$ make test
```

Enfin, pour finir, pour effacer tous les fichiers exécutable, nous pouvons taper la commande suivante :

```
your-computeur:/$ make clean
```

## Utilisation

Pour compiler un fichier assembleur en un fichier picode (code binaire), il suffit de faire l'instruction suivante :

```
apico ass.asm
```

Le programme à différentes options :

> - -t affiche sur le flux standard de sortie la table des symboles puis se termine.

> - -d affiche sur le flux standard de sortie un dump humainement lisible du PICODE généré.

> - -o file le fichier de PICODE généré. Si cette option est absente le fichier est par défaut picode.
